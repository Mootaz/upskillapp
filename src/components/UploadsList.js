import React, { useState, useEffect } from "react";
import { FaArrowCircleDown, FaArrowCircleUp, FaTrash } from "react-icons/fa";
import { MdExpandMore } from 'react-icons/md';
import { baseURL } from "../constants";
import axios from "axios";
import { deleteMedia } from "./Actions/mediaActions.";
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import './UploadList.css'; // Import the CSS file
import { FaFilePdf, FaThumbsUp, FaComment, FaShare, FaThumbsDown, FaHeart } from 'react-icons/fa';
import { selectSessionUser } from "../topics/Session/slice";
import moment from 'moment';
import hero from '../assets/hero.png';
import Snackbar from '@mui/material/Snackbar';
import 'tailwindcss/tailwind.css';
import { FaThumbtack } from 'react-icons/fa';
import { useNavigate } from "react-router-dom";





const UploadsList = ({ medias, setMediaList, mediaId, commentId, userId }) => {


  useEffect(() => {
    // Function to fetch the like count

  }, []);

  const currentUser = useSelector(selectSessionUser);
  const [User_id, setUser_id] = useState(currentUser._id);
  const [selectedMediaId, setSelectedMediaId] = useState('');

  const [showModal, setShowModal] = useState(false);
  const [commentInput, setCommentInput] = useState('');
  const [replyInput, setReplyInput] = useState('');
  const [isLiked, setIsLiked] = useState(false);
  const [selectedMedia, setSelectedMedia] = useState(null);
  const [isDeleting, setIsDeleting] = useState(false);
  const [existingCommentId, setExistingCommentId] = useState('');
  const [likeCount, setLikeCount] = useState(0);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [popularComments, setPopularComments] = useState([]);
  const [liked, setLiked] = useState(false);
  const [courses, setCourses] = useState([]);
  const [IsDisliked, setIsDisliked] = useState(false);
  const [showComments, setShowComments] = useState(false);

  const [repliesVisible, setRepliesVisible] = useState([]);
  const [showFullResume, setShowFullResume] = useState(false);
  const [isVisible, setIsVisible] = useState(false);



  console.log(medias)





  const sequence = {
    startTime: 30, // Start time of the desired sequence in seconds
    endTime: 45,
    startTime2: 46,
    endTime2: 60,
    startTime3: 46,
    endTime3: 60, // End time of the desired sequence in seconds
    description: "Sequence description"
  };

  const toggleShowFullResume = () => {
    setShowFullResume(!showFullResume);
  };


  const navigate = useNavigate();

  const navigaterecommendationpage =()=> {
    navigate ('/UserRecommandationList')
  }


  const dispatch = useDispatch();

  const getComment = async (mediaId) => {


    // Send the comment data to the server
    const response = await axios.get(`http://localhost:4000/api/v1/media/${mediaId}/comments`);


    // Update the media state with the newly created comment
    const updatedMedias = medias.map((media) => {
      if (media._id === mediaId) {
        const updatedComments = response.data.comments.map((comment) => {
          // Add the comment sender's firstName and lastName to each comment
          return {
            ...comment,
            commentsender: {
              firstName: comment.commentsender.firstName,
              lastName: comment.commentsender.lastName,
            },
          };
        });
        return {
          ...media,
          comments: updatedComments,
        };
      }
      return media;
    });

    setMediaList({ media: updatedMedias }); // Update the state with the updated media data
  };


  const handleComment = async (mediaId) => {
    try {
      if (commentInput.trim() === '') {
        console.log('Comment is required');
        return;
      }
      console.log(mediaId)
      const commentData = {
        text: commentInput,
        User_id, // Use "user_id" instead of "User_id"
        date: moment().format('YYYY-MM-DD HH:mm:ss'),

      };
      console.log(commentData.User_id);

      // Send the comment data to the server
      const response = await axios.post(`http://localhost:4000/api/v1/media/${mediaId}/comments`, commentData);

      console.log('mootazr:', response.data); // Check the response data

      // Update the media state with the newly created comment
      const updatedMedias = medias.map((media) => {
        if (media._id === mediaId) {
          const updatedComments = response.data.comments.map((comment) => {
            // Add the comment sender's firstName and lastName to each comment
            return {
              ...comment,
              commentsender: {
                firstName: comment.commentsender.firstName,
                lastName: comment.commentsender.lastName,
              },
            };
          });
          return {
            ...media,
            comments: updatedComments,
          };
        }
        return media;
      });

      setMediaList({ media: updatedMedias }); // Update the state with the updated media data
      // Show a success notification
      setSnackbarOpen(true);
      setSnackbarMessage('Comment added successfully');
      // Clear the comment input field
      setCommentInput('');

    } catch (error) {
      console.log(error);
    }
  };



  const toggleReplies = (commentId) => {
    setRepliesVisible((prevVisible) => {
      if (prevVisible.includes(commentId)) {
        return prevVisible.filter((id) => id !== commentId);
      } else {
        return [...prevVisible, commentId];
      }
    });
  };


  const handleReply = async (mediaId, commentId, replyInput) => {
    try {
      if (replyInput.trim() === '') {
        console.log('Reply is required');
        return;
      }

      const replyData = {
        text: replyInput,
        User_id: userId,
        date: moment().format('YYYY-MM-DD HH:mm:ss')
      };

      // Send the reply data to the server
      const response = await axios.post(
        `http://localhost:4000/api/v1/media/${mediaId}/comments/${commentId}/replies`,
        replyData
      );

      console.log('Response from server:', response.data.comment.replies); // Check the response data

      // Update the media state with the newly created reply
      const updatedMedias = medias.map((media) => {
        if (media._id === mediaId) {
          const updatedComments = media.comments.map((comment) => {
            console.log(comment.replies);
            if (comment._id === commentId) {
              const updatedReplies = [...response.data.comment.replies];


              return {
                ...comment,
                replies: updatedReplies,
              };
            }
            return comment;
          });


          return {
            ...media,
            comments: updatedComments,
          };
        }
        return media;
      });

      setMediaList({ media: updatedMedias }); // Update the state with the updated media data
      // Show a success notification
      setSnackbarOpen(true);
      setSnackbarMessage('Reply added successfully');
      setReplyInput(''); // Clear the reply input field
      // Clear the comment input field
      setCommentInput('');
    } catch (error) {
      console.log(error);
    }

  };



  const handleDeleteReply = async (mediaId, commentId, replyId) => {
    try {
      // Send a request to the server to delete the reply
      const response = await axios.delete(
        `http://localhost:4000/api/v1/media/${mediaId}/comments/${commentId}/replies/${replyId}`
      );

      if (response.status === 200) {
        // Update the media state after successful deletion
        const updatedMedias = medias.map((media) => {
          if (media._id === mediaId) {
            const updatedComments = media.comments.map((comment) => {
              if (comment._id === commentId) {
                const updatedReplies = comment.replies.filter((reply) => reply._id !== replyId);

                return {
                  ...comment,
                  replies: updatedReplies,
                };
              }
              return comment;
            });

            return {
              ...media,
              comments: updatedComments,
            };
          }
          return media;
        });

        setMediaList({ media: updatedMedias });
        console.log('Reply deleted successfully');
      } else {
        console.log('Failed to delete reply');
      }
    } catch (error) {
      console.log(error);
    }
  };



  const deleteComment = async (mediaId, commentId) => {
    try {
      const response = await axios.delete(`http://localhost:4000/api/v1/media/${mediaId}/comments/${commentId}`);
      console.log(response.data.comments);
      return response.data;

    } catch (error) {
      throw new Error('Failed to delete comment');
    }
    ;
  };

  // ...

  const handleDeleteComment = async (mediaId, commentId) => {
    try {
      setIsDeleting(true);

      // Delete the comment on the server
      await deleteComment(mediaId, commentId);


      // Update the media state by removing the deleted comment from the corresponding media
      const updatedMedias = medias.map(media => {
        if (media._id === mediaId) {
          const updatedComments = media.comments.filter(comment => comment._id !== commentId);
          return {
            ...media,
            comments: updatedComments
          };
        }
        return media;
      });
      setMediaList({ media: updatedMedias });
      // Show a success notification
      setSnackbarOpen(true);
      setSnackbarMessage('Comment deleted succefully  ');

    } catch (error) {
      console.error(error.message);
    } finally {
      setIsDeleting(false);
    }
  };



  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };


  const handleLike = async ({ userId, videoId }) => {
    try {
      const response = await axios.post('http://localhost:4000/like/addlike', {
        userId,
        videoId,
      });

      // Assuming the response returns the updated like count
      const { likeCount: updatedLikeCount } = response.data;
      setLikeCount(updatedLikeCount);
      // Toggle like status
      setIsLiked((prevState) => !prevState);

      // Show snackbar
      setSnackbarOpen(true);
      setSnackbarMessage('Like added successfully');

    } catch (error) {
      console.error('Error liking:', error);
      // Handle error, e.g., show an error message
    }
  };



  const handleLikeComment = async (userIdParam) => {
    try {
      const response = await axios.post(`http://localhost:4000/api/media/${mediaId}/comments/${commentId}/like`, {
        userId: userIdParam
      });

      if (response.status === 200) {
        setLiked(true);
        // Optionally, you can update the UI or perform additional actions after a successful like
      }
    } catch (error) {
      // Handle any error that occurs during the request
      console.error('Failed to like comment', error);
    }
  };



  const handledislike = async ({ userId, videoId }) => {
    try {
      const response = await axios.post('http://localhost:4000/dislike', {
        userId,
        videoId,
      });

      // Assuming the response returns the updated like count
      const { likeCount: updatedLikeCount } = response.data;
      setLikeCount(updatedLikeCount);
      // Toggle like status
      setIsDisliked((prevState) => !prevState);

      // Show a dislike notification
      setSnackbarOpen(true);
      setSnackbarMessage('Unfortunaly Disliked ');

    } catch (error) {
      console.error('Error disliking:', error);
      // Handle error, e.g., show an error message
    }
  };



  const updateComment = (mediaId, commentId, updatedComment) => {
    setMediaList(prevMediaList => {
      const updatedMedias = [...prevMediaList];
      const mediaIndex = updatedMedias.findIndex(media => media._id === mediaId);
      const commentIndex = updatedMedias[mediaIndex].comments.findIndex(comment => comment._id === commentId);
      updatedMedias[mediaIndex].comments[commentIndex] = updatedComment; // Replace the old comment with the updated comment
      return updatedMedias;
    });
  };
  // Define the handleComment function





  const handleClick = () => {
    setIsVisible(true);
  };



  const handleInputChange = (event) => {
    setCommentInput(event.target.value);
  };




  const handleUpdateComment = async (mediaId, commentId, updatedText) => {
    try {
      const updatedCommentData = {
        text: updatedText,
      };

      // Send a PUT request to update the comment
      await axios.put(`http://localhost:4000/api/v1/media/${mediaId}/comments/${commentId}`, updatedCommentData);

      // Update the media state with the updated comment text
      const updatedMedias = medias.map(media => {
        if (media._id === mediaId) {
          const updatedComments = media.comments.map(comment => {
            if (comment._id === commentId) {
              return {
                ...comment,
                text: updatedText
              };
            }
            return comment;
          });
          return {
            ...media,
            comments: updatedComments
          };
        }
        return media;
      });

      setMediaList({ media: updatedMedias });

      // Show a success notification
      setSnackbarOpen(true);
      setSnackbarMessage('Comment Updated succefully  ');
      // Perform any necessary actions after updating the comment
      console.log('Comment updated successfully');
    } catch (error) {
      console.log(error);
    }
  };






  const openModal = (mediaId, index) => {
    setSelectedMediaId(mediaId);

    const existingComment = medias.find(media => media._id === mediaId)?.comments[index] || '';
    setCommentInput(existingComment.text);
    setExistingCommentId(existingComment._id);

    setShowModal(true);
  };




  const handleDelete = async (id) => {
    try {
      await axios.delete(`http://localhost:4000/api/v1/media/delete/${id}`);
      dispatch(deleteMedia(id));

      // Remove the deleted media from the medias state
      const deletedMedias = medias.filter((media) => media._id !== id);
      setMediaList({ media: deletedMedias });
    } catch (error) {
      console.log(error);
    }
  };





  return (

    <div className="container">

      <div className="flex justify-start">
        <div className="w-10/12 ">

          {
            medias?.map((media) => (
              <div key={media._id}>
                {/* Course Content */}
                <h1 className="font-bold text-3xl">{media.courseName}</h1>
                <p>{media.courseDescription}</p>
                <div>
                  <div className="flex justify-center space-x-4">
                    {media.videos.map((file, index) => (
                      <div key={index} className="mr-2">
                        {file.endsWith('.mp4') ? (
                          <div>
                            <video preload="auto" width="920" height="240" controls>
                              <source src={`${baseURL}${file}`} />
                              Your browser does not support the video tag.
                            </video>

                          </div>


                        ) : file.endsWith('.pdf') ? (
                          <Link to={`${baseURL}${file}`} target="_blank" rel="noopener noreferrer" className="pdf-link">
                            <div className="pdf-file">
                              <FaFilePdf className="pdf-file-icon" />
                              <div className="pdf-file-name">{media.name}.pdf</div>
                            </div>
                          </Link>
                        ) : (
                          <img src={`${baseURL}${file}`} alt="Media" />
                        )}
                        <br />
                        <h1 className="font-bold text-3xl">{media.name}</h1>
                        <br />
                        <div>
                          <div className="sequence mr-20">
                            <video preload="auto" width="130" height="130" controls>
                              <source src={`${baseURL}${file}#t=${sequence.startTime},${sequence.endTime}`} />
                              Your browser does not support the video tag.
                            </video>
                            <div className="sequence-label">00-30</div>
                          </div>
                          <div className="sequence mr-20">
                            <video preload="auto" width="130" height="130" controls>
                              <source src={`${baseURL}${file}#t=${sequence.startTime2},${sequence.endTime2}`} />
                              Your browser does not support the video tag.
                            </video>
                            <div className="sequence-label">30-45</div>
                          </div>
                          <div className="sequence">
                            <video preload="auto" width="130" height="130" controls>
                              <source src={`${baseURL}${file}#t=${sequence.startTime3},${sequence.endTime3}`} />
                              Your browser does not support the video tag.
                            </video>
                            <div className="sequence-label">46-60</div>
                          </div>
                        </div>



                        <br />
                        <div className="videos-icons font-bold pl-60 ml-40 d-flex">
                          <div style={{
                            backgroundColor: "#f5f5f5",
                            borderRadius: "10px", display: "inline-block"
                          }}>
                            <button className="icon-button">
                              <FaThumbsUp
                                style={{

                                  padding: "10px",
                                  color: "gray",
                                  height: "100%",
                                  width: "90%",
                                  color: isLiked ? "blue" : "gray",
                                }}
                                onClick={() => handleLike({ userId: User_id, videoId: media._id })}
                              />
                            </button>

                            <button className="icon-button ">
                              <FaThumbsDown
                                style={{

                                  padding: "10px",
                                  color: "gray",
                                  height: "100%",
                                  width: "90%",
                                  color: IsDisliked ? "blue" : "gray",
                                }}
                                onClick={() => handledislike({ userId: User_id, videoId: media._id })}
                              />
                            </button>
                          </div>

                         
                        </div>





                        <div className="video-actions">

                          <div className="action-container">


                            <Snackbar
                              open={snackbarOpen}
                              message={snackbarMessage}
                              autoHideDuration={3000} // Adjust the duration as needed
                              onClose={handleSnackbarClose}
                              anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                              }}

                            />




                            <div className="action-space"></div>


                          </div>
                        </div>
                        <br />
                        <br />
                        <div style={{ backgroundColor: "white", borderRadius: "10px", padding: "10px" }}>


                          {showFullResume ? (
                            <div >
                              {media.resume}
                            </div>
                          ) : (
                            <div style={{ maxHeight: "3.2em", overflow: "hidden" }}>
                              {media.resume}
                            </div>
                          )}
                          {media.resume.length > 100 && (
                            <button onClick={toggleShowFullResume}>
                              .... {showFullResume ? "Show less" : "Show more"}
                            </button>
                          )}


                        </div>

                        {/* Recommend Course Label */}
                        <div className="flex mr-8 mt-8">
                          <div className="bg-gradient-to-r from-green-800 to-green-200 px-4 py-2 rounded-lg flex items-center " onClick={navigaterecommendationpage}>
                            <FaThumbtack className="text-white mr-2" /> {/* Icon */}
                            <h2 className="text-white text-xl font-semibold" >Recommend Courses</h2>
                          </div>
                        </div>

                        {/* Comments Section */}
                        {!selectedMedia && (
                          <div className="comments">
                            {/* <p className="font-bold  text-blue-400">Comments of {media.name}:</p>    <br></br> */}
                            <div >
                              <div >
                                <textarea
                                  value={commentInput}
                                  onChange={handleInputChange}
                                  placeholder="Write a comment..."
                                  className="comment-textarea"
                                  required
                                ></textarea>
                                <div className="modal-buttons">
                                  <button onClick={() => handleComment(media._id)} className="comment-button bg-Teal">
                                    Add Comment
                                  </button>

                                </div>

                              </div>
                              <br />

                              <div class="comment-container">
                                <button class="comment-label" onClick={() => { setSelectedMedia(media); }}>
                                  All Comments ({media.comments.length})
                                </button>
                                {!showComments && <FaArrowCircleDown class="comment-icon" onClick={() => { setShowComments(true); getComment(media._id); }} />}
                                {showComments && <FaArrowCircleUp class="comment-icon" onClick={() => { setShowComments(false); }} />}
                              </div>


                            </div>
                            <br></br>
                            <div className="comment-list">
                              {showComments && media.comments.map((comment, commentIndex) => (
                                <div className="comment-content" key={commentIndex}>
                                  <div className="comment-avatar">
                                    <img src={hero} alt="User Avatar" className="avatar-image rounded-avatar" />
                                  </div>
                                  <div className="comment-text-container">
                                    <div className="comment-details">
                                      <p className="comment-username">{`${comment?.commentsender?.firstName} ${comment?.commentsender?.lastName}`}</p>
                                      <p className="comment-date ml-5">{comment.date}</p>
                                    </div>
                                    <p className="comment-text">{comment.text}</p>
                                    <div className="comment-actions-comment">

                                      <button className="comment-action-comment">

                                        <FaHeart style={{ color: 'red', height: "100%", width: "80%" }} />
                                      </button>

                                      <button className="comment-action-comment">

                                        <FaThumbsDown style={{ height: "100%", width: "80%" }} />
                                      </button>

                                      <button className="comment-action-comment" onClick={() => openModal(media._id, commentIndex)}>
                                        <FaComment style={{ height: "100%", width: "80%" }} />
                                      </button>

                                      <button className="comment-action-comment" onClick={() => handleDeleteComment(media._id, comment._id)}>
                                        <FaTrash style={{ height: "100%", width: "80%" }} />
                                      </button>


                                    </div>
                                    <div className="replies">
                                      <button onClick={() => toggleReplies(comment._id)}>
                                        {repliesVisible.includes(comment._id) ? 'Hide Replies' : 'Show Replies'}
                                      </button>

                                      {repliesVisible.includes(comment._id) && (

                                        <div className="replies-content">

                                          {comment?.replies?.map((reply) => (
                                            <div key={reply._id} className="reply flex items-center">
                                              <img src={hero} alt="User Avatar" className="avatar-image rounded-avatar mr-2" />
                                              <div className="flex flex-col">
                                                <p className="text-xl"> {reply.text}</p>
                                                <p className="comment-date mt-auto">{reply.date}</p>
                                                {/* Delete reply button */}
                                                <button onClick={() => handleDeleteReply(media._id, comment._id, reply._id)}>Delete Reply</button>

                                              </div>
                                            </div>
                                          ))}



                                          <div className="reply-input">
                                            <input
                                              type="text"
                                              value={replyInput}
                                              onChange={(e) => setReplyInput(e.target.value)}
                                              placeholder="Write a reply..."
                                              className="border border-gray-300 rounded py-1 px-2"
                                            />
                                            <button onClick={() => handleReply(media._id, comment._id, replyInput)}>Reply</button>
                                          </div>
                                        </div>
                                      )}

                                    </div>

                                    <div>


                                      {comment._id === existingCommentId && (
                                        <button onClick={() => handleUpdateComment(media._id, comment._id, commentInput)}>
                                          Update Comment
                                        </button>
                                      )}


                                    </div>
                                    {/* Comment content */}


                                  </div>



                                </div>

                              ))}

                            </div>
                          </div>
                        )}




                        <br />
                        {/* Comments Label */}

                        <br />
                        {selectedMedia && (
                          <div className="comment-modal animate-popup">
                            <div className="modal-content animate-scale bg-white shadow-lg rounded-lg p-4">
                              <h2 className="text-center font-bold text-xl mb-2">Comments of {selectedMedia.name}</h2>
                              <h2 className="text-center mb-4">({media.comments.length}) Comments</h2>

                              <div className="comment-list" style={{ maxHeight: '200px', overflowY: 'auto' }}>
                                {selectedMedia.comments.length > 0 ? (
                                  selectedMedia.comments.map((comment, commentIndex) => (
                                    <div className="comment-content flex items-start mb-4" key={commentIndex}>
                                      <div className="comment-avatar">
                                        <img src={hero} alt="User Avatar" className="avatar-image rounded-avatar w-8 h-8" />
                                      </div>
                                      <div className="comment-text-container ml-4">
                                        <p className="comment-text">{comment.text}</p>
                                        <p className="comment-date text-gray-500 text-sm">Commented on: {comment.date}</p>
                                      </div>
                                      <div className="comment-actions ml-auto flex space-x-2">
                                        <button className="comment-action" onClick={handleLikeComment}>
                                          <FaHeart style={{ color: 'red' }} />
                                        </button>
                                        <button className="comment-action">
                                          <FaThumbsDown style={{ color: 'gray' }} />
                                        </button>
                                        <button className="comment-action-comment" onClick={() => handleDeleteComment(media._id, comment._id)}>
                                          <FaTrash style={{ height: "100%", width: "80%" }} />
                                        </button>
                                      </div>
                                    </div>
                                  ))
                                ) : (
                                  <p>No comments available.</p>
                                )}
                              </div>
                              <button onClick={() => setSelectedMedia(null)} className="close-button mt-4 bg-gray-200 hover:bg-gray-300 py-2 px-4 rounded">
                                Close
                              </button>
                            </div>
                          </div>
                        )}

                        <br />


                      </div>
                    ))}

                  </div>
                </div>

                <div className="text-right px-9">
                  <div>
                    <FaTrash onClick={() => handleDelete(media._id)} style={{ width: 'auto' }} />
                  </div>
                </div>








              </div>


            ))}



        </div>


      </div>


    </div>

  );


};
export default UploadsList;
