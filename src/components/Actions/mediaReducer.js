import { DELETE_MEDIA } from "../Actions/types";
import React from 'react'
  

const initialState = {
    medias: [], // Your initial state
  };
  
  const mediaReducer = (state = initialState, action) => {
    switch (action.type) {
      case DELETE_MEDIA:
        const updatedMedias = state.medias.filter(media => media._id !== action.payload);
        return {
          ...state,
          medias: updatedMedias,
        };
      // Handle other actions and return the state accordingly
      default:
        return state;
    }
  };
  
  
  export default mediaReducer
  