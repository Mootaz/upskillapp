import {DELETE_COURSE_FAILURE,DELETE_COURSE_REQUEST,DELETE_COURSE_SUCCESS}  from "../Actions/types"
import { createStore } from 'redux';

const initialState = {
    courses: [],
    loading: false,
    error: null
  };
  
  const courseReducer = (state = initialState, action) => {
    switch (action.type) {
      case DELETE_COURSE_REQUEST:
        return {
          ...state,
          loading: true,
          error: null
        };
      case DELETE_COURSE_SUCCESS:
        const filteredCourses = state.courses.filter(course => course._id !== action.payload);
        return {
          ...state,
          courses: filteredCourses,
          loading: false,
          error: null
        };
      case DELETE_COURSE_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload
        };
        case ADD_COURSE:
      return {
        ...state,
        courses: [...state.courses, action.payload]
      };
      default:
        return state;
    }
  };
  
// Create store
const store = createStore(coursesReducer);
  export default courseReducer;