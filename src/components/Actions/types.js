export const DELETE_COURSE_REQUEST = 'DELETE_COURSE_REQUEST';
export const DELETE_COURSE_SUCCESS = 'DELETE_COURSE_SUCCESS';
export const DELETE_COURSE_FAILURE = 'DELETE_COURSE_FAILURE';

const ADD_COURSE = 'ADD_COURSE';

export const addCourseAction = (newCourse) => {
  return {
    type: ADD_COURSE,
    payload: newCourse
  }
}

// actions/types.js
export const DELETE_MEDIA = 'DELETE_MEDIA';
