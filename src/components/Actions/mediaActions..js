import axios from "axios";
import { DELETE_MEDIA } from "./types";

export const deleteMedia = (id) => async (dispatch) => {
  try {
    await axios.delete(`http://localhost:4000/api/v1/media/delete/${id}`);
    // Dispatch the delete action to update the state
    dispatch({ type: DELETE_MEDIA, payload: id });
  } catch (error) {
    console.log(error);
  }
};