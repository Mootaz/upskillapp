import React, { useState } from 'react';
import axios from 'axios';
import { useSelector } from "react-redux";
import { useParams } from 'react-router-dom';
import { selectSessionUser } from '../../../../topics/Session/slice';

const AddRecommandation = () => {

    const { courseId } = useParams();
    const currentUser = useSelector(selectSessionUser);
    const [User_id, setUser_id] = useState(currentUser._id);

    const handleAddRecommendation = async () => {
        try {
            await axios.post(`http://localhost:4000/users/${User_id}/recommendations/${courseId}`);
            alert('Recommendation added successfully');
        } catch (error) {
            console.error(error);
            alert('Failed to add recommendation');
        }
    };
    return (
        <div>
           
            <button onClick={handleAddRecommendation} className='text-xl '>Add Recommendation</button>
        </div>
    )
}

export default AddRecommandation
