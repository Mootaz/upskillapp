import React, { useState, useEffect } from 'react';
import axios from 'axios';

import { Route } from "react-router-dom";


const Recommandation = () => {

    const [courses, setCourses] = useState([]);

    const [searchValue, setSearchValue] = useState('');









    useEffect(() => {
        getCourses();    
    }, [searchValue]);

    const getCourses = async () => {
        try {
          const response = await axios.get('http://localhost:4000/course/getallcourse');
      
          const filteredCourses = response.data.filter(course => {
            const titleMatch = course.title.toLowerCase().includes(searchValue.toLowerCase());
            const categoryMatch = course.category.toLowerCase().includes(searchValue.toLowerCase());
            
            return titleMatch || categoryMatch;
          });
      
          setCourses(filteredCourses);
        } catch (err) {
          console.error(err);
        }
      };
      








      return (
        <div>
          <div className="flex justify-center">
            <input
              type="text"
              placeholder="Search by title"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              className="w-64 p-2 border border-gray-300 rounded-md"
            />
            <button>
                Submit
            </button>
          </div>
      
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 mt-4">
            {courses.map((course) => (
              <div key={course.id} className="bg-white rounded-md shadow-md p-4">
                <div className="flex justify-center">
                  <img src={`/coursefile/${course.image}`} className="w-64 h-40 object-cover" alt={course.title} />
                </div>
                <div className="mt-4">
                  <h2 className="text-xl font-bold">{course.title}</h2>
                  <p className="text-gray-600">{course.description}</p>
                  <p className="text-gray-600">Author: {course.category}</p>
                  <p className="text-gray-600">Price: {course.price}</p>
                  <p className="text-gray-600">Rating: {course.rating}</p>
                  <div className="flex items-center">
                    <svg
                      aria-hidden="true"
                      className="w-5 h-5 text-yellow-500 fill-current"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                     
                    </svg>
                   
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      );

};

export default Recommandation;
