import React from 'react'
import Courseimage from '../../../../assets/About.jpg'

const CourseRecommandation = () => {
  return (
    <section className="py-8 flex flex-col-reverse lg:flex-row items-center">
      <div className="lg:w-2/3 lg:mr-8 flex items-center">
        <div className="ml-10 mb-20  bg-gradient-to-r from-blue-400 to-indigo-500 rounded-lg px-8 py-6">
          <h2 className="text-4xl font-bold text-center text-white mb-6">Welcome to Our Course</h2>
          <div className="grid gap-4 text-lg text-gray-100">
            <div className="flex items-center">
              <span className="mr-4 bg-white text-blue-600 rounded-full p-2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 9.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
              <span>Best instructors</span>
            </div>
            <div className="flex items-center">
              <span className="mr-4 bg-white text-blue-600 rounded-full p-2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M7.293 7.293a1 1 0 011.414 0L10 9.586l1.293-1.293a1 1 0 111.414 1.414l-2 2a1 1 0 01-1.414 0l-2-2a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
              <span>Enhance skills </span>
            </div>
            <div className="flex items-center">
              <span className="mr-4 bg-white text-blue-600 rounded-full p-2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M7.293 7.293a1 1 0 011.414 0L10 9.586l1.293-1.293a1 1 0 111.414 1.414l-2 2a1 1 0 01-1.414 0l-2-2a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
              <span>Join our community </span>
            </div>
            <div className="flex items-center">
              <span className="mr-4 bg-white text-blue-600 rounded-full p-2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 9.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
              <span>Get certified </span>
            </div>
          </div>
        </div>



        <img
          src={Courseimage}
          alt="Course"
          className="rounded-md shadow-lg mb-4 ml-40"
          style={{ width: '700px', height: '500px' }}
        />
      </div>
    </section>
  );


}

export default CourseRecommandation
