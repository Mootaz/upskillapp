import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSelector } from "react-redux";
import { selectSessionUser } from '../../../../topics/Session/slice';
import { Link } from 'react-router-dom';
import './UserRecommandation.css';



const UserRecommandation = () => {

    const [recommendations, setRecommendations] = useState([]);
    const currentUser = useSelector(selectSessionUser);
    const [allCourses, setAllCourses] = useState([]);
    const [User_id, setUser_id] = useState(currentUser._id);
    const [selectedTitle, setSelectedTitle] = useState('All');
    const [showSuccessNotification, setShowSuccessNotification] = useState(false);
    const [selectedCourseId, setSelectedCourseId] = useState(null);
    const [selectedCourse, setSelectedCourse] = useState(null); // Store the selected course object
    const [canceledRecommendations, setCanceledRecommendations] = useState([]);



    useEffect(() => {

        fetchRecommendations();
        fetchAllCourses();
    }, [User_id]);


    const handleAddRecommendation = async (courseId) => {
        try {
            await axios.post(`http://localhost:4000/users/${User_id}/recommendations/${courseId}`);

            // Update the recommendations state locally with the newly added course
            const selectedCourse = allCourses.find((course) => course._id === courseId);
            setRecommendations((prevRecommendations) => [...prevRecommendations, selectedCourse]);

            succesmessage();
        } catch (error) {
            console.error(error);
            alert('Failed to add recommendation');
        }
    };


    const handleCourseChange = async (event) => {
        const courseId = event.target.value;
        const selectedCourse = allCourses.find((course) => course._id === courseId);
        setSelectedCourseId(courseId); // Store the selected course ID
        setSelectedCourse(selectedCourse); // Store the selected course object

        try {
            // Fetch the recommended courses for the selected course ID
            const response = await axios.get(`http://localhost:4000/users/${User_id}/recommendations`);
            setRecommendations(response.data); // Assuming the API response returns an array of recommended courses
        } catch (error) {
            console.error(error);
        }
    };


    const fetchRecommendations = async () => {
        try {
            const response = await axios.get(`http://localhost:4000/users/${User_id}/recommendations`);
            setRecommendations(response.data);
        } catch (error) {
            console.error(error);
        }
    };
    const fetchAllCourses = async () => {
        try {
            const response = await axios.get('http://localhost:4000/course/getallcourse');
            setAllCourses(response.data);
        } catch (error) {
            console.error(error);
        }
    };


    const handleDeleteRecommendation = async (courseId) => {
        try {
            await axios.delete(`http://localhost:4000/users/${User_id}/recommendations/${courseId}`);
            // After successful deletion, fetch the updated recommendations
            fetchRecommendations();
            alert('Recommendation canceled');
        } catch (error) {
            console.error(error);
            alert('Failed to delete recommendation');
        }
    };

    const succesmessage = () => {
        setShowSuccessNotification(true);
        setTimeout(() => {
            setShowSuccessNotification(false);
        }, 1000)
    };




    return (
        <div>
            <h3 className="font-bold text-3xl py-4">Recommendation List</h3>
            <div className="dropdown-container mb-20">
                <select id="title" onChange={handleCourseChange}>
                    <option value="">Select a course to recommend</option>
                    {allCourses.map((course) => (
                        <option key={course._id} value={course._id}>
                            {course.title}

                        </option>
                    ))}
                </select>
            </div>
            {selectedCourse && (
                <div className="card">
                    <img src={`/coursefile/${selectedCourse.image}`} alt={selectedCourse.title} style={{ width: '170px', height: '110px' }} />
                    <h4>{selectedCourse.title}</h4>
                    <p>Description:{selectedCourse.description}</p>
                    <p>Category: {selectedCourse.category}</p>
                    <p>Price: {selectedCourse.price}</p>
                    <div className='flex space-x-5 '>
                        <button
                            className="text-lg  bg-gradient-to-r from-blue-900 to-blue-400 text-white font-bold py-1 px-2 rounded transition-colors duration-300 ease-in-out transform hover:scale-105"
                            onClick={() => {
                                if (selectedCourseId) {
                                    succesmessage();
                                    handleAddRecommendation(selectedCourseId);
                                }
                            }}
                        >
                            Recommend
                        </button>

                    </div>
                    <div className="success-notification" style={{ display: showSuccessNotification ? 'block' : 'none' }}>
                        Recommendation successful!
                    </div>
                </div>
            )}

            <h1 className='text-white font-bold text-2xl '>Recommended Courses</h1>
            <div className="cards-container">
                <div className="cards-container">
                    {recommendations.map((course) => {
                        // Check if the course is in the canceledRecommendations state
                        if (!canceledRecommendations.includes(course._id)) {
                            return (
                                <div className="card" key={course._id}>
                                    <img
                                        className="px-1 custom-image"
                                        src={`/coursefile/${course.image}`}
                                        alt={course.title}
                                        style={{ width: '170px', height: '110px' }}
                                    />

                                    <Link to={`/payment`} key={course._id}>
                                    <h4 className='text-center text-white text-4xl font-bold'> {course.title}</h4>
                                    </Link>
                                    <p className='font-bold'> {course.title}</p>
                                    <p>Description: {course.description}</p>
                                    <p>Category: {course.category}</p>
                                    <p>Price: {course.price}</p>
                                    <button
                                        className="text-lg  bg-gradient-to-r from-indigo-700 to-indigo-300 text-white font-bold py-1 px-1 rounded transition-colors duration-300 ease-in-out transform hover:scale-105"
                                        onClick={() => handleDeleteRecommendation(course._id)}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            );
                        }
                        return null; 
                    })}
                </div>

            </div>
        </div>
    );


}

export default UserRecommandation
