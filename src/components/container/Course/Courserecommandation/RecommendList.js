import React from 'react';
import AddRecommandation from './AddRecommandation';
import UserRecommandation from './UserRecommandation';
import './RecommandationList.css';
import CourseRecommandation from './CourseRecommandation';
import { useParams } from 'react-router-dom';

const RecommendList = () => {
  const { courseId } = useParams();

  return (
    <div className="min-h-screen flex flex-col justify-center items-center bg-gradient-to-b from-blue-500 to-indigo-600">
      {/* Background Animation (Hero Pattern) */}
      <div className="absolute top-0 left-0 w-full h-full bg-hero-pattern bg-opacity-40 z-0" />

      {/* Content Container */}
      <div className="z-10">
        {/* Course Recommendation Section */}
        <div >
          <CourseRecommandation />
        </div>

        {/* User Recommendation Section */}
        <div className="my-8 flex flex-col items-center justify-center">
          <h2 className="text-3xl font-semibold text-white mb-4">User Recommendation</h2>
          <UserRecommandation />
        </div>

      </div>
    </div>
  );
};

export default RecommendList;



