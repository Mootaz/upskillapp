import React, { useState, useEffect } from 'react';
import './DisplayCourse.css'; // Import the CSS file
import hero from "../../../assets/hero.png";
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { deleteCourse } from "../../../components/Actions/courseActions";
import { Link } from 'react-router-dom';
import Uploadvideo from '../../../Uploadvideo';
import Footer from '../Footer';
import { Navbar } from 'react-bootstrap';

const DisplayCourse = () => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [price, setPrice] = useState('');

    const [image, setImage] = useState('');

    const [message, setMessage] = useState('');
    const [courses, setCourses] = useState([]);
    const [uploading, setUploading] = useState(false)
    const [deletedCourse, setDeletedCourse] = useState(null);
    const [showUpdateModal, setShowUpdateModal] = useState(false);
    const [updateCourseId, setUpdateCourseId] = useState('');
    const [selectedCourse, setSelectedCourse] = useState(null);


    useEffect(() => {
        getCourses();
    }, []);


    const getCourses = async () => {
        try {
            const response = await axios.get('http://localhost:4000/course/getallcourse');
            setCourses(response.data);
           
        } catch (err) {
            console.error(err);
        }
    };

    const uploadFileHandler = async (e) => {
        const file = e.target.files[0]
        const formData = new FormData()
        formData.append('image', file)
        setUploading(true)
    
        try {
          const config = {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          }
    
          const { data } = await axios.post('http://localhost:4000/api/upload', formData, config)
       
          console.log(data)
          setImage(data)
          setUploading(false)
        } catch (error) {
          console.error(error)
          setUploading(false)
        }
      }


  const handleCourseClick = (course) => {
    setSelectedCourse(course);

  };

    const dispatch = useDispatch();
    const coursesdelete = useSelector(state => state.courses);
    const handleDelete = async (id) => {
        try {
            await axios.delete(`http://localhost:4000/course/deletecourse/${id}`);
            setCourses(courses.filter((course) => course._id !== id));
            dispatch(deleteCourse(id));
        } catch (err) {
            console.error(err);
        }
    };

    const updateCourse = (id, course) => {
        setUpdateCourseId(id);
        setTitle(course.title);
        setDescription(course.description);
        setCategory(course.category);
        setPrice(course.price);
        setShowUpdateModal(true);
        setImage(course.image)
    };

    const handleUpdateSubmit = async (event) => {
        event.preventDefault();
   
        try {
            const response = await axios.put(`http://localhost:4000/course/updatecourse/${updateCourseId}`, {
                title,
                description,
                category,
                price,
                image 

            });
            setMessage(response.data.message);
            setShowUpdateModal(false);
            getCourses();

        } catch (err) {
            console.error(err);
        }
    };


    const handleUpdateCancel = () => {
        setShowUpdateModal(false);
    };


    const handleDeleteCancel = () => {
        setDeletedCourse(null);
    };

    const refresh = () => {
        window.location.reload();
    };
    return (
        <div style={{backgroundColor:"#f5f5f5"}}>
        <div className="course-list">
          
           <Navbar/>
            <ul class="display-list">
                {courses.map((course) => (
                    <li class="course-item" key={course._id} onClick={() => handleCourseClick(course)}>
                       
                        <div class="course-image">
                            <img src={`/coursefile/${course.image}`} alt={course.title}  />
                        </div>
                        <div>
                            <div>
                        <Link to={`/courses/${course._id}/uploadvideo`} key={course._id}>
                            <h3 class="course-title font-bold">{course.title}</h3>
                            </Link>
                            </div>
                            <p class="course-description">Description: {course.description}</p>
                            <p class="course-author">Category: {course.category}</p>
                            <p class="course-price">Price: {course.price}$</p>


                            <div class="flex items-center">
                                <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>First star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                                <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Second star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                                <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Third star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                                <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fourth star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                                <svg aria-hidden="true" class="w-5 h-5 text-gray-300 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fifth star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                            </div>
                            </div>
                            <div class="course-actions">
                                <button onClick={() => updateCourse(course._id, course)}>Update</button>
                                <button onClick={() => handleDelete(course._id)}>Delete</button>
                            </div>
                       
                       
                    </li>
                       

                ))}
                 {/* {selectedCourse && <Uploadvideo course={selectedCourse} />} */}
            </ul>



            {/* Update Course Modal */}
            {showUpdateModal && (
                <div className="modal">
                    <form onSubmit={handleUpdateSubmit} className="modal-content">
                        <h2 className='text-green-300 font-bold text-3xl'>Update Course</h2>
                        <br />
                     <label>
                        image
                            <input type="file"  onChange={(uploadFileHandler)} />
                            </label>
                        <label>
                            Title:
                            <input type="text" value={title} onChange={(event) => setTitle(event.target.value)} />
                        </label>

                        <br />
                        <label>
                            Description:
                            <input
                                type="text"
                                value={description}
                                onChange={(event) => setDescription(event.target.value)}
                            />
                        </label>
                        <br />
                        <label>
                            Category:
                            <input type="text" value={category} onChange={(event) => setCategory(event.target.value)} />
                        </label>
                        <br />
                        <label>
                            Price:
                            <input type="text" value={price} onChange={(event) => setPrice(event.target.value)} />
                        </label>



                        <button type="submit">Update Course</button>
                        <button type="reset" onClick={handleUpdateCancel}>Cancel</button>
                        {message && <p>{message}</p>}
                    </form>
                </div>
            )}
         
        </div>
        <br/>
        <div className='mt-60'>
        <Footer/> 
        </div>
       
        </div>
    )
}

export default DisplayCourse
