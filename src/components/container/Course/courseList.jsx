import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './CourseList.css'; // Import the CSS file
import { useNavigate } from "react-router-dom";
import DropFileInput from '../../../components/drop-file-input/DropFileInput';
import UploadButton from '../../../components/upload-button/UploadButton';
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage"
import { storage, db } from './firebase';
import { doc, setDoc } from "firebase/firestore"
import Navbar from '../../Navbar/Navbar';
import Footer from '../Footer';
import { selectSessionUser } from '../../../topics/Session/slice';
import { useSelector } from "react-redux";



const CourseList = () => {


  const [file, setFile] = useState(null);
  const [courses, setCourses] = useState([]);
  const [selectedFile, setSelectedFile] = useState(null);
  const currentUser = useSelector(selectSessionUser);

  const [newCourse, setNewCourse] = useState({
    title: '',
    description: '',
    category: '',
    price: '',
    rating: '',
    image: '',
    User_id: currentUser
  });

  const [image, setImage] = useState('')

  const [uploading, setUploading] = useState(false)

  const [formData, setFormData] = useState({
    title: '',
    description: '',
    category: '',
    price: '',
    rating: '',
    image: ''
  });





  useEffect(() => {

  }, []);


  const navigate = useNavigate();
  function displaycourse() {
    navigate('/displaycourse')
  }

  const onFileChange = (files) => {
    const currentFile = files[0]
    setFile(currentFile)
    console.log(files);
  }

  const uploadToDatabase = (url) => {
    let docData = {
      mostRecentUploadURL: url,
      username: "jasondubon"
    }
    const userRef = doc(db, "users", docData.username)
    setDoc(userRef, docData, { merge: true }).then(() => {
      console.log("successfully updated DB")
    }).catch((error) => {
      console.log("errrror")
    })
  }


  const handleClick = () => {
    if (file === null) return;
    const fileRef = ref(storage, `videos/${file.name}`)
    const uploadTask = uploadBytesResumable(fileRef, file)

    uploadTask.on('state_changed', (snapshot) => {
      let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      console.log(progress)
    }, (error) => {
      console.log("error :(")
    }, () => {
      console.log("success!!")
      getDownloadURL(uploadTask.snapshot.ref).then(downloadURL => {
        uploadToDatabase(downloadURL)
        console.log(downloadURL)
      })
    })
  }

  const uploadFileHandler = async (e) => {
    const file = e.target.files[0]
    const formData = new FormData()
    formData.append('image', file)
    setUploading(true)

    try {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }

      const { data } = await axios.post('http://localhost:4000/api/upload', formData, config)
      setNewCourse({ ...newCourse, image: data })
      console.log(data)
      setImage(data)
      setUploading(false)
    } catch (error) {
      console.error(error)
      setUploading(false)
    }
  }
  const addCourse = async () => {
    try {
      const response = await axios.post('http://localhost:4000/course/addcourse', newCourse);
      setCourses([...courses, response.data]);
      setNewCourse({
        title: '',
        description: '',
        category: '',
        price: '',
        image: '',
        User_id: currentUser
      })

    } catch (err) {
      console.error(err);
    }


  };






  return (
    <div style={{backgroundColor: "#f5f5f5"}}>
    <div className="course-list " >
     
      <h1 id="animated-text" className="cd-headline clip is-full-width">
        <span className="cd-words-wrapper" style={{ width: "266px", overflow: "hidden" }}>

          <b className="is-visible mr-20 pr-20">Add Upskill Courses</b>
        </span>
      </h1>

      <form className="add-course" onSubmit={addCourse}>

        <input type="file" id='image-file'
          label='Choose File'
          custom

          onChange={(uploadFileHandler)} />

            <br/>
        <div class="relative z-0 w-full mb-6 group py-5">
          <input
            class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" Title "
            type="text"
            value={newCourse.title}
            onChange={(event) =>
              setNewCourse({ ...newCourse, title: event.target.value })
            }
          />
          <label for="floating_email" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"></label>
        </div>

    
        <div class="relative z-0 w-full mb-6 group py-5">
          <input
            class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" Description "
            type="text"
            value={newCourse.description}
            onChange={(event) =>
              setNewCourse({ ...newCourse, description: event.target.value })
            }
          />
          <label for="floating_email" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"></label>
        </div>
        <div class="relative z-0 w-full mb-6 group py-5">
        
          <input
            class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" Category "
            type="text"
            value={newCourse.category}
            onChange={(event) =>
              setNewCourse({ ...newCourse, category: event.target.value })
            }
          />
          <label for="floating_email" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"></label>
        </div>
        <div class="relative z-0 w-full mb-6 group py-5">
          <input
          class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" Price "
            type="text"
            value={newCourse.price}
            onChange={(event) =>
              setNewCourse({ ...newCourse, price: event.target.value })
            }
          />
            <label for="floating_email" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"></label>
      </div>
        <br />

        <button type="submit" >Add Course</button>
        <button className='List-course' onClick={displaycourse}  >List Course</button>
      </form>


      {/* Your other form inputs
      <DropFileInput onFileChange={(files) => onFileChange(files)} />
      <br />
      <UploadButton onClick={() => handleClick()} />
      <br /> */}


    </div>
    <Footer/>
    </div>

  );
};

export default CourseList;
