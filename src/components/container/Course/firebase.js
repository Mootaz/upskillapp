import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage"
import { getFirestore } from "@firebase/firestore"

const firebaseConfig = {
    apiKey: "AIzaSyDy8W0c5HxTzEFRout_eu9yhP3_9wb5mSY",
    authDomain: "upskillupload-148d7.firebaseapp.com",
    projectId: "upskillupload-148d7",
    storageBucket: "upskillupload-148d7.appspot.com",
    messagingSenderId: "586976248558",
    appId: "1:586976248558:web:5e1bd89a33c52bd09f29ff"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const storage = getStorage(app)
export const db = getFirestore(app)