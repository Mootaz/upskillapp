import React, { useEffect, useState } from 'react';
import './Quiz.css'; // Import the CSS file
import axios from 'axios';
import {
    selectSessionUser,
} from "../../../topics/Session/slice";
import { useSelector } from "react-redux";
import Snackbar from '@mui/material/Snackbar';

const Quiz = () => {

    const API_BASE_URL = 'http://localhost:4000';
    const [questions, setQuestions] = useState([]);
    const [question, setQuestion] = useState('');
    const [options, setOptions] = useState(['', '', '']); // Initialize options with empty values
    const [correctOption, setCorrectOption] = useState(0); // Set initial value to 0
    const currentUser = useSelector(selectSessionUser);
    const [User_id, setUser_id] = useState(currentUser._id);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [questionList, setQuestionList] = useState([]); // Store the list of questions

    useEffect(() => {
        fetchQuestions();
    }, []);



    const handleAddQuestion = () => {
        const newQuestion = {
            question: question,
            options: options,
            correctOption: correctOption,
        };

        setQuestions((prevQuestions) => [...prevQuestions, newQuestion]);
        setQuestion('');
        setOptions(['', '', '']);
        setCorrectOption(0);
    };
    const handleSnackbarClose = () => {
        setSnackbarOpen(false);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const questionData = {
            question: question,
            options: options,
            correctOption: correctOption,
            User_id: currentUser,
        };

        try {
            const newQuestion = await createQuestion(questionData);
            setSnackbarOpen(true);
            setSnackbarMessage('Question created successfully');
            console.log('New question:', newQuestion);
            setQuestion('');
            setOptions(['', '', '']);
            setCorrectOption(0);
            setQuestionList((prevQuestionList) => [...prevQuestionList, newQuestion]); // Add new question to the list
        } catch (error) {
            console.error(error);
        }

        // TODO: Handle submission of quiz
        console.log('Quiz submitted:', questions);
    };


    const handleOptionChange = (index, value) => {
        const updatedOptions = [...options];
        updatedOptions[index] = value;
        setOptions(updatedOptions);
    };

    const fetchQuestions = async () => {
        try {
            const allQuestions = await getAllQuestions();
            setQuestionList(allQuestions); // Set the list of questions
        } catch (error) {
            console.error(error);
        }
    };

    const getAllQuestions = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/quiz/getallquiz`);
            return response.data;
        } catch (error) {
            throw new Error('Error retrieving questions');
        }
    };

    const createQuestion = async (questionData) => {
        try {
            const response = await axios.post(`${API_BASE_URL}/quiz/addquiz`, questionData);
            return response.data;
        } catch (error) {
            throw new Error('Error creating question');
        }
    };

    async function getQuestionById(questionId) {
        try {
            const response = await axios.get(`${API_BASE_URL}/quiz/getquizbyid/${questionId}`);
            return response.data;
        } catch (error) {
            throw new Error('Error retrieving question');
        }
    }

    const handleAddOption = () => {
        setOptions([...options, '']);
    };

    return (
        <div className="quiz-container">
            <h2>Create Quiz</h2>
            <form onSubmit={handleSubmit}>
                {questions.map((question, index) => (
                    <div key={index}>
                        <h3>Question {index + 1}</h3>
                        <p>{question.question}</p>
                        <ul>
                            {question.options.map((option, optionIndex) => (
                                <li key={optionIndex}>{option}</li>
                            ))}
                        </ul>
                        <p>Correct Option: {question.correctOption + 1}</p>
                    </div>
                ))}
                <div>
                    <label>Question:</label>
                    <input type="text" value={question} onChange={(event) => setQuestion(event.target.value)} />
                </div>
                <div>
                    <label>Options:</label>
                    {options.map((option, index) => (
                        <input
                            type="text"
                            value={option}
                            onChange={(event) => handleOptionChange(index, event.target.value)}
                            key={index}
                        />
                    ))}
                </div>
                <div>
                    <label>Correct Option:</label>
                    <select value={correctOption} onChange={(event) => setCorrectOption(parseInt(event.target.value))}>
                        {options.map((option, index) => (
                            <option value={index} key={index}>
                                Option {index + 1}
                            </option>
                        ))}
                    </select>
                    <button  onClick={handleAddQuestion}>
                   +
                </button>
                </div>
                
                <br/>
                <button type="submit">Create Quiz</button>
            </form>
        </div>
    );
}

export default Quiz
