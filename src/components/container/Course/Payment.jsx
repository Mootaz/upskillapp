import React, { useRef, useState } from 'react';
import axios from 'axios';
import './Payment.css'; // Import the CSS file
import { selectSessionUser } from '../../../topics/Session/slice';
import upskilllogo from "../../../assets/upskilllogo.png";
import { useSelector } from "react-redux";
import emailjs from '@emailjs/browser';
import Swal from 'sweetalert2';
import { Footer, Navbar } from '../../../components';
import { useLocation } from 'react-router-dom';
import { selectUserRole } from "../../../topics/Session/slice";
import { FaEnvelope, FaMoneyBillAlt, FaCreditCard, FaCalendarAlt } from 'react-icons/fa';
import paymentimage from '../../../assets/main-3d-effectuant-paiement-sans-numeraire-partir-smartphone_107791-16609.avif'
import paymentimage1 from '../../../assets/pay9.jpeg'
import paymentimage2 from '../../../assets/pay2.png'






function Payment() {
  const location = useLocation();
  const { totalPrice } = location?.state;
  const [email, setEmail] = useState("");
  const [amount, setAmount] = useState("");
  const [currency, setCurrency] = useState("");
  const [source, setSource] = useState({
    cardNumber: '',
    expMonth: '',
    expYear: '',
  });
  const [cvc, setCvc] = useState('');
  const [date, setDate] = useState('');
  const [payment, setPayment] = useState(null);
  const [error, setError] = useState(null);
  const currentUser = useSelector(selectSessionUser);
  const [User_id, setUser_id] = useState(currentUser._id);
  // Add these state variables and setter functions to your component
  const [cardNumber, setCardNumber] = useState('');
  const [expMonth, setExpMonth] = useState('');
  const [expYear, setExpYear] = useState('');
  const [step, setStep] = useState(1);







  const form = useRef();


  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      // // Check if the payment amount matches the totalPrice
      // if (parseFloat(amount) !== parseFloat(totalPrice)) {
      //   console.log("Payment amount does not match the total price.");
      //   setError("Payment amount does not match the total price.");
      //   return; // Exit the function to prevent storing the payment in the database
      // }

      const response = await fetch("http://localhost:4000/payment/createpayment", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          amount: totalPrice,
          currency: currency,
          source: {
            cardNumber: cardNumber,
            expMonth: expMonth,
            expYear: expYear,
          },
          cvc: cvc,
          date: date,
          User_id: User_id,
        }),
      });

      const data = await response.json();
      const payment = data.payment;
      setPayment(payment);

      // Update payment status to "paid"
      const updatedPayment = await updatePaymentStatus(payment._id, "paid");
      console.log(updatedPayment);

      // Send email notification
      await sendEmailNotification();

      Swal.fire({
        icon: "success",
        title: "Payment Successful",
        text: "Your payment has been processed successfully.",
        showCancelButton: true,
        confirmButtonText: "Go to Dashboard",
        cancelButtonText: "Close",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        reverseButtons: true,
      }).then((result) => {
        if (result.isConfirmed) {
          // Redirect to the dashboard or perform any other action
          window.location.href = "/displaycourse";
        }
      });
      // Update the step to the next value
      setStep(step + 1);
    } catch (error) {
      setError(error.message);
    }
  };


  const updatePaymentStatus = async (paymentId, status) => {
    try {
      const response = await axios.put(`http://localhost:4000/payment/updatestatus/${paymentId}`, {
        status: status,
      });
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const sendEmailNotification = async () => {
    try {
      await emailjs.sendForm("service_koxixtq", "template_yd8kk1c", form.current, "DgyWz6om_AFHtgvMX");
      console.log("Email notification sent");
    } catch (error) {
      console.log("Error sending email notification:", error);
      throw error;
    }
  };

  const [productsInCart, setProducts] =
    useState(
      JSON.parse(
        localStorage.getItem(
          "shopping-cart"
        )
      ) || []
    );

  const userRole = useSelector(selectUserRole);


  // if (userRole !== 'student') {
  //   alert("You need to be a student to pay ");
  //   return null; // Return null or any other component to prevent rendering for non-Students
  // }

  return (

    <div className="flex flex-wrap" style={{ backgroundColor: "#f5f5f5" }}>
      <div className="max-w-md mx-auto flex ">
        <form ref={form} onSubmit={handleSubmit} className=" bg-white rounded-lg shadow-lg p-6 ">

          <div className='mb-4'>
            <img id="upskilllogo"
              name="upskilllogo" src={upskilllogo} alt='' style={{ width: '20%', height: '30%' }} />
          </div>

          <h1 className='font-bold text-center text-xl text-blue-400 mb-6'>Upskill Payment</h1>
          <div className="progress-bar">
            <div className={`step ${step === 1 ? 'active' : ''}`}></div>
            <div className={`step ${step === 2 ? 'active' : ''}`}></div>
            <div className={`step ${step === 3 ? 'active' : ''}`}></div>
            <div className={`step ${step === 4 ? 'active' : ''}`}></div>
          </div>

          <div className='image-container' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            
            <img src={paymentimage1} style={{ width: '160px', height: '130px' }} />
              
            {/* <img src={paymentimage2} style={{ width: '110px', height: '90px' }} /> */}
          
          </div>



          <div>

          </div>

          <br />
        


          <div className="mb-4 flex flex-wrap">
            <div className="items-center">
              <label htmlFor="email" className="block text-gray-700 font-bold mb-2">
                <FaEnvelope className="inline-block mr-2" />
                Email:
              </label>
              <input
                id="email"
                name="email"
                type="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                className="border border-gray-400 p-2 w-full rounded-lg focus:outline-none focus:border-blue-500"
                required
              />
            </div>

          </div>
          <div className="mb-4 flex flex-wrap">
            <div className="w-1/2 pr-2">
              <label htmlFor="currency" className="block text-gray-700 font-bold mb-2">
                <FaMoneyBillAlt className="inline-block mr-2" />
                Currency:
              </label>
              <input
                id="currency"
                name="currency"
                type="text"
                value={currency}
                onChange={(event) => setCurrency(event.target.value)}
                className="border border-gray-400 p-2 w-full rounded-lg focus:outline-none focus:border-blue-500"
                required
              />
            </div>

            <div className="w-1/2 pr-2">
              <label htmlFor="source" className="block text-gray-700 font-bold mb-2">
                <FaCalendarAlt className="inline-block mr-2" />
                Payment Date:
              </label>
              <input
                id="date" type="date"
                name="date"
                value={date}
                onChange={(event) => setDate(event.target.value)}
                className="border border-gray-400 p-2 w-full rounded-lg focus:outline-none focus:border-blue-500"
                required
              />
            </div>

          </div>
          <br />

          <div className="mb-4">
            <label htmlFor="source" className="block text-gray-700 font-bold mb-2">
              <FaMoneyBillAlt className="inline-block mr-2" />
              Source:
            </label>
            <div className="flex">
              <input
                id="cardNumber"
                name="cardNumber"
                type="text"
                value={cardNumber}
                onChange={(event) => setCardNumber(event.target.value)}
                className="border border-gray-400 p-2 mr-2 rounded-lg focus:outline-none focus:border-blue-500"
                required
                placeholder="Card Number"
              />
              <input
                id="expMonth"
                name="expMonth"
                type="text"
                value={expMonth}
                onChange={(event) => setExpMonth(event.target.value)}
                className="border border-gray-400 p-2 mr-2 rounded-lg focus:outline-none focus:border-blue-500"
                required
                placeholder="MM"
                maxLength="2"
              />
              <input
                id="expYear"
                name="expYear"
                type="text"
                value={expYear}
                onChange={(event) => setExpYear(event.target.value)}
                className="border border-gray-400 p-2 mr-2 rounded-lg focus:outline-none focus:border-blue-500"
                required
                placeholder="YYYY"
                maxLength="4"
              />
              <input
                id="cvc"
                name="cvc"
                type="password"
                value={cvc}
                onChange={(event) => setCvc(event.target.value)}
                className="border border-gray-400 p-2 rounded-lg focus:outline-none focus:border-blue-500"
                required
                placeholder="CVC"
              />
            </div>
          </div>











          {error && <p className="text-red-500 mb-4">Error: {error}</p>}

          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-110 w-full"
          >
            <span className="inline-block mr-2">Pay Now</span>
            <span className="inline-block">
              <svg className="h-6 w-6 text-white inline-block" fill="currentColor" viewBox="0 0 20 20">
                <path
                  fillRule="evenodd"
                  d="M16.707,5.293l-4-4C12.52,1.105,12.266,1,12,1H2C1.447,1,1,1.447,1,2v16c0,0.553,0.447,1,1,1h16c0.553,0,1-0.447,1-1V6C18,5.734,17.895,5.48,16.707,5.293zM13,4.414L14.586,6H13V4.414zM17,17H3V3h8v5h6V17z"
                  clipRule="evenodd"
                />
              </svg>
            </span>
          </button>
        </form>

      </div>
      <div className="bg-white rounded-lg shadow-lg p-6 mt-15 pt-7 ">
        <h1 className='text-2xl font-bold text-blue-400'>User Details:</h1><br />
        <p className="text-xl font-normal ">{`${currentUser.firstName} ${currentUser.lastName}`} </p>
        <p className="text-xl font-normal text-blue-400"> {`${currentUser.mainRole}`}</p> <br />
        <br />
        <h2 className="font-bold text-lg mb-4">Total Price: {totalPrice}  $ </h2>

      </div>
      <div className='mt-10'>
        <Footer />
      </div>

    </div>

  );
}

export default Payment;