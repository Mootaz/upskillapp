import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { GiCancel, GiShoppingBag } from 'react-icons/gi';
import { MdDelete } from 'react-icons/md';
import { FaBell } from 'react-icons/fa'; // import the notification icon
import './CourseDetails.css';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useNavigate } from "react-router-dom";
import Snackbar from '@mui/material/Snackbar';
import { Footer } from '../../../components';
import Button from './chatboat/Button';
import Chat from './chatboat/Chat';
import Payment from './Payment';
import { useSelector } from "react-redux";
import { selectSessionUser } from '../../../topics/Session/slice';



const CourseDetails = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const [course, setCourse] = useState(null);
  const [showCartModal, setShowCartModal] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [show, setShow] = useState(false);
  const currentUser = useSelector(selectSessionUser);
  const [User_id, setUser_id] = useState(currentUser._id);




  const paymentpage = () => {
    navigate("/payment", { state: { totalPrice: totalPrice } });
  }

  const [productsInCart, setProducts] =
    useState(
      JSON.parse(
        localStorage.getItem(
          "shopping-cart"
        )
      ) || []
    );

  const removeProduct = (index) => {
    const updatedProducts = [...productsInCart];
    updatedProducts.splice(index, 1);
    setProducts(updatedProducts);
    localStorage.setItem('shopping-cart', JSON.stringify(updatedProducts));
  };




  useEffect(() => {
    const fetchCourseDetails = async () => {
      try {
        const response = await axios.get(`http://localhost:4000/course/getcoursebyid/${id}`);
        setCourse(response.data);
      } catch (err) {
        console.error(err);
      }


    };

    fetchCourseDetails();
  }, [id]);

  const showBtn = () => {
    setShow(!show)
  }
  const addToCart = (course) => {
    setProducts([...productsInCart, course]);
    localStorage.setItem('shopping-cart', JSON.stringify([...productsInCart, course]));
    // Show snackbar
    setSnackbarOpen(true);
    setSnackbarMessage('Cart added successfully');

   
  };


  const navigateopage = () => {
    navigate("/panier2")
  }
  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const totalPrice = productsInCart.reduce((total, product) => total + product.price, 0);
  console.log(totalPrice);


  if (!course) {
    return <p>Loading course details...</p>;
  }

  return (
    <div>
      <nav className="nav-container flex justify-between items-center py-4 px-6 bg-indigo-50 ">
        <div className="flex items-center flex-grow">
          <h1 className="text-2xl font-bold text-gray-800 text-Teal">UPSKILL LABS</h1>
        </div>

        {/* Shopping Cart Button */}
        <button
          className="relative mr-4 text-gray-700 hover:text-gray-600 focus:text-gray-600"
          onClick={() => setShowCartModal(true)}
        >
          <GiShoppingBag size={33} />
          {productsInCart.length > 0 && (
            <span className="absolute top-0 left-0 rounded-full bg-red-500 text-white p-1 text-xs">
              {productsInCart.length}
            </span>
          )}
        </button>
        {/* Notification Icon */}
        <div className="relative mr-4 text-gray-700 hover:text-gray-600 focus:text-gray-600">
          <FaBell
            size={24}
            onClick={() => setSuccessMessage(null)}
          />
          {productsInCart.length > 0 && (
            <span className="absolute top-0 left-0 rounded-full bg-red-500 text-white p-1 text-xs">
              {productsInCart.length}
            </span>
          )}
        </div>
      </nav>
      <div className="grid grid-cols-2 gap-8">

        <div>
          <img
            src={`/coursefile/${course.image}`}
            alt={course.title}
            className='ml-60 mt-10 '
            style={{ maxHeight: '300px' }}
          />
        </div>
        <div className="flex flex-col justify-between">
          <div className="max-w-xl py-8 px-4">
            <h1 className="text-3xl font-bold text-gray-800 mb-4">{course.title}</h1>
            <p className="text-lg text-gray-600 mb-8">{course.description}</p>
            <p className="text-lg text-gray-600 mb-8">{course.category}</p>

            <div className="flex justify-between items-center">
              <p className="text-xl font-bold text-gray-800">
                {course.price}$

              </p>
            </div>

            <div class="flex items-center">
              <svg aria-hidden="true" class="w-5 h-5 text-yellow" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>First star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
              <svg aria-hidden="true" class="w-5 h-5 text-yellow" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Second star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
              <svg aria-hidden="true" class="w-5 h-5 text-yellow" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Third star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
              <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fourth star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
              <svg aria-hidden="true" class="w-5 h-5 text-gray-300 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fifth star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
            </div>
          </div>

          <Snackbar
            open={snackbarOpen}
            message={snackbarMessage}
            autoHideDuration={3000} // Adjust the duration as needed
            onClose={handleSnackbarClose}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          />


          <div>
            <button
              className="px-4 py-2 bg-blue-600 text-white rounded hover:bg-blue-400"
              onClick={() => addToCart(course)}
            >
              Add to cart
            </button>

            <button
              className="px-4 py-2 bg-blue-600 text-white rounded hover:bg-blue-400 ml-9"
              onClick={() => navigateopage()}
            >
              Back
            </button>
          </div>

        </div>


      </div>

    

      <div>
        <Button show={show} showBtn={showBtn} />
        {show && <Chat />}
      </div>


      {/* Shopping Cart */}

      {showCartModal && (
        <div className="modal">
          <div className="modal-content bg-white rounded-lg shadow-lg p-6">
            <button className="close text-gray-500" onClick={() => setShowCartModal(false)}>
              &times;
            </button>
            <h2 className="text-2xl font-bold mb-4">Your Shopping Cart</h2>
            <input
              type="text"
              placeholder="Search by title"
              className="border border-gray-300 rounded-lg px-4 py-2 mb-4"
              onChange={(e) => setSearchTerm(e.target.value)}
            />

            <Slider dots={true} infinite={false} slidesToShow={3}>
              {productsInCart
                .filter((product) => product.title.toLowerCase().includes(searchTerm.toLowerCase()))
                .map((product, index) => (
                  <li key={index}>
                    <div className="product-info">
                      <div className="product-image">
                        <img
                          src={`/coursefile/${product.image}`}
                          style={{ width: "40%", height: "30%" }}
                          alt={product.title}
                        />
                      </div>
                      <div className="product-text">
                        <p className="product-title font-bold">{product.title}</p>
                        <p className="product-description">{product.description}</p>
                        <p className="product-title font-bold">{product.category}</p>
                        <span className="product-price">{product.price}$</span>

                      </div>
                      <button className="delete-button" onClick={() => removeProduct(index)}>
                        <MdDelete size={20} />
                      </button>
                    </div>
                  </li>
                ))}
            </Slider>




            <div className="flex justify-between items-center mt-6">
              <button
                className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
                onClick={paymentpage}
              >
                Checkout
              </button>
              <p className="text-xl">
                <span className="font-bold text-xl">Total Price:</span> {productsInCart.reduce((total, product) => total + product.price, 0)}$
              </p>
            </div>
          </div>
        </div>
      )}
      
      <div className='mt-60'>
        <Footer />

      </div>

    </div>



  );
};

export default CourseDetails;
