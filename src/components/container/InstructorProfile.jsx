import React, { useState, useEffect} from "react";
import ImgCrop from "antd-img-crop";
import PhoneInput from "react-phone-number-input";
import {
  Layout,
  Form,
  Input,
  Select,
  Radio,
  Checkbox,
  Row,
  Col,
  message,
  Button,
  Upload,
} from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  $insertUserCV,
  selectSessionUser,
  selectUserCV,
  selectUserRole,
} from "../../topics/Session/slice";
// UI local components
import DatePicker from "../../topics/Shared/components/inputs/DatePicker";
import CountrySelect from "../../topics/Shared/components/inputs/countrySelect";

// constants
import { baseURL } from "../../constants";
import axios from "axios";

//styles
import "./InstructorProfile.css";

import fr from "react-phone-number-input/locale/fr.json";

import { hobbies } from "../../topics/Shared/Entities/hobbies";
import nationalities from "../../topics/Shared/mocks/nationalities";
//assets

import team from "../../assets/bg-profile.jpg";
import { Message, TopologyStar3 } from "tabler-icons-react";
// scoped componenets
import logo from  "../../assets/team-2.jpg"

const { TextArea } = Input;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function InstructorProfile() {
  /* ---------------------------------- HOOKS --------------------------------- */
  const [personalInfoForm] = Form.useForm();
  const currentUser = useSelector(selectSessionUser);
  const currentRole = useSelector(selectUserRole);
  console.log("currentUser", currentUser);
  const dispatch = useDispatch();
  const currentUserCV = useSelector(selectUserCV);
  // states
  const [selectedFile, setSelectedFile] = useState();
  const [bg_profile, setBgProfile] = useState('');
  const [uploading, setUploading] = useState(false)
  const [image, setImage] = useState('');


  useEffect(() => {
    const storedBgProfile = localStorage.getItem('bg_profile');
    if (storedBgProfile) {
      setBgProfile(storedBgProfile);
    }
  }, []);
  const uploadFileHandler = async (e) => {
    const file = e.target.files[0]
    const formData = new FormData()
    formData.append('image', file)
    setUploading(true)

    try {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }

      const { data } = await axios.post('http://localhost:4000/api/upload', formData, config)
   
      console.log(data)
      setImage(data)
      setUploading(false)

      // Store the profile image URL in local storage
      localStorage.setItem('bg_profile', data);
    } catch (error) {
      console.error(error)
      setUploading(false)
    }
  };
   
  /* -------------------------------- CALLBACKS ------------------------------- */

   

  function onSaveUserData() {
    const userData = {
      userPersonalInfo: personalInfoForm.getFieldsValue(),
      selectedFile: selectedFile
    };

    dispatch($insertUserCV({ _id: currentUser._id, ...userData }))
      .then((action) => {
        onUploadIMG(selectedFile);
        message.success("Changes saved !");
      })
      .catch((error) => {
        message.error("An error occured");
      });
  }
  /* ---------------------------- RENDERIN HELPERS ---------------------------- */
  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };

  const handleFileChange = (e) => {
    setSelectedFile(e.target.files[0]);
    uploadFileHandler(e);
  };
  
  function uploadImg() {
    return (
      <ImgCrop rotate>
        {
          <input type="file" onChange={handleFileChange} />
        }
      </ImgCrop>
    );
  }

  // uplaodIMG
  // On file upload (click the upload button)
  const onUploadIMG = (e) => {
    // Create an object of formData
    const data = new FormData();
    data.append("img", selectedFile);

    axios({
      method: "post",
      baseURL: `${baseURL}/user/uploadIMG`,
      headers: { "Content-Type": "multipart/form-data" },
      data,
      body: data,
    })
      .then((response) => {
        const imageURL = response.data.imageURL;
        localStorage.setItem('bg_profile', imageURL); // save the imageURL to localStorage
        setBgProfile(imageURL); // update the state with the imageURL
        message.success(response.data.message);
      })
      .catch((error) => {
        message.error(error);
      });
  };

  function getPersonalInformationsForm() {
    return (
     
      <Form
        form={personalInfoForm}
        {...formItemLayout}
        initialValues={{ ...currentUser, ...currentUserCV }}
      >
        <h2 class="text-4xl font-bold mb-6 text-center animated-text text-Teal"> {`${currentRole} `} Profile</h2>
        
        <Row>
          <Col span={11}>
            <Form.Item
              label="First name"
              name="firstName"
              required
              rules={[
                {
                  required: true,
                  message: "Please input your first name!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item
              label="Last name"
              name="lastName"
              required
              rules={[
                {
                  required: true,
                  message: "Please input your last name!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <Form.Item label="Birth Date" name="birthDate">
              <DatePicker />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item label="Phone" name="phone">
              <PhoneInput
                placeholder="Enter phone number"
                defaultCountry="TN"
                international
                labels={fr}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <Form.Item
              label="Address"
              name="address"
              required
              rules={[
                {
                  required: true,
                  message: "Please input your address!",
                },
              ]}
            >
              <CountrySelect onChange={(e) => { }} />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item
              required
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <b>Social accounts</b>
        <Row>
          <Col span={11}>
            <Form.Item label="Facebook" name="facebookLink">
              <Input />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item label="Instagram" name="instagramLink">
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <Form.Item label="LinkedIn" name="linkedinLink">
              <Input />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item label="Whatsapp" name="whatsappLink">
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item label="Nationality" name="nationalities">
          <Select
            options={nationalities.map((nationality) => ({
              value: nationality.label,
              label: nationality.label,
            }))}
            mode="multiple"
          />
        </Form.Item>
        <Form.Item name="preferredSchedule" label="Preferred schedule">
          <Checkbox.Group>
            <Row>
              <Col>
                <Checkbox value="part-time">Part time</Checkbox>
              </Col>
              <Col>
                <Checkbox value="full-time">Full time</Checkbox>
              </Col>
              <Col>
                <Checkbox value="remote">Remote</Checkbox>
              </Col>
              <Col>
                <Checkbox value="on-site">On site</Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
        </Form.Item>
        <Form.Item label="Interests and hobbies" name="hobbies">
          <Select
            mode="multiple"
            allowClear
            options={hobbies.map((hobby) => hobby)}
          />
        </Form.Item>
        <Form.Item label="About me" name="aboutMe">
          <TextArea placeholder="maxLength is 300" showCount maxLength={650} />
        </Form.Item>
      </Form>
    );
  }

  /* -------------------------------- RENDERING ------------------------------- */
  return (
    // <Content className="profile-content">

    // </Content>
    <div class="grid grid-cols-2 gap-2">
      <div>
        {uploadImg()}
        {getPersonalInformationsForm()}
        <Row justify="end">
          <Button
            type="primary"
            className="upload-cv-button "
            onClick={() => {
              onSaveUserData();
            }}
            style={{
              backgroundColor: "Teal",
              width: "8em",
              height: "3em",
              border: "1px Teal solid",
              borderRadius: "10px",
            }}
          >
            Save
          </Button>
        </Row>
      </div>

      <div>
        <div class="w-full max-w-full px-3 mt-6 shrink-0  md:flex-0 md:mt-0">
          <div class="relative flex flex-col min-w-0 break-words bg-white border-0 shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
            <img
              className="w-full rounded-t-2xl"
              src={team}
              alt="profile cover image"
            />
            
            <div class="flex flex-wrap justify-center -mx-3">
  <div class="w-8/12 max-w-full px-3 flex-0">
    <div class="mb-6 -mt-6 lg:mb-0 lg:-mt-16">
      <a href="javascript;">
        <img
          class="border-2 border-white border-solid rounded-circle ml-40"
          src={logo}
          alt="profile image"
        />
      </a>
    </div>
  </div>
</div>


            <div class="flex-auto p-6 pt-0">
              <div class="mt-6 text-center ">
                <h5 class="dark:text-white  font-semibold leading-relaxed text-base dark:text-white/80 text-slate-700">{`${currentRole} `}</h5>
              </div>
            </div>

            <div class="border-black/12.5 rounded-t-2xl p-6 text-center pt-0 pb-6 lg:pt-2 lg:pb-4">
              <div class="flex justify-between">
                <button
                  type="button"
                  class="hidden px-8 py-2 font-bold leading-normal text-center text-white align-middle transition-all ease-in border-0 rounded-lg shadow-md cursor-pointer text-xs bg-cyan-500 lg:block tracking-tight-rem hover:shadow-xs hover:-translate-y-px active:opacity-85"
                >
                  Connect
                </button>
                <button
                  type="button"
                  class="block px-8 py-2 font-bold leading-normal text-center text-white align-middle transition-all ease-in border-0 rounded-lg shadow-md cursor-pointer text-xs bg-cyan-500 lg:hidden tracking-tight-rem hover:shadow-xs hover:-translate-y-px active:opacity-85"
                >
                  <TopologyStar3 />
                </button>
                <button
                  type="button"
                  class="hidden px-8 py-2 font-bold leading-normal text-center text-white align-middle transition-all ease-in border-0 rounded-lg shadow-md cursor-pointer text-xs bg-slate-700 lg:block tracking-tight-rem hover:shadow-xs hover:-translate-y-px active:opacity-85"
                >
                  Message
                </button>
                <button
                  type="button"
                  class="block px-8 py-2 font-bold leading-normal text-center text-white align-middle transition-all ease-in border-0 rounded-lg shadow-md cursor-pointer text-xs bg-slate-700 lg:hidden tracking-tight-rem hover:shadow-xs hover:-translate-y-px active:opacity-85"
                >
                  <Message />
                </button>
              </div>
            </div>

            <div class="flex-auto p-6 pt-0">
              <div class="flex flex-wrap -mx-3">
                <div class="w-full max-w-full px-3 flex-1-0">
                  <div class="flex justify-center">
                    <div class="grid text-center">
                      <span class="font-bold dark:text-white text-lg">22</span>
                      <span class="leading-normal dark:text-white text-sm opacity-80">
                        Friends
                      </span>
                    </div>
                    <div class="grid mx-6 text-center">
                      <span class="font-bold dark:text-white text-lg">10</span>
                      <span class="leading-normal dark:text-white text-sm opacity-80">
                        Photos
                      </span>
                    </div>
                    <div class="grid text-center">
                      <span class="font-bold dark:text-white text-lg">89</span>
                      <span class="leading-normal dark:text-white text-sm opacity-80">
                        Comments
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-6 text-center">
                <h5 class="dark:text-white  ">
                  {`${currentUser.firstName} ${currentUser.lastName}`}
                  <span class="font-light">, 35</span>
                </h5>
                <br />
                <div class="mb-2 font-semibold leading-relaxed text-base dark:text-white/80 text-slate-700">
                  <i class="mr-2 dark:text-white ni ni-pin-3"></i>
                  {`${currentUser.email}`} <br />
                  {` ${currentUser.nationalities}`}
                </div>
                <div class="mt-6 mb-2 font-semibold leading-relaxed text-base dark:text-white/80 text-slate-700">
                  <i class="mr-2 dark:text-white ni ni-briefcase-24"></i>

                  {` ${currentUser.linkedinLink}`}  <br />


                </div>
                <div class="dark:text-white/80">
                  <i class="mr-2 dark:text-white ni ni-hat-3"></i>
                  University of Computer Science
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default InstructorProfile;
