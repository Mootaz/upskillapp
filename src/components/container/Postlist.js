import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { baseURL } from '../../constants';
import UploadForm from '../../components/UploadForm';
import Navbar from '../Navbar/Navbar';
import Footer from './Footer';




const Postlist = () => {
  const { courseId } = useParams();
  const [medias, setMedias] = useState([]);
 

  useEffect(() => {
    getAllMedias();
  }, []);

  const getAllMedias = () => {
    axios
      .get(`${baseURL}/api/media/course/${courseId}`)
      .then((result) => {
        setMedias(result.data);
      })
      .catch((error) => {
        setMedias([]);
        console.log(error);
        alert('Error happened!');
      });
  };

  

  return (
    <div className="min-h-screen bg-gray-50 " style={{ backgroundColor: "#f5f5f5", }}>
      <Navbar />
      <br />
      <br />
      <div style={{ display: 'flex' }}>
        <div className="px-6 py-8 flex-1">
          <UploadForm courseId={courseId} getAllMedias={getAllMedias} />
        </div>
       
      </div>
      <div>
     
      </div>
      
      <Footer />
    </div>


  )
}

export default Postlist
