import React from 'react'
import  { Navbar, About, Contact, Courses, Footer, Home, Teacher } from './components/index';
import './LMS.css';

export default function LMS() {
  return (
    <div className="font-Poppins bg-Solitude">
      <Navbar/>
      <Home/>
      <About/>
      <Courses/>
      <Teacher/>
      <Contact/>
      <Footer/>
    </div>
  )
}
