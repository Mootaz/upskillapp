import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { GiCancel, GiShoppingBag } from 'react-icons/gi';
import { FaBell } from 'react-icons/fa'; // import the notification icon
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'swiper/swiper-bundle.css';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination } from 'swiper';
import 'swiper/swiper-bundle.css';
import './Panier.css';
import { useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import { Route } from "react-router-dom";



import {

  selectSessionUser,

} from "../src/topics/Session/slice";
import Footer from './HomepageT/Footer';
import CourseDetails from './components/container/Course/CourseDetails';
SwiperCore.use([Navigation, Pagination]);

const Panier = () => {

  const [courses, setCourses] = useState([]);
  const [cartsVisibilty, setCartVisibility] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);
  const [activeTab, setActiveTab] = useState('');
  const [searchValue, setSearchValue] = useState('');
  const [showCartModal, setShowCartModal] = useState(false);
  const currentUser = useSelector(selectSessionUser);
  const [selectedCourse, setSelectedCourse] = useState(null);




  const handleTabClick = (tab) => {
    setActiveTab(tab);
  };


  const [productsInCart, setProducts] =
    useState(
      JSON.parse(
        localStorage.getItem(
          "shopping-cart"
        )
      ) || []
    );

  const removeProduct = (index) => {
    const updatedProducts = [...productsInCart];
    updatedProducts.splice(index, 1);
    setProducts(updatedProducts);
    localStorage.setItem('shopping-cart', JSON.stringify(updatedProducts));
  };



  useEffect(() => {
    getCourses();
  }, [searchValue]);

  const getCourses = async () => {
    try {
      const response = await axios.get('http://localhost:4000/course/getallcourse');

      const filteredCourses = response.data.filter(course => course.title.toLowerCase().includes(searchValue.toLowerCase()));
      setCourses(filteredCourses);
    } catch (err) {
      console.error(err);
    }
  };



  const handleCourseClick = (course) => {
    setSelectedCourse(course);

  };


  const addToCart = (course) => {
    setProducts([...productsInCart, course]);
    localStorage.setItem('shopping-cart', JSON.stringify([...productsInCart, course]));
    setSuccessMessage(`${course.title} has been added to your cart.`);
    setTimeout(() => setSuccessMessage(false), 2000); // hide success message after 3 seconds
  };
  const openCartModal = () => {
    setCartVisibility(true);
  };

  const closeCartModal = () => {
    setCartVisibility(false);
  };

  <Route path="/courses/:id">
    <CourseDetails course={selectedCourse} />
  </Route>




  return (
    <div>


      <br />

      {/* Success Message */}
      {successMessage && (
        <div className="success-message">
          <p>{successMessage}</p>
          <button onClick={() => setSuccessMessage(null)}>x</button>
        </div>
      )}

      <div className="text-center">
        <div className="flex justify-center space-x-2 mt-4">
          <button
            className={`bg-blue-300 hover:bg-red-600 text-white font-bold py-3 px-4 rounded-xl ${activeTab === 'overview' ? 'bg-red-600' : ''}`}
            onClick={() => handleTabClick('overview')}
          >
            Overview
          </button>
          <button
            className={`bg-blue-300 hover:bg-red-600 text-white font-bold py-3 px-4 rounded-xl ${activeTab === 'program' ? 'bg-red-600' : ''}`}
            onClick={() => handleTabClick('program')}
          >
            Program
          </button>
          <button
            className={`bg-blue-300 hover:bg-red-600 text-white font-bold py-3 px-4 rounded-xl ${activeTab === 'instructor' ? 'bg-red-600' : ''}`}
            onClick={() => handleTabClick('instructor')}
          >
            Instructor
          </button>
          <button
            className={`bg-blue-300 hover:bg-red-600 text-white font-bold py-3 px-4 rounded-xl ${activeTab === 'resume' ? 'bg-red-600' : ''}`}
            onClick={() => handleTabClick('resume')}
          >
            Resume
          </button>
        </div>
        {activeTab === 'overview' && (
          <div>
            <h1 className="font-bold">Certification description</h1>
            <p>Participants will have gained valuable skills and knowledge in areas such as, </p>
            <p>which will prepare them for a successful career in the field.</p>
            <p>which will prepare them for a successful career in the field.</p>
            <p>Upon completion of the program...</p>
          </div>
        )}
        {activeTab === 'program' && (
          <div>
            <h1 className="font-bold">Program message</h1>
            <p>Participants will have gained valuable skills and knowledge in areas such as</p>
            <p>,which will prepare them for a successful career in the field.</p>
            <p>Upon completion of the program...</p>
          </div>
        )}
        {activeTab === 'instructor' && (
          <div>
            <h1 className="font-bold">Instructor Details</h1>
            <p> {`${currentUser.firstName} ${currentUser.lastName}`}</p>
            <p>{`${currentUser.email}`} </p>
            <p> {`${currentUser.phone}`}</p>
            <p>{`${currentUser.nationalities}`} </p>
          </div>
        )}
        {activeTab === 'resume' && (
          <div>
            <h1 className="font-bold">Resume message</h1>
            <p>Participants will have gained valuable skills and knowledge in areas such as, which will prepare them for a successful career in the field.</p>
            <p>Upon completion of the program...</p>
          </div>
        )}
      </div>
      <br />
      <br />
      <div className="course-list">
        <h1 id="animated-text" className="cd-headline clip is-full-width text-center">
          <span className="cd-words-wrapper" style={{ width: '266px', overflow: 'hidden' }}>
            Our <b className='text-blue-400'>popular</b> Upskill Courses
          </span>
        </h1>
        <div className='text-center'>
          <input type="text" placeholder="Search by title" value={searchValue} onChange={(e) => setSearchValue(e.target.value)} style={{ width: '266px' }} />
        </div>
        <Swiper
          slidesPerView={3}
          spaceBetween={30}
          navigation
          pagination={{ clickable: true }}
        >
          {courses.map((course) => (
            <SwiperSlide className="course-item" key={course._id} onClick={() => handleCourseClick(course)}>
              <Link to={`/courses/${course._id}`} key={course._id}>
                <div className="md:w-[60%]">
                  <img src={`/coursefile/${course.image}`} width="400" height="300" />
                </div>

                <div className="course-details">
                  <h2 className="course-title font-bold">{course.title}</h2>
                  {/* <p className="course-description">Description: {course.description}</p>
                <p className="course-author">Author: {course.author}</p>
                <p className="course-price">Price: {course.price}</p> */}
                  <p className="course-rating"> {course.rating}</p>


                  <div class="flex items-center">
                    <svg aria-hidden="true" class="w-5 h-5 text-yellow" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>First star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                    <svg aria-hidden="true" class="w-5 h-5 text-yellow" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Second star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                    <svg aria-hidden="true" class="w-5 h-5 text-yellow" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Third star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                    <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fourth star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                    <svg aria-hidden="true" class="w-5 h-5 text-gray-300 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fifth star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                  </div>


                  <div class="course-buttons flex justify-between">
                    {/* <button class="course-btn-add-to-cart" onClick={() => addToCart(course)}>Add To Cart</button> */}

                  </div>


                </div>
              </Link>
            </SwiperSlide>
          ))}
        </Swiper>
        {selectedCourse && <CourseDetails course={selectedCourse} />}

      </div>

      {/* Shopping Cart */}
      {showCartModal && (
        <div className="modal">
          <div className="modal-content">
            <span className="close" onClick={() => setShowCartModal(false)}>
              &times;
            </span>
            <h2>Your Shopping Cart</h2>

            <Slider dots={true} infinite={false} slidesToShow={3}>
              {productsInCart
                .map((product, index) => (
                  <li key={index}>
                    <div className="product-info">
                      <div className="product-image">
                        <img
                          src={`/coursefile/${product.image}`}
                          style={{ width: "40%", height: "30%" }}
                          alt={product.title}
                        />
                      </div>
                      <div className="product-text">
                        <p className="product-title font-bold">{product.title}</p>
                        <p className="product-description">{product.description}</p>
                        <span className="product-price">{product.price}$</span>
                        <button onClick={() => removeProduct(index)}>
                          <GiCancel size={20} />
                        </button>
                      </div>
                    </div>
                  </li>
                ))}
            </Slider>
            <div>
              <p className=' text-xl'><p className='font-bold text-xl'>Total Price:</p> {productsInCart.reduce((total, product) => total + product.price, 0)}$</p>
            </div>

          </div>
        </div>
      )}

      <div>
        <br />

      </div>


    </div >
  );

};

export default Panier;
