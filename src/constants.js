export const baseURL = "http://localhost:4000";
const strongRegex = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])"
  );
  const validEmail = new RegExp("(.*[a-zA-Z0-9.])@(.*[a-zA-Z.]).(.*[a-zA-Z.])");
export {  strongRegex,validEmail };
