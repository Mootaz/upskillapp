// Packages
import React from "react";
import { Route, Routes, Navigate } from "react-router-dom";

// pages
import NotFoundPage from "./topics/Shared/StaticPages/NotFoundPage";
import OfflinePage from "./topics/Shared/StaticPages/OfflinePage";
import HomePage from "./topics/layouts/HomePage";
import ResetPassword from "./topics/Session/resetPassword";
import RecruterPage from "./topics/recruterPage";
import CareerLabPage from "./topics/Labs/careerLab/CareerLabPage";
import MentorLab from "./topics/Labs/MentorLab";
import MentorProfil from "./topics/Profil/MentorProfil";

import CreateCv from "./topics/Dashboard/CreateCv";
import Dashbaod from "./topics/Dashboard";
import GlobalDashboard from "./topics/Dashboard/GlobalDashboard";
import MentorServices from "./topics/Dashboard/MentorServices";
import MentorInbox from "./topics/Dashboard/MentorInbox";
import MyMentoringLab from "./topics/Dashboard/MyMentoringLab";
import MyWallet from "./topics/Dashboard/MyWallet";
import Profile from "./topics/Dashboard/Profile";
import ResumePage from "./topics/Labs/careerLab/ResumePage";
import MentorCalendar from "./topics/Dashboard/MentorCalendar";
import UsersList from "./topics/AdminPanel/UsersList";
import Homepaget from "./Homepaget";
import LMS from "./LMS";
import CourseList from "./components/container/Course/courseList";
import DisplayCourse from "./components/container/Course/DisplayCourse";
import Panier from "./Panier";
import InstructorProfile from "./components/container/InstructorProfile";
import Uploadvideo from "./Uploadvideo";
import CourseDetails from "./components/container/Course/CourseDetails";
import Payment from "./components/container/Course/Payment";
import Home from "./components/home/Home";
import Quiz from "./components/container/Course/Quiz";
import App from "./Components/App";
import Openai from "./components/container/OpenAI/Openai";
import Postlist from "./components/container/Postlist";
import Dashboard from "./topics/AdminPanel/Dashboard";
import Userslistdata from "./topics/AdminPanel/Uselistdata";
import Recommandation from "./components/container/Course/Courserecommandation/Recommandation";
import UserRecommandation from "./components/container/Course/Courserecommandation/UserRecommandation";
import RecommendList from "./components/container/Course/Courserecommandation/RecommendList";
import CourseRecommandation from "./components/container/Course/Courserecommandation/CourseRecommandation";










function MainRouter() {
  /* ********************************** HOOKS ********************************* */

  return (
    <Routes>
      <Route path="/" element={<Navigate to="/homePage" />} />
      <Route path="homePage" element={<HomePage />} />
      <Route path="recruterPage" element={<RecruterPage />} />
      <Route path="panier" element={<Panier />} />
      <Route path="homepaget" element={<Homepaget />} />
      <Route path="learning" element={<LMS />} />
      <Route path="/courses/:courseId/uploadvideo" element={<Uploadvideo />} />
      <Route path="/Quizz" element={<Quiz />} />
      <Route path="/z" element={<Quiz />} />
      <Route path="/Quiz" element={<App />} />
      <Route path="/chatboat" element={<Openai />} />
      <Route path="/recommandation" element={<Recommandation/>} />             

      <Route path="/UserRecommandation" element={<UserRecommandation/>} />
      <Route path="/courseSectionrecommandation" element={<CourseRecommandation/>} />
      <Route path="/UserRecommandationList" element={<RecommendList/>} />
      <Route path="/courses/:courseId/postlist" element={<Postlist />} />
      
     
      

      <Route path="Instructor" element={<InstructorProfile/>} />
      <Route path="crud" element={<CourseList />} />
      <Route path="/courses/:id" Component={CourseDetails} />
      <Route path="displaycourse" element={<DisplayCourse />} />
      <Route path="payment" element={<Payment/>} />
      <Route path="panier2" element={<Home/>} />
    
   

     
    
     
      
      
      
     
      {/* Admin routing */}
      <Route path="UsersList" element={<UsersList />}/>
      <Route path="Dashboardd" element={<Dashboard />}/>
      <Route path="userlistdata" element={<Userslistdata />}/>

      {/* Labs routing */}
      <Route path="careerLab" element={<CareerLabPage />} />
      <Route path="mentorLab" element={<MentorLab />} />
      <Route path="mentorProfil/:mentorId" element={<MentorProfil />} />

      {/* Dashboard routing */}
      <Route   path="/dashboard" element={<Dashbaod />}>
        <Route path="globalDashboard" element={<GlobalDashboard />} />
        <Route path="careerLab" element={<CreateCv />} />
        <Route path="services" element={<MentorServices />} />
        <Route path="inbox" element={<MentorInbox />} />
        <Route path="myMentoringLab" element={<MyMentoringLab />} />
        <Route path="myWallet" element={<MyWallet />} />
        <Route path="profile" element={<Profile />} />
        <Route path="mentorCalendar" element={<MentorCalendar />} />
      </Route>

      {/* Errors routing */}
      <Route path="/not-found" element={<NotFoundPage />} />
      <Route path="/offline" element={<OfflinePage />} />
      <Route path="*" element={<NotFoundPage />} />
      <Route path="resetPassword/:token" element={<ResetPassword />} />

      <Route path="resume" element={<ResumePage />} />
    </Routes>
  );
}

function OfflineRouter() {
  return (
    <Routes>
      <Route path="*" element={<OfflinePage />} />
    </Routes>
  );
}

export { MainRouter as default, OfflineRouter };
