import React, { useState, useEffect } from 'react';
import { baseURL } from './constants';
import axios from 'axios';
import UploadsList from './components/UploadsList';
import { useParams } from 'react-router-dom';
import './uploadvideo.css';
import { Courses, Footer, Navbar } from './components';

const Uploadvideo = () => {
  const { courseId } = useParams();
  const [medias, setMedias] = useState([]);
  const [course, setCourse] = useState(null); // Add state for course


  useEffect(() => {
    getAllMedias(); // Fetch all medias

    fetchCourse(); // Fetch the course

    // ... Other code

  }, []);


  const getAllMedias = () => {
    axios
      .get(`${baseURL}/api/media/course/${courseId}`)
      .then((result) => {
        setMedias(result.data);
      })
      .catch((error) => {
        setMedias([]);
        console.log(error);
        alert('Error happened!');
      });
  };

  const fetchCourse = async () => {
    try {
      const response = await fetch(`http://localhost:4000/course/getcoursebyid/${courseId}`);
      if (response.ok) {
        const data = await response.json();
        setCourse(data);
      } else {
        throw new Error('Failed to fetch course');
      }
    } catch (err) {
      console.error(err);
    }
  };

  return (

    <div className="min-h-screen bg-gray-50" style={{ backgroundColor: "#f5f5f5", }}>


      <Navbar />

      <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 gap-6 lg:grid-cols-2">
          <Courses />

          <div style={{ display: 'flex' }}>
            <div style={{ marginRight: '10px' }}>
              <UploadsList medias={medias.media} setMediaList={setMedias} />
            </div>

            <div class="course-content">

              {course && (
                <div class="course-item bg-white">
                  <div class="course-image w-15 h-10 rounded-full relative">
                    <img src={`/coursefile/${course.image}`} alt={course.title} style={{  position: 'relative', top: '-40px' }} />
                  </div>
                  <div className='mr-30'>
                  <p class="text-xl font-bold mr-20">{course.title}</p> {/* Display the course title */}
                  <p class="text ">{course.price}$</p> {/* Display the course price */}
                  </div>
                </div>
              )}

              <div class="course-item bg-white">
                <div class="icon">
                  <i class="fas fa-video"></i>
                </div>
                <p class="text">Introduction to the Course</p>
                <div class="dropdown hidden">
                  <ul class="course-details">
                    <li>Course overview</li>
                    <li>Learning objectives</li>
                    <li>Course structure</li>
                    <li>Assessment methods</li>
                  </ul>
                </div>
              </div>
              <div class="course-item bg-white">
                <div class="icon">
                  <i class="fas fa-book"></i>
                </div>
                <p class="text">Module 1: Fundamentals of HTML</p>
                <div class="dropdown hidden">
                  <ul class="course-details">
                    <li>Module 1 overview</li>
                    <li>HTML syntax</li>
                    <li>Tags and attributes</li>
                    <li>Structuring a webpage</li>
                  </ul>
                </div>
              </div>
              <div class="course-item bg-white">
                <div class="icon">
                  <i class="fas fa-laptop-code"></i>
                </div>
                <p class="text">Module 2: CSS Styling Techniques</p>
                <div class="dropdown hidden">
                  <ul class="course-details">
                    <li>Module 2 overview</li>
                    <li>CSS selectors</li>
                    <li>Box model</li>
                    <li>Layout and positioning</li>
                  </ul>
                </div>
              </div>
              <div class="course-item bg-white">
                <div class="icon">
                  <i class="fas fa-code"></i>
                </div>
                <p class="text">Module 3: JavaScript Essentials</p>
                <div class="dropdown hidden">
                  <ul class="course-details">
                    <li>Module 3 overview</li>
                    <li>Variables and data types</li>
                    <li>Conditional statements</li>
                    <li>Functions and loops</li>
                  </ul>
                </div>
              </div>
            </div>


          </div>


        </div>

      </div>

      <Footer/>
    </div>

  );
};

export default Uploadvideo;
