/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

// UI lib components
import {
  Button,
  Col,
  Image,
  Layout,
  Input,
  Rate,
  Row,
  Form,
  Switch,
} from "antd";
import Slider from "react-slick";
import { Briefcase } from "tabler-icons-react";

// Local ui components
import TopBar from "../../layouts/HomePage/topBar";

// Redux
import { selectSessionUser } from "../../Session/slice";

// style
import "./index.less";

//assests
import FOULAN from "../../../assets/mentorLab/foulan.png";
import Calend from "../../../assets/mentorProfil/Calend.png";
import $$ from "../../../assets/mentorProfil/$$.png";
import Awards from "../../../assets/mentorProfil/Award.png";
import Backg from "../../../assets/mentorProfil/Backg.png";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import axios from "axios";
import { baseURL } from "../../../constants";
import { useParams } from "react-router-dom";
import RequestMentoringModal from "./RequestMentoringModal";

// scoped components
const { Content } = Layout;

const { TextArea } = Input;
function MentorProfil() {
  /* ---------------------------------- HOOKS --------------------------------- */

  const params = useParams();
  const { mentorId } = params;

  // states
  const [mentor, setMentor] = useState();
  const [mentorCV, setMentorCV] = useState();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedService, setSelectedService] = useState();
  // redux
  const currentUser = useSelector(selectSessionUser);

  useEffect(() => {
    (async function () {
      try {
        await axios({
          method: "get",
          baseURL: `${baseURL}/user/getUser`,
          params: { userId: mentorId },
        })
          .then((res) => {
            setMentor(res.data.user);
            setMentorCV(res.data.cv);
          })
          .catch();
      } catch (e) {
        console.error(e);
      }
    })();
  }, []);

  /* ------------------------------ CALLBAKCS ----------------------------- */
  async function onActivateUser(e) {
    await axios({
      method: "post",
      baseURL: `${baseURL}/user/updateUser`,
      data: { userId: mentorId, updatedUser: { isMentorAvailable: e } },
    })
      .then((res) => {})
      .catch();
  }

  /* --------------------------- RENDERING HELPERS ------------------------- */
  function getMentorProfilSection() {
    return (
      <Content className="my-mentor-section">
        <div>
          <Row>
            <Col span={4} offset={2}>
              <div className="profil-section">
                <Row>
                  <Image src={FOULAN}></Image>
                </Row>
                <Row className="mentor-details">
                  <Col>
                    <Row>
                      <Col>
                        <Briefcase size={17} />
                      </Col>
                      <Col>{mentor?.currentWork}</Col>
                    </Row>
                  </Col>
                </Row>
                <Row justify="center">
                  <Button className="mentor-profile-button">Profile</Button>
                </Row>
              </div>
              <br></br>
              <br></br>
              <div className="profile-Info-Box">
                <div className="profile-Info-Box-Job" justify="center">
                  <Image
                    className="profile-Info-Box-Job-IMG"
                    src={Calend}
                  ></Image>
                  <label className="profile-Info-Box-Job-label">
                    {mentor?.currentWork}
                  </label>
                </div>
                <div className="profile-Info-Box-Job" justify="center">
                  <Image className="profile-Info-Box-Job-IMG" src={$$}></Image>
                  <label className="profile-Info-Box-Job-label">$$$$$</label>
                </div>
                <div className="profile-Info-Box-Job" justify="center">
                  <Image
                    className="profile-Info-Box-Job-IMG"
                    src={Calend}
                  ></Image>
                  <label className="profile-Info-Box-Job-label">
                    Year of experience
                  </label>
                </div>
                <div className="profile-Info-Box-Job" justify="center">
                  <Image
                    className="profile-Info-Box-Job-IMG"
                    src={Awards}
                  ></Image>
                  <label className="profile-Info-Box-Job-label">Awards</label>
                </div>
                <div
                  className="profile-Info-Box-Job-industries"
                  justify="center"
                >
                  <label className="profile-Info-Box-Job-label">
                    Industries worked in{" "}
                  </label>
                  <p className="profile-Info-Box-Job-para-industries">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                  </p>
                </div>
              </div>
            </Col>

            <Col span={14} offset={2} className="text-section">
              <Row justify="align">
                <Col span={12} offset={1}>
                  <Row className="text-Title">
                    <Col>
                      {`  Hi I'm ${mentor?.firstName}  ${mentor?.lastName} ${mentor?.isMentorAvailable}`}
                    </Col>
                    <Col offset={1}>
                      <Switch
                        defaultChecked={mentor?.isMentorAvailable}
                        onChange={(e) => {
                          onActivateUser(e);
                        }}
                        disabled={currentUser._id !== mentorId}
                      />
                    </Col>
                    <Col>{mentor?.isMentorAvailable}</Col>
                  </Row>
                  <Row className="text-Para">{mentorCV?.aboutMe}</Row>
                </Col>
                <Col span={8} offset={2}>
                  <div className="text-Title"> Expertise </div>
                  <br></br>
                  <Row justify="align">
                    {mentorCV?.skills?.map((skill) => (
                      <div className="text-box">{skill}</div>
                    ))}
                  </Row>
                </Col>
              </Row>

              <br></br>

              <div className="service-Box">
                {mentor?.mentorServices
                  ?.filter((sevice) => sevice.enabled)
                  ?.map((service) => {
                    return (
                      <Row
                        className="service-mentor"
                        align="middle"
                        onClick={() => {
                          setSelectedService(service);
                          setIsModalVisible(true);
                        }}
                      >
                        <Col span={16}>
                          <div className="text-Title-Service">
                            {service?.name}
                          </div>
                          <div className="text-Text-Service">
                            {service.description}
                          </div>
                        </Col>

                        <Col span={8} style={{ margin: "0 auto" }}>
                          <Row style={{ justifyContent: "end" }}>
                            <div>
                              <label className="price-Title-Service">
                                {service?.price
                                  ? `${service?.price} DT`
                                  : "Free"}
                              </label>
                              &nbsp;&nbsp;&nbsp;
                              {currentUser._id !== mentorId && (
                                <Button className="button-Title-Service">
                                  Request
                                </Button>
                              )}
                              &nbsp;
                            </div>
                          </Row>
                        </Col>
                      </Row>
                    );
                  })}
              </div>
            </Col>
          </Row>
        </div>
      </Content>
    );
  }

  function getMyTestsSection() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      autoplaySpeed: 2000,
      draggable: false,
      cssEase: "linear",
      raggable: true,
      autoplay: true,
      backgroundImage: `url(${Backg})`,
      backgroundPosition: "center",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
    };
    return (
      <Content className="mentor-profil-page">
        <Slider {...settings}>
          <div className="slide-test-mentor">
            <Row align="middle">
              <Col span={6} offset={6}>
                <Row>
                  <h1 className="slider-subtitre-mentor">
                    <p className="p1"></p>{" "}
                  </h1>
                </Row>
                <h2 className="slider-titre-mentor">
                  {" "}
                  <p className="p1-mentor">Refer a friend </p>{" "}
                </h2>
                <h5 className="slider-text">
                  <p>Invite your friends to join.</p>
                  <p>Help them skill up and win valuable rewards. </p>
                </h5>
                <br></br>
                <Button
                  style={{
                    width: "50%",
                    boxSizing: "content-box",
                    borderRadius: "50px",
                    color: "white",
                    backgroundColor: "#9900ff",
                    height: "27px",
                    fontWeight: "600",
                    fontSize: "90%",
                    top: "-3px",
                  }}
                >
                  <label> Invite a friend</label>
                </Button>
              </Col>
              <Col span={12}></Col>
            </Row>
          </div>
        </Slider>
      </Content>
    );
  }

  function getSubmitReviewSection() {
    return (
      <Content>
        <div style={{ padding: "8%" }}>
          <Form>
            <Row className="mentor-Submit-review-page">
              <Col span={4}>
                <div className="Submit-review-page">
                  <label className="Submit-review-page-title">
                    Submit review
                  </label>
                  <Rate
                    className="Submit-review-page-stars"
                    allowHalf
                    defaultValue={3}
                  />
                  <Form.Item name="firstName">
                    <Input
                      className="Submit-review-page-input-name"
                      placeholder="Name"
                    />
                  </Form.Item>

                  <Form.Item name="textArea">
                    <TextArea
                      showCount
                      maxLength={55}
                      style={{ borderColor: "red" }}
                      placeholder="Review message"
                    />
                  </Form.Item>
                </div>
              </Col>

              <Col span={18} offset={2}>
                <div className="mentor-Review-All-Box">
                  <Row style={{ padding: "2%" }}>
                    <div className="mentor-Review-Box" align="middle">
                      <Rate
                        className="mentor-Review-Box-stars"
                        allowHalf
                        defaultValue={3}
                      />
                      <p className="mentor-Review-Box-title">
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry.{" "}
                      </p>
                      <p className="mentor-Review-Box-Nom">Foulan FOULANI </p>
                    </div>

                    <div className="mentor-Review-Box" align="middle">
                      <Rate
                        className="mentor-Review-Box-stars"
                        allowHalf
                        defaultValue={3}
                      />
                      <p className="mentor-Review-Box-title">
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry.{" "}
                      </p>
                      <p className="mentor-Review-Box-Nom">Foulan FOULANI </p>
                    </div>

                    <div className="mentor-Review-Box" align="middle">
                      <Rate
                        className="mentor-Review-Box-stars"
                        allowHalf
                        defaultValue={3}
                      />
                      <p className="mentor-Review-Box-title">
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry.{" "}
                      </p>
                      <p className="mentor-Review-Box-Nom">Foulan FOULANI </p>
                    </div>
                  </Row>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
      </Content>
    );
  }
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="mentor-profil-page">
      <TopBar />
      <RequestMentoringModal
        isModalVisible={isModalVisible}
        currentUserId={currentUser?._id}
        mentorId={mentor?._id}
        setIsModalVisible={setIsModalVisible}
        service={selectedService}
      />
      <Content className="mentor-profil-content">
        <br></br>
        {getMentorProfilSection()}
        <br></br>
        {getSubmitReviewSection()}
        <br></br>
        {getMyTestsSection()}
      </Content>
    </Content>
  );
}
export default MentorProfil;
