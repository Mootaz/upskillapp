// packages
import React, { useState } from "react";
import { Button, Modal, message, Steps, Form, Input, Checkbox } from "antd";

// constants
import { baseURL } from "../../../constants";
import axios from "axios";

// components
import UploadCV from "../../Shared/components/uploadCV";

//styles
import "./index.less";

// Scope components
const { Step } = Steps;

function BuildCVPage({ modalVisible, setModalVisible }) {
  // States

  const [userData, setUserData] = useState({});

  // Hooks
  const [form] = Form.useForm();
  // Callbacks
  const onFinish = (values) => {};
  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };
  // LocalVariables
  const steps = [
    {
      title: "CV",
      content: <UploadCV userData={userData} onChange={setUserData} />,
    },
    {
      title: "Preferences",
      content: (
        <Form
          form={form}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          autoComplete="off"
          {...formItemLayout}
        >
          <Form.Item
            label="email"
            name="email"
            rules={[
              {
                required: true,
                message: "Please input your email!",
              },
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Email confirmation"
            name="emailConfirmation"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("email") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error("should be  same as email"));
                },
              }),
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="acceptPolicy"
            valuePropName="checked"
            required
            rules={[{ required: true, message: "Should be checked" }]}
          >
            <Checkbox>Accept the privacy policy</Checkbox>
          </Form.Item>
          <Form.Item name="subscribe" valuePropName="checked">
            <Checkbox>Suscribe to newletter</Checkbox>
          </Form.Item>
        </Form>
      ),
    },
  ];

  // uplaodCV
  // On file upload (click the upload button)
  async function fileUpload() {
    // Create an object of formData
    const data = new FormData();

    data.append("test", " un petit test");

    axios({
      method: "post",
      baseURL: `${baseURL}/user/uploadCV`,
      withCredentials: true,
      headers: { "Content-Type": "multipart/form-data" },
      body: data,
    })
      .then((response) => {
        message.success(response.data.message);
      })
      .catch((error) => {
        message.error(error);
      });
  }

  // on click Done
  function sendEmail() {
    form.validateFields();
    message.success("CV uploaded successfully");
    fileUpload();
    setModalVisible(false);
  }

  const [current, setCurrent] = useState(0);

  const next = () => {
    setCurrent(current + 1);
    if (current === 0) {
      const user = form.getFieldsValue();
      setUserData({ ...userData, ...user });
    }
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  return (
    <>
      <Modal
        title="Create User"
        visible={modalVisible}
        destroyOnClose={true}
        onCancel={() => {
          setModalVisible(false);
        }}
        footer={null}
      >
        <Steps current={current}>
          {steps.map((item) => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">{steps[current].content}</div>
        <div className="steps-action">
          {current < steps.length - 1 && (
            <Button type="primary" onClick={() => next()}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button type="primary" onClick={() => sendEmail()}>
              Done
            </Button>
          )}
          {current > 0 && (
            <Button
              style={{
                margin: "0 8px",
              }}
              onClick={() => prev()}
            >
              Previous
            </Button>
          )}
        </div>
      </Modal>
    </>
  );
}

export default BuildCVPage;
