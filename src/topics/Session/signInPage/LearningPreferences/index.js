// packages
import React, { useState } from "react";
import { Col, Row, Image, Badge } from "antd";
// assess
import design from "../../../../assets/loginModal/design.png";
import web_development from "../../../../assets/loginModal/web-development.png";
import digital_marketing from "../../../../assets/loginModal/digital-marketing.png";
import modeling from "../../../../assets/loginModal/modeling.png";
import software_engineering from "../../../../assets/loginModal/software-engineering.png";
import project_management from "../../../../assets/loginModal/project-managment.png";
//styles
import "./index.less";
import { PlusOutlined } from "@ant-design/icons";
import { Check } from "tabler-icons-react";

function LearningPreferences({ userData, onChange }) {
  // states
  const [changed, setChanged] = useState(0);
  const learningPref = [
    { label: "Building information modelling", value: "modeling" },
    { label: "UI & UX Design", value: "design" },
    { label: "Degital marketing", value: "digital-marketing" },
    { label: "Project management", value: "project-management" },
    { label: "Software engineering", value: "software-engineering" },
    { label: "Web development", value: "web-development" },
  ];
  /* -------------------------------- callbacks ------------------------------- */
  function onClickLab(lab) {
    if (!userData.learningPreferences) {
      userData.learningPreferences = [];
    }
    if (!userData.learningPreferences.includes(lab.value)) {
      userData.learningPreferences.push(lab.value);
    } else {
      userData.learningPreferences = userData.learningPreferences.filter(
        (lb) => lb !== lab.value
      );
    }
    onChange(userData);
    setChanged(changed + 1);
  }

  function getImage(value) {
    switch (value) {
      case "design":
        return design;

      case "modeling":
        return modeling;

      case "web-development":
        return web_development;

      case "digital-marketing":
        return digital_marketing;

      case "software-engineering":
        return software_engineering;

      case "project-management":
        return project_management;

      default:
        break;
    }
  }
  /* ---------------------------- rendering helpers --------------------------- */
  function getLabs() {
    return (
      <Row className="preference-row" justify="center">
        {learningPref?.map((learning) => (
          <Col
            span={7}
            offset={1}
            onClick={() => {
              onClickLab(learning);
            }}
          >
            <Row>
              <Badge
                count={
                  userData?.learningPreferences?.includes(learning.value) ? (
                    <Check />
                  ) : (
                    <PlusOutlined />
                  )
                }
              >
                <Image preview={false} src={getImage(learning.value)}></Image>
              </Badge>
            </Row>
            <Row className="image-label">{learning.label}</Row>
          </Col>
        ))}
      </Row>
    );
  }
  /* -------------------------------- rendering ------------------------------- */
  return (
    <div className="learning-preferences">
      <Row className="introduction-row" justify="center">
        <Col>What are you intrested to learn ?</Col>
      </Row>
      <Row justify="center">
        <Col span={24}>{getLabs()}</Col>
      </Row>
    </div>
  );
}

export default LearningPreferences;
