// packages
import React from "react";
import { Form, Select, Row, Col, Layout, Tooltip } from "antd";

//styles
import "./index.less";
const { Content } = Layout;

function UserDetails({ userData, onChange }) {
  // Hooks
  const [form] = Form.useForm();
  // functions
  function onFieldChange(key, value) {
    if (!userData?.userDetails) {
      userData.userDetails = {};
    }
    userData.userDetails[key] = value;
    onChange(userData);
  }
  return (
    <Content className="wrapper">
      <Form
        form={form}
        name="basic"
        initialValues={userData?.userDetails}
        autoComplete="off"
      >
        <Row>
          <Col span={11}>
            <Form.Item name="USLUse">
              <Select
                placeholder="A quoi  vous servra USL?"
                onChange={(e) => onFieldChange("USLUse", e)}
                mode="multiple"
                allowClear
                maxTagCount="responsive"
                options={[
                  { value: "Formation", label: "Formation" },
                  { value: "Community", label: "Community" },
                  { value: "CV_Template", label: "CV Template" },
                  { value: "career", label: "Trouver un travail" },
                  { value: "Freelance", label: "Freelance" },
                  { value: "Test-Certif", label: "Test-Certif" },
                ]}
              ></Select>
            </Form.Item>
          </Col>
          <Col offset={1} span={11}>
            <Form.Item name="role">
              <Select
                placeholder="Rôle"
                onChange={(e) => onFieldChange("role", e)}
                allowClear
                options={[
                  { value: "business-manager", label: "Gestion d’entreprise" },
                  { value: "project-manager", label: "Gestion de projet" },
                  {
                    value: "product-development",
                    label: "Développement de produit ",
                  },
                  { value: "graphic-design", label: "Design Graphique" },
                  {
                    value: "Développement-VR-AR",
                    label: "Développement VR/AR",
                  },
                  { value: "AI", label: "Intelligence Artificielle" },
                  { value: "BI", label: "Business Intelligence" },
                  { value: "power-engineering", label: "Génie Energétique " },
                  {
                    value: "electrical-engineering",
                    label: "Génie Electrique ",
                  },
                  { value: "civil-engineering", label: "Génie Civil " },
                  { value: "mecanic-engineering", label: "Génie Mécanique " },
                  { value: "architecture", label: "Architecture " },
                  {
                    value: "scrum-master",
                    label: "Coach Agile / Scrum master",
                  },
                  { value: "UX/UI-Design ", label: "UX/UI Design" },
                  {
                    value: "software-development ",
                    label: " Développement Informatique  ",
                  },
                  {
                    value: "marketing-communication ",
                    label: "Marketing et Communication",
                  },
                  { value: "Operations", label: "Operations" },
                  { value: "Freelance", label: "Freelance" },
                  { value: "sells", label: "Vente / succès consommateurs" },
                  { value: "teacher", label: "Enseignant" },
                  { value: "student", label: "Etudiant" },
                  { value: "other", label: "Autre" },
                ]}
              ></Select>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <Form.Item name="description">
              <Select
                placeholder="Comment vous décrieriez-vous?"
                onChange={(e) => onFieldChange("description", e)}
                options={[
                  { value: "student", label: "Etudiant" },
                  { value: "Freelance", label: "Freelance" },
                  { value: "job-seeker", label: "Chercheur d'emploi" },
                  { value: "teacher", label: "Enseignant" },
                  { value: "business-manager", label: "Chef d’entreprise " },
                  { value: "director", label: "Directeur" },
                  { value: "team-manager", label: "Chef d'équipe" },
                  { value: "team-member", label: "Membre d'une équipe" },
                ]}
              ></Select>
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item name="company-size">
              <Tooltip
                title="Quelle est la taille de votre entreprise ?"
                color={"#FF9900"}
              >
                <Select
                  placeholder="Quelle est la taille de votre entreprise ?"
                  onChange={(e) => onFieldChange("company-size", e)}
                  options={[
                    { value: "only-me", label: "Moi uniquement" },
                    { value: "02-50", label: "02 à 50" },
                    { value: "51-250", label: "51 à 250" },
                    { value: "251-1000", label: "251 à 1000" },
                    { value: "1001-5000", label: "1001 à 5000" },
                    { value: "5001-more ", label: "5001 et plus " },
                  ]}
                ></Select>
              </Tooltip>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <Form.Item name="lms-use">
              <Tooltip
                title="Avez-vous utilisé des LMS(Learning management system)?"
                color={"#FF9900"}
              >
                <Select
                  placeholder="Avez-vous utilisé des LMS(Learning management system)? "
                  onChange={(e) => onFieldChange("lms-use", e)}
                  options={[
                    {
                      value: "never",
                      label: "Je n’en ai jamais utilisé auparavant ",
                    },
                    { value: "few", label: "J’en ai utilisé quelque uns" },
                    { value: "ever", label: "J’en ai utilisé beaucoup " },
                  ]}
                ></Select>
              </Tooltip>
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item name="know-usl">
              <Tooltip
                title="Comment avez-vous entendu parler de USL ?"
                color={"#FF9900"}
              >
                <Select
                  maxTagCount="responsive"
                  mode="multiple"
                  placeholder="Comment avez-vous entendu parler de USL ?"
                  onChange={(e) => onFieldChange("know-usl", e)}
                  options={[
                    { value: "LinkedIn", label: "LinkedIn" },
                    { value: "YouTube", label: "YouTube" },
                    {
                      value: "facebook-instagram",
                      label: "Facebook/Instagram",
                    },
                    { value: "job-school", label: "Travail/ Ecole" },
                    {
                      value: "friend-recommandation",
                      label: "Ami / Recommandation",
                    },
                    { value: "google", label: "Google (moteur de recherche)" },
                    { value: "tv", label: "Télévision" },
                    { value: "radio", label: "Podcast/Radio" },
                    { value: "other", label: "Autre" },
                  ]}
                />
              </Tooltip>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Content>
  );
}

export default UserDetails;
