// packages
import React, { useState } from "react";
import { Button, Col, Row } from "antd";

//styles
import "./index.less";
import { Heart, HeartPlus } from "tabler-icons-react";

function UserPreferences({ userData, onChange }) {
  // states
  const [changed, setChanged] = useState(0);
  const labs = [
    { label: "Build my CV - Skills Assessment", value: "build-cv" },

    { label: "BIM Lab - Agile Lab", value: "bim-lab" },
    { label: "Community", value: "Community" },
    { label: " Tests - Certifications ", value: "test-certif" },
  ];
  // callbacks
  function onClickLab(lab) {
    if (!userData.preferredLabs) {
      userData.preferredLabs = [];
    }
    if (!userData.preferredLabs.includes(lab.value)) {
      userData.preferredLabs.push(lab.value);
    } else {
      userData.preferredLabs = userData.preferredLabs.filter(
        (lb) => lb !== lab.value
      );
    }
    onChange(userData);
    setChanged(changed + 1);
  }

  // rendering helpers
  function getLabs() {
    return labs?.map((lab) => (
      <Row className="preference-row" justify="center">
        <Button className="preference-button" onClick={() => onClickLab(lab)}>
          <Row justify="center" align="middle">
            <Col span={18}> {lab.label}</Col>
            <Col span={2} offset={2}>
              {userData?.preferredLabs?.includes(lab.value) ? (
                <Heart />
              ) : (
                <HeartPlus color="grey" />
              )}
            </Col>
          </Row>
        </Button>
      </Row>
    ));
  }
  return (
    <div className="user-preferences">
      <Row className="introduction-row" justify="center">
        <span>
          Choose the labs that intrests you , <br /> and we will provide an
          oriented experience.
        </span>
      </Row>
      <Row justify="center">
        <Col span={24}>{getLabs()}</Col>
      </Row>
    </div>
  );
}

export default UserPreferences;
