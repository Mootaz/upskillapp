/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */
// packages
import { Button, Form, Input, message } from "antd";
import axios from "axios";
import { useParams } from "react-router-dom";
import { baseURL, strongRegex } from "../../../constants";

/* -------------------------------------------------------------------------- */
/*                               Implementation                               */
/* -------------------------------------------------------------------------- */
function ResetPassword() {
  /* ---------------------------------- Hooks --------------------------------- */
  const [form] = Form.useForm();
  let { token } = useParams();
  /* -------------------------------- Constants -------------------------------- */
  
  const onFinish = (values) => {
    axios({
      method: "post",
      url: `${baseURL}/user/reset-password`,
      data: { password: form.getFieldValue("password"), token },
    })
      .then((res) => {
        message.success("password changed");
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  const onFinishFailed = (errorInfo) => {};
  /* -------------------------------- Rendering ------------------------------- */
  return (
    <>
      <br></br>
      <Form
        form={form}
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinishFailed={onFinishFailed}
        onFinish={onFinish}
      >
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
            () => ({
              validator(_, value) {
                if (!value || value.match(strongRegex)) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error("The password does not match the requirement")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Password Confirmation"
          name="passwordConfirmation"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
            () => ({
              validator(_, value) {
                if (!value || value.match(form.getFieldValue("password"))) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error("The password does not match the requirement")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Update Password
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

export default ResetPassword;
