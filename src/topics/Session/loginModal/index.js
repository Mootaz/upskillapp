// packages
import React, { useEffect } from "react";
import { Button, Form, Input, Image, message, Row, Col, Layout } from "antd";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { GoogleLogin } from "react-google-login";
import { useSelector } from 'react-redux';

// constants
import { baseURL } from "../../../constants";
import axios from "axios";
import { $googleLogin, $login, selectUserRole } from "../slice";
import { useDispatch } from "react-redux";

// styles
import "./index.less";
import { $setModalContext } from "../../layouts/slice";
import roles from "../../Shared/Entities/roles";
import { useNavigate } from "react-router-dom";

// Assets
import logosignin from "../../../assets/logo2.png";
import loginImage from "../../../assets/modal-img.png";

// scoped components
const { Content } = Layout;
function LoginModal() {
  const clientId =
    "691444625528-vuhftrv2rlvi2r48n4j1q2tako4ig4vh.apps.googleusercontent.com";
  /* ********************************** HOOKS ********************************* */
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const navigate = useNavigate();
  /* ******************************** CALLBACKS ******************************* */
  const currentRole = useSelector(selectUserRole);
  useEffect(()=>{
    console.log(currentRole);
    switch (currentRole) {
      case "instructor":
        navigate("/learning")
        break;

        case "mentor":
          navigate("/mentorLab")
          break;

          case "student":  
            navigate("/panier")
            break;

            case "recruter":
              navigate("/recruterPage")
              break;
    
      default:
        break;
    }
  },[currentRole])
  
  const onFinish = (values) => {
    const { email, password } = values;
    dispatch($login({ email, password })).then(


      (action) => {

        if (action.error) {
          console.log("action error ", action.error);

        } else {
          dispatch($setModalContext(null));
          message.success("user logged in successfully");

        
        }
      })
  };

  const onFinishFailed = (errorInfo) => { };

  // @Todo : Refactor : Add another page for email validation
  const onSendEmailForget = () => {
    const email = form.getFieldValue("email");
    if (typeof email != "undefined") {
      axios({
        method: "post",
        url: `${baseURL}/user/forgotPassword`,
        data: { email },
      })
        .then((res) => {
          message.success(res.data);
        })
        .catch((error) => {
          message.error(error.response.data.message);
        });
    } else message.error("Fill in the form");
  };

  // handle fb login
  const responseFacebook = (res) => {
    const user = {
      email: res.email,
      isForeignUser: true,
    };

    dispatch($googleLogin(user)).then((action) => {
      if (action.error) {
        message.error("login error");
      } else {
        dispatch($setModalContext(null));
        message.success("user logged in successfully");
      }
    });
  };

  // handle google login
  function onSuccessGoogle(res) {
    const user = {
      firstName: res.profileObj.givenName,
      lastName: res.profileObj.familyName,
      email: res.profileObj.email,

      isForeignUser: true,
    };

    dispatch($googleLogin(user)).then((action) => {
      if (action.error) {
        message.error("login error");
      } else {
        dispatch($setModalContext(null));
        message.success("user logged in successfully");
      }
    });
  }

  /* ******************************** RENDERING ******************************* */

  return (
    <Content className="main-container1">
      <Content className="image-container1">
        <Image className="login-image" preview={false} src={loginImage}></Image>
      </Content>
      <div className="form-container1">
        <Row className="logorow">
          <Image
            className="logosignin"
            preview={false}
            src={logosignin}
          ></Image>
        </Row>

        <Row className="text-row" justify="center">
          <span>
            USL is more than an educational platform. Join our community
          </span>
          <br />
          <span>
            and join exclusive challenges, rewards and job opportunities.
          </span>
          <br />
          <span>we can't wait for you to get aboard!</span>
        </Row>
        <Row justify="center" className="text-row">
          <GoogleLogin
            clientId={clientId}
            buttonText="Sign in with Google"
            onSuccess={(e) => {
              onSuccessGoogle(e);
            }}
            onFailure={(e) => {
              message.error("Service  not available");
            }}
            cookiePolicy={"single_host_origin"}
            autoLoad={false}
            render={(renderProps) => (
              <Button
                type="primary"
                className="login-button google"
                onClick={renderProps.onClick}
              >
                Sign in with Google
              </Button>
            )}
          />
        </Row>

        <Row justify="center" className="text-row">
          <FacebookLogin
            appId="395198189249942"
            autoLoad={false}
            fields="name,email,picture"
            callback={responseFacebook}
            render={(renderProps) => (
              <Button
                type="primary"
                className="login-button"
                onClick={renderProps.onClick}
              >
                Sign in with Facebook
              </Button>
            )}
          />
        </Row>

        <Row className="text-row" justify="center">
          <span>Or sign up with your email</span>
        </Row>
        <Row className="form-row" style={{ marginLeft: "0%", width: "100%" }}>
          <Form
            form={form}
            name="basic"
            onFinishFailed={onFinishFailed}
            onFinish={onFinish}
          >
            <Row justify="center">
              <Col span={9}>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: "Please input your Email!",
                    },
                  ]}
                >
                  <Input placeholder="Email" />
                </Form.Item>
              </Col>
              <Col span={9} offset={1}>
                <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "Please input your password!",
                    },
                  ]}
                >
                  <Input.Password placeholder="Password" />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item>
              <Row className="Button-Get-Started-container" justify="center">
                <Button
                  className="Button-Get-Started"
                  type="primary"
                  htmlType="submit"
                  style={{
                    width: "50%",
                    borderRadius: "10px",
                    fontWeight: "600",
                    fontSize: "1em",
                    borderColor: "#fe9816",
                    background: "#fe9816",
                  }}  
                >
                  Login
                </Button>
              </Row>
            </Form.Item>
          </Form>
        </Row>

        <Row justify="center" className="forgot-password">
          <div
            onClick={() => {
              onSendEmailForget();
            }}
          >
            Forgot password ?
          </div>
        </Row>
        <Row justify="center" className="register-button">
          <Col offset={6} span={18}>
            <span>Don't have an account?</span>
            <span
              className="signin-button"
              onClick={() => {
                dispatch($setModalContext("Signup"));
              }}
            >
              {`   Register now`}
            </span>
          </Col>
        </Row>
      </div>
    </Content>
  );
}

export default LoginModal;
