/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React, { useState } from "react";
import ImgCrop from "antd-img-crop";
import PhoneInput from "react-phone-number-input";
import {
  Layout,
  Form,
  Input,
  Select,
  Radio,
  Checkbox,
  Row,
  Col,
  message,
  Button,
  Upload,
} from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  $insertUserCV,
  selectSessionUser,
  selectUserCV,
} from "../../Session/slice";
// UI local components
import DatePicker from "../../Shared/components/inputs/DatePicker";
import CountrySelect from "../../Shared/components/inputs/countrySelect";

// constants
import { baseURL } from "../../../constants";
import axios from "axios";
//styles
import "./index.less";

import fr from "react-phone-number-input/locale/fr.json";

import { hobbies } from "../../Shared/Entities/hobbies";
import nationalities from "../../Shared/mocks/nationalities";
//assets

// scoped componenets

const { Content } = Layout;
const { TextArea } = Input;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function Profile() {
  /* ---------------------------------- HOOKS --------------------------------- */
  const [personalInfoForm] = Form.useForm();
  const currentUser = useSelector(selectSessionUser);
  const dispatch = useDispatch();
  const currentUserCV = useSelector(selectUserCV);
  // states
  const [selectedFile, setSelectedFile] = useState();
  /* -------------------------------- CALLBACKS ------------------------------- */
  function onSaveUserData() {
    const userData = {
      userPersonalInfo: personalInfoForm.getFieldsValue(),
    };

    dispatch($insertUserCV({ _id: currentUser._id, ...userData }))
      .then((action) => {
        onUploadIMG(selectedFile);
        message.success("Changes saved !");
      })
      .catch((error) => {
        message.error("An error occured");
      });
  }
  /* ---------------------------- RENDERIN HELPERS ---------------------------- */
  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };
  function uploadImg() {
    return (
      <ImgCrop rotate>
        {
          <Upload
            maxCount={1}
            onChange={(e) => {
              setSelectedFile(e.file.originFileObj);
            }}
            listType="picture-card"
          >
            + Upload
          </Upload>
        }
      </ImgCrop>
    );
  }

  // uplaodIMG
  // On file upload (click the upload button)
  const onUploadIMG = (e) => {
    // Create an object of formData
    const data = new FormData();
    data.append("img", e);

    axios({
      method: "post",
      baseURL: `${baseURL}/user/uploadIMG`,
      headers: { "Content-Type": "multipart/form-data" },
      data,
      body: data,
    })
      .then((response) => {
        message.success(response.data.message);
      })
      .catch((error) => {
        message.error(error);
      });
  };

  function getPersonalInformationsForm() {
    return (
      <Form
        form={personalInfoForm}
        {...formItemLayout}
        initialValues={{ ...currentUser, ...currentUserCV }}
      >
        <Row>
          <Col span={11}>
            <Form.Item
              label="First name"
              name="firstName"
              required
              rules={[
                {
                  required: true,
                  message: "Please input your first name!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item
              label="Last name"
              name="lastName"
              required
              rules={[
                {
                  required: true,
                  message: "Please input your last name!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <Form.Item label="Birth Date" name="birthDate">
              <DatePicker />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item label="Phone" name="phone">
              <PhoneInput
                placeholder="Enter phone number"
                defaultCountry="TN"
                international
                labels={fr}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <Form.Item
              label="Address"
              name="address"
              required
              rules={[
                {
                  required: true,
                  message: "Please input your address!",
                },
              ]}
            >
              <CountrySelect onChange={(e) => {}} />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item
              required
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <b>Social accounts</b>
        <Row>
          <Col span={11}>
            <Form.Item label="Facebook" name="facebookLink">
              <Input />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item label="Instagram" name="instagramLink">
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <Form.Item label="LinkedIn" name="linkedinLink">
              <Input />
            </Form.Item>
          </Col>
          <Col span={11} offset={1}>
            <Form.Item label="Whatsapp" name="whatsappLink">
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item label="Drive license" name="driveLicence">
          <Radio.Group>
            <Radio value="yes">yes</Radio>
            <Radio value="no">No</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Nationality" name="nationalities">
          <Select
            options={nationalities.map((nationality) => ({
              value: nationality.label,
              label: nationality.label,
            }))}
            mode="multiple"
          />
        </Form.Item>
        <Form.Item name="preferredSchedule" label="Preferred schedule">
          <Checkbox.Group>
            <Row>
              <Col>
                <Checkbox value="part-time">Part time</Checkbox>
              </Col>
              <Col>
                <Checkbox value="full-time">Full time</Checkbox>
              </Col>
              <Col>
                <Checkbox value="remote">Remote</Checkbox>
              </Col>
              <Col>
                <Checkbox value="on-site">On site</Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
        </Form.Item>
        <Form.Item label="Interests and hobbies" name="hobbies">
          <Select
            mode="multiple"
            allowClear
            options={hobbies.map((hobby) => hobby)}
          />
        </Form.Item>
        <Form.Item label="About me" name="aboutMe">
          <TextArea placeholder="maxLength is 300" showCount maxLength={650} />
        </Form.Item>
      </Form>
    );
  }

  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="profile-content">
      <Row justify="end">
        <Button
          type="primary"
          className="upload-cv-button"
          onClick={() => {
            onSaveUserData();
          }}
        >
          Save
        </Button>
      </Row>
      {uploadImg()}
      {getPersonalInformationsForm()}
      <Row justify="end">
        <Button
          type="primary"
          className="upload-cv-button"
          onClick={() => {
            onSaveUserData();
          }}
        >
          Save
        </Button>
      </Row>
    </Content>
  );
}
export default Profile;
