import { Button } from "antd";
import { useState } from "react";
import ZoomMeeting from "..";




function Test() {
  const [joinMeeting, setJoinMeeting] = useState(false);

  return joinMeeting ? (
    <ZoomMeeting />
  ) : (
    <Button
      onClick={function cb() {
        setJoinMeeting(true);
      }}
    >
      Join meeting
    </Button>
  );
}

export default Test;
