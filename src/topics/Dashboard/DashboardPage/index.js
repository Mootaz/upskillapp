/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

// lib dependencies
import React from "react";
import { Col, Divider, Layout, Row, Image } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";

// local UI components
import SiderMenu from "./siderMenu";
import App from "../../../App";
import { IconAward } from "@tabler/icons";

import Avatar from "antd/lib/avatar/avatar";
import { UserOutlined } from "@ant-design/icons";

// Assests
import { ReactComponent as IMAGE_1 } from "../../../assets/dashboard/image1.svg";
import { ReactComponent as IMAGE_2 } from "../../../assets/dashboard/image2.svg";
import IMAGE_3 from "../../../assets/dashboard/image3.svg";
import { ReactComponent as IMAGE_4 } from "../../../assets/dashboard/image4.svg";
//styles
import "./index.less";
import { Outlet } from "react-router-dom";

// scoped componenets

const { Content } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function DashbaodPage() {
  /* ---------------------------- Rendering Helpers ---------------------------- */
  const friendsList = [
    { name: "John Doe" },
    { name: "Silivia " },
    { name: "Thania" },
    { name: "Jessica Doe" },
    { name: "Jhames Doe" },
    { name: "John Doe" },
    { name: "Silivia " },
    { name: "Thania" },
  ];
  /* -------------------------------- CALLBACKS ------------------------------- */

  /* ---------------------------- RENDERIN HELPERS ---------------------------- */

  return (
    <App>
      <SiderMenu />
      <Content className="dashboard-page-layout">
        <Content className="dashboard-page-content">
          <Content className="basic-info">
            <Row className="Dash-All-Box">
              <div className="statistics-col2">
                <Row>
                  <Col align="middle" justify="center" span={18}>
                    <Row className="first-row2">My resume level </Row>
                    <Row className="second-row2">89%</Row>
                    <Row className="third-row2">
                      Complete my CV <CaretRightOutlined />
                    </Row>
                  </Col>
                  <Col span={5} className="image-col2">
                    <div>
                      <Image className="picture12" src={IMAGE_3} />
                    </div>
                  </Col>
                </Row>
              </div>

              {/* <div className="statistics-col2">
                <Row>
                  <Col align="middle" justify="center" span={18}>
                    <Row className="first-row2">My resume level </Row>
                    <Row className="second-row2">89%</Row>
                    <Row className="third-row2">
                      Complete my CV <CaretRightOutlined />
                    </Row>
                  </Col>
                  <Col span={5} className="image-col2">
                    <Avatar
                      className="picture12"
                      icon={<Icon component={IMAGE_3} />}
                    />
                  </Col>
                </Row>
              </div>

              <div className="statistics-col2">
                <Row>
                  <Col align="middle" justify="center" span={18}>
                    <Row className="first-row2">My resume level </Row>
                    <Row className="second-row2">89%</Row>
                    <Row className="third-row2">
                      Complete my CV <CaretRightOutlined />
                    </Row>
                  </Col>
                  <Col span={5} className="image-col2">
                    <Avatar
                      className="picture12"
                      icon={<Icon component={IMAGE_3} />}
                    />
                  </Col>
                </Row>
              </div>

              <div className="statistics-col2">
                <Row>
                  <Col align="middle" justify="center" span={18}>
                    <Row className="first-row2">My resume level </Row>
                    <Row className="second-row2">89%</Row>
                    <Row className="third-row2">
                      Complete my CV <CaretRightOutlined />
                    </Row>
                  </Col>
                  <Col span={5} className="image-col2">
                    <Avatar
                      className="picture12"
                      icon={<Icon component={IMAGE_3} />}
                    />
                  </Col>
                </Row>
              </div>*/}
            </Row>

            <Row className="second-section" justify="center">
              <Col span={17} className="outlet-col">
                <Outlet />
              </Col>

              <Col offset={1} span={5} className="friends-column">
                <div className="friends-content-wrapper">
                  <Row className="title"> Friends List </Row>
                  {friendsList.map((friend) => (
                    <Row>
                      <Col span={5}>
                        <Avatar icon={<UserOutlined />} />
                      </Col>
                      <Col className="friend-name" offset={2} span={8}>
                        {friend.name}
                      </Col>

                      <Col offset={2} span={5}>
                        <IconAward color="red" />
                      </Col>
                      <Divider></Divider>
                    </Row>
                  ))}
                </div>
              </Col>
            </Row>
          </Content>
        </Content>
      </Content>
    </App>
  );
}
export default DashbaodPage;
