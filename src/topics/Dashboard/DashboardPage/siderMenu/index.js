/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Icon from "@ant-design/icons";
// UI lib components
import { Col, Divider, Layout, Menu, message, Row,Popconfirm } from "antd";
import { ChevronsLeft, ChevronsRight } from "tabler-icons-react";
// ASSETS
import { ReactComponent as DASHBOARD } from "../../../../assets/siderMenu/dashboard.svg";
import { ReactComponent as PROFILE } from "../../../../assets/siderMenu/profile.svg";
import { ReactComponent as ORDER_HISTORY } from "../../../../assets/siderMenu/orderHistory.svg";
import { ReactComponent as BECOME_INSTRUCTOR } from "../../../../assets/siderMenu/becomeInstructor.svg";
import { ReactComponent as BECOME_MENTOR } from "../../../../assets/siderMenu/becomeMentor.svg";
import { ReactComponent as REVIEW } from "../../../../assets/siderMenu/review.svg";
import { ReactComponent as TRAINING } from "../../../../assets/siderMenu/training.svg";
import { ReactComponent as SCHEDULE } from "../../../../assets/siderMenu/schedule.svg";

import { ReactComponent as WALLET } from "../../../../assets/siderMenu/wallet.svg";
import { ReactComponent as HELP } from "../../../../assets/siderMenu/help.svg";
import { ReactComponent as RESSOURCE } from "../../../../assets/siderMenu/ressource.svg";
import { ReactComponent as CAREER_LAB } from "../../../../assets/siderMenu/careerLab.svg";
import { ReactComponent as BADGES } from "../../../../assets/siderMenu/badges.svg";
import { ReactComponent as MENTORSHIP_LAB } from "../../../../assets/siderMenu/mentorshipLab.svg";
// Style
import "./index.less";
import { useDispatch, useSelector } from "react-redux";
import {
  $upsertUserRole,
  selectSessionUser,
  selectUserRole,
} from "../../../Session/slice";

// Scoped comoponents
const { Sider } = Layout;
const { Item } = Menu;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function SiderMenu() {
  /* *********** CONSTANTS ************ */
  const location = useLocation();
  const [, , page] = location.pathname.split("/");

  const HOVER_COLLAPSE_ENABLED = false;
  const BLUR_COLLAPSE = true;

  /* ************ HOOKS *********** */

  // State
  const [isMenuCollapsed, setIsMenuCollapsed] = useState(
    !HOVER_COLLAPSE_ENABLED
  );
  const [selectedMenuKeys, setSelectedMenuKeys] = useState([]);
  const dispatch = useDispatch();
  // Slice
  const currentRole = useSelector(selectUserRole);
  const currentUser = useSelector(selectSessionUser);
  // Navigation
  const navigate = useNavigate();
  // side effects
  useEffect(() => {
    setSelectedMenuKeys(page);
  }, [page]);
  /**************callbacks functions************* */
  function onConfirmSwAcount(role) {
    dispatch(
      $upsertUserRole({ userId: currentUser._id,role })
    )
      .then((action) => {
        message.success("Role changed !");
        navigate("/dashboard/globalDashboard");
      })
      .catch((error) => {
        message.error("An error occured");
      });


  }
  


  /* *********** RENDERING ************ */
  return (
    <Sider
      className="main-menu"
      trigger={null}
      width={250}
      collapsedWidth={68}
      onCollapse={function cb(collapse) {
        setIsMenuCollapsed(collapse);
      }}
      collapsed={isMenuCollapsed}
      collapsible
      tabIndex={0}
      onBlur={(e) => {
        if (!isMenuCollapsed) {
          if (!e.currentTarget.contains(e.relatedTarget)) {
            setIsMenuCollapsed(BLUR_COLLAPSE);
          }
        }
      }}
    >
      <div
        className="top-collaps-area-slider"
        onClick={function cb() {
          setIsMenuCollapsed(!isMenuCollapsed);
        }}
      >
        <Row justify="center" align="middle" className="chevrons">
          <Col>
            {isMenuCollapsed && <ChevronsRight />}
            {!isMenuCollapsed && <ChevronsLeft />}
          </Col>
        </Row>
      </div>
      <Menu
        className="menu"
        onSelect={function cb(e) {
          if (e.key !== "operations") {
            setSelectedMenuKeys([e.key]);
          }
        }}
        defaultSelectedKeys={["globalDashboard"]}
        selectedKeys={selectedMenuKeys}
        mode="inline"
      >
        <Item
          key="globalDashboard"
          icon={<Icon component={DASHBOARD} />}
          onClick={function cb() {
            navigate("/dashboard/globalDashboard");
          }}
        >
          Dashboard
        </Item>
        <Item
          key="profile"
          icon={<Icon component={PROFILE} />}
          onClick={() => {
            navigate("/dashboard/profile");
          }}
        >
          Profile
        </Item>
        {currentRole === "mentor" && (
          <Item
            key="sevices"
            icon={<Icon component={REVIEW} />}
            onClick={() => {
              navigate("/dashboard/services");
            }}
          >
            Services
          </Item>
        )}
        <Item
          key="careerLab"
          icon={<Icon component={CAREER_LAB} />}
          onClick={() => navigate("/dashboard/careerLab")}
        >
          My Career Lab
        </Item>
        {currentRole === "student" && (
          <Item
            key="mentorshipLab"
            icon={<Icon component={MENTORSHIP_LAB} />}
            onClick={() => navigate("/dashboard/myMentoringLab")}
          >
            My Mentoring Lab
          </Item>
        )}
        <Item key="trainingLab" icon={<Icon component={TRAINING} />}>
          My Training Lab
        </Item>
        <Item
          key="orderHistory"
          icon={<Icon component={ORDER_HISTORY} />}
          onClick={() => navigate("/dashboard/inbox")}
        >
          Inbox
        </Item>
        {/* <Item key="orderHistory" icon={<Icon component={ORDER_HISTORY} />}>
          Order History
        </Item> */}
        {/* <Item key="scheduledServices" icon={<Icon component={SCHEDULE} />}>
          Scheduled Services
        </Item>
        <Item key="reviews" icon={<Icon component={REVIEW} />}>
          My reviews
        </Item> */}
        <Divider></Divider>
        <Item
          key="wallet"
          icon={<Icon component={WALLET} />}
          onClick={() => navigate("/dashboard/myWallet")}
        >
          My Wallet
        </Item>
        <Divider></Divider>
        <Item key="bagdes" icon={<Icon component={BADGES} />}>
          My Badges
        </Item>
        <Item key="ressources" icon={<Icon component={RESSOURCE} />}>
          My Ressources
        </Item>
        {currentRole === "mentor" && (
           <Popconfirm title="Title" onConfirm={() => {onConfirmSwAcount("student")}} onOpenChange={() => console.log('open change')}>
          <Item
            key="becomeStudent"
            icon={<Icon component={BECOME_INSTRUCTOR} />}
          >
            {currentUser.roles.includes("student")
              ? "Switch to Student"
              : "Become Student"}
          </Item>

          </Popconfirm>
        )}
        {currentRole === "student" && (
 <Popconfirm title="Title" onConfirm={() => {onConfirmSwAcount("mentor")}} onOpenChange={() => console.log('open change')}>
          <Item
            key="becomeMentor"
            icon={<Icon component={BECOME_MENTOR} />}
          >
            {currentUser.roles.includes("mentor")
              ? "Switch to Mentor"
              : "Become Mentor"}
          </Item>
          </Popconfirm>

        )}

        <Item key="help" icon={<Icon component={HELP} />}>
          Help
        </Item>
        {
          <Item
            key="mentorCalendar"
            icon={<Icon component={HELP} />}
            onClick={() => navigate("/dashboard/mentorCalendar")}
          >
            Calendar
          </Item>
        }
      </Menu>
      <div
        className="slider-footer"
        onClick={function cb() {
          setIsMenuCollapsed(!isMenuCollapsed);
        }}
      />
    </Sider>
  );
}

export default SiderMenu;
