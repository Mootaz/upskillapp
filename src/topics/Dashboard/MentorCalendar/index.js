/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React, { useEffect, useState } from "react";

import { Calendar, dateFnsLocalizer } from "react-big-calendar";

import { format, parse, startOfWeek, getDay, add } from "date-fns";
// import "react-big-calendar/lib/css/react-big-calendar.css";
import "react-big-calendar/lib/css/react-big-calendar.css";

import "./index.less";
import { baseURL } from "../../../constants";
import { useSelector } from "react-redux";
import { selectSessionUser } from "../../Session/slice";
import axios from "axios";
import { Link } from "react-router-dom";

const locales = {
  "en-US": require("date-fns"),
};
const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function MentorCalendar() {
  /* ---------------------------------- HOOKS --------------------------------- */
  // redux
  const currentUser = useSelector(selectSessionUser);
  const [mentoringRequests, setMentoringRequests] = useState([]);
  console.log("mentoring request", mentoringRequests);
  // side- effects
  useEffect(() => {
    (async function () {
      try {
        await axios({
          method: "get",
          baseURL: `${baseURL}/mentoringRequest/getMentoringRequests`,
          params: { mentorId: currentUser._id, status: "accepted" },
        })
          .then((res) => {
            setMentoringRequests(res.data);
          })
          .catch();
      } catch (e) {
        console.error(e);
      }
    })();
  }, []);
  /* -------------------------------- FUNCTION -------------------------------- */

  function getDate(requestDate, time) {
    const date = new Date(requestDate).toISOString();
    const [dateMember] = date.split("T");
    const resolvedDate = `${dateMember}T${time}:00.000+00:00`;

    return new Date(resolvedDate);
  }

  // get events list
  const events = [];


  mentoringRequests?.forEach((request) => {
    if (request.dates.length > 1) {
      request.dates.forEach((date) => {
        events.push({
          _id: request._id,
          title: request.title,
          resourceId: request._id,
          start: getDate(date, request.time),
          end: add(getDate(date, request.time), { hours: 1 }),
        });
      });
    } else {
      events.push({
        _id: request._id,
        title: request.title,
        resourceId: request._id,
        start: getDate(request.dates[0], request.time),
        end: add(getDate(request.dates[0], request.time), { hours: 1 }),
      });
    }

  });

  console.log("eventrs", events);

  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <>
      <div className="calendars">
        <div>
          <h1>My mentoring calendar</h1>
          <Calendar
            events={events}
            localizer={localizer}
            defaultDate={new Date()}
            style={{ height: 700 }}
            onSelectEvent={() => {
              window.location.href = "http://localhost:4000";
            }}
            
          />
         
        </div>
      </div>
    </>
  );
}
export default MentorCalendar;
