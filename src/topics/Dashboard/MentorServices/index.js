/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React from "react";
import { Layout, Tabs } from "antd";
import { useLocation, useNavigate } from "react-router-dom";
// Local UI components
import Services from "./Services";
import MentorCalendar from "./MentorCalender";
import MentorRequests from "./MentorRequests";

//styles
import "./index.less";

// scoped componenets

const { Content } = Layout;
const { TabPane } = Tabs;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function MentorServices() {
  const { hash } = useLocation();
  const navigate = useNavigate();
  /* -------------------------------- CALLBACKS ------------------------------- */
  function onClickTopicTab(topic) {
    navigate({ hash: topic });
  }
  /* ---------------------------- RENDERIN HELPERS ---------------------------- */

  /* -------------------------------- RENDERING ------------------------------- */

  return (
    <Content className="services-option">
      <Tabs
        key="services-tabs"
        defaultActiveKey={hash ? hash.replace("#", "") : "services"}
        onTabClick={function cb(topic) {
          onClickTopicTab(topic);
        }}
      >
        <TabPane tab="Services" key="services">
          <Services />
        </TabPane>
        <TabPane tab="Requests" key="requests">
          <MentorRequests />
        </TabPane>
        <TabPane tab="Active calendar" key="active-calendar">
          <MentorCalendar />
        </TabPane>
      </Tabs>
    </Content>
  );
}
export default MentorServices;
