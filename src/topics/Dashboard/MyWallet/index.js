/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React from "react";
import { Image, Layout, Row } from "antd";
//styles
import "./index.less";

//assets
import COMING_SOON from "../../../assets/coming-soon.png";

// scoped componenets

const { Content } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function MyWallet() {
  /* ---------------------------- Rendering Helpers ---------------------------- */

  /* -------------------------------- CALLBACKS ------------------------------- */

  /* ---------------------------- RENDERIN HELPERS ---------------------------- */

  return (
    <Content className="my-wallet-content">
      <Row className="wallet-row">
        <Image
          className="coming-soon-image"
          src={COMING_SOON}
          preview={false}
        ></Image>
      </Row>
    </Content>
  );
}
export default MyWallet;
