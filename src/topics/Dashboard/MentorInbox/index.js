/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React, { useEffect, useState } from "react";
import { Col, Layout, Row, Collapse, message, Button } from "antd";

// Constant

//styles
import "./index.less";
import TextArea from "antd/lib/input/TextArea";
import { baseURL } from "../../../constants";
import axios from "axios";
import { useSelector } from "react-redux";
import { selectSessionUser, selectUserRole } from "../../Session/slice";

// scoped componenets

const { Content } = Layout;
const { Panel } = Collapse;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function MentorInbox() {
  /* ---------------------------- Rendering Helpers ---------------------------- */

  const Request = {
    Status: "Rejected",
    Time: "19h00-19h30",
    ServiceName: "Virtual Cofee",
  };

  /* ---------------------------------- HOOKS --------------------------------- */

  // states
  const [mentoringRequests, setMentoringRequests] = useState([]);
  const [message, setMessage] = useState(null);
  const [messagesNumber, setMessagesNmber] = useState(0);
  // selectors
  const currentUser = useSelector(selectSessionUser);
  const currentUserRole = useSelector(selectUserRole);

  // side- effects

  useEffect(() => {
    (async function () {
      if (currentUserRole === "student") {
        await axios({
          method: "get",
          baseURL: `${baseURL}/mentoringRequest/getMentoringRequests`,
          params: { senderId: currentUser._id, status: "accepted" },
        })
          .then((res) => {
            setMentoringRequests(res.data);
          })
          .catch();
      } else if (currentUserRole === "mentor") {
        await axios({
          method: "get",
          baseURL: `${baseURL}/mentoringRequest/getMentoringRequests`,
          params: { mentorId: currentUser._id, status: "accepted" },
        })
          .then((res) => {
            setMentoringRequests(res.data);
          })
          .catch();
      }
    })();
  }, [messagesNumber]);

  /* -------------------------------- CALLBACKS ------------------------------- */
  function HeaderCollapse() {
    const classname = getRequestClassName(Request.Status);
    return (
      <Row style={{ width: "100%" }}>
        <Col span={3}>foulan foulani</Col>
        <Col span={9} offset={2}>
          Web design project [MERN]
        </Col>
        <Col span={3} offset={1}>
          {Request.ServiceName}
        </Col>
        <Col span={2} style={{ right: "-1%" }}>
          10 sep22
        </Col>
        <Col span={4} style={{ right: "-2%" }}>
          <Row align="center">
            <Col>{Request.Time}</Col>&nbsp;
            <Col>
              <span class={classname}></span>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }

  async function onClickSendMessage(request) {
    if (message) {
      await axios({
        method: "post",
        baseURL: `${baseURL}/message/sendMessage`,
        data: {
          message: {
            receiver:
              currentUserRole === "student"
                ? request.mentorId
                : request.senderId,
            sender: currentUser._id,
            referenceId: request._id,
            referenceType: "mentoringRequest",
            text: message,
          },
        },
      })
        .then((res) => {
          setMessage();
          setMessagesNmber(messagesNumber + 1);
        })
        .catch();
    } else {
      message.error("use have to write a message");
    }
  }
  /* ---------------------------- RENDERING HELPERS ---------------------------- */

  function getRequestClassName(status) {
    let classNameStatus;
    switch (status) {
      case "Accepted":
        return (classNameStatus = "dot-accepted");
      case "Pending":
        return (classNameStatus = "dot-pending");
      case "Rejected":
        return (classNameStatus = "dot-rejected");
    }
  }
  function getRequestMessages(request) {
    return request.messages.map((message) => (
      <Row justify={currentUser._id === message.sender ? "end" : "start"}>
        <Col
          span="fit-content"
          offset={4}
          style={{
            backgroundColor:
              currentUser._id === message.sender ? "gray" : "orange",
            padding: "1%",
            borderRadius: "15px",
            marginTop: "1px",
            marginBottom: "1px",
          }}
        >
          {message.text}
        </Col>
      </Row>
    ));
  }
  function getRequestDiscussion(request) {
    return (
      <>
        <Row className="collapse-wrapper-user-dash">
          <Collapse bordered={false}>
            <Panel header={HeaderCollapse()}>
              <Row>
                <Col span={20}>
                  <Row>
                    <Col
                      span="fit-content"
                      offset={4}
                      style={{
                        backgroundColor: "gray",
                        padding: "1%",
                        borderRadius: "15px",
                      }}
                    >
                      {request.subject}
                    </Col>
                  </Row>
                  {getRequestMessages(request)}

                  <Row>
                    <Col span={20} offset={4}>
                      <Row>
                        <TextArea
                          value={message}
                          onChange={(e) => {
                            setMessage(e.target.value);
                          }}
                          placeholder="Type message here"
                        ></TextArea>
                      </Row>
                    </Col>
                  </Row>

                  <Row justify="end">
                    <Col span={12} offset={4}>
                      <Row>
                        <Button
                          className="accept-button"
                          type="primary"
                          onClick={function cb() {
                            onClickSendMessage(request);
                          }}
                        >
                          Send message
                        </Button>
                        &nbsp; &nbsp;
                        <Button className="reject-button" type="primary">
                          Mark as important
                        </Button>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Panel>
          </Collapse>
        </Row>
      </>
    );
  }

  /* ------------------------------- RENDERING  ------------------------------- */

  return (
    <Content className="mentor-inbox">
      <Row>
        <Col span={4} offset={1}>
          <h1 className="title">Inbox</h1>
        </Col>
      </Row>
      <Row className="header">
        <Col span={4} offset={2}>
          User
        </Col>
        <Col span={9}>Subject</Col>
        <Col span={2}>Service</Col>
        <Col span={3} offset={1}>
          Date
        </Col>
        <Col span={2}>Time</Col>
      </Row>
      {mentoringRequests.map((request) => getRequestDiscussion(request))}
    </Content>
  );
}
export default MentorInbox;
