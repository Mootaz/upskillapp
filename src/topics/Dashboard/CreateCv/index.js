/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
// UI lib components
import {
  Layout,
  Form,
  Input,
  Typography,
  Select,
  Row,
  Col,
  Button,
  Space,
  Switch,
  Collapse,
  message,
  InputNumber,
} from "antd";

// UI local components
import DatePicker from "../../Shared/components/inputs/DatePicker";

import CountrySelect from "../../Shared/components/inputs/countrySelect";

// constants
import { mentions } from "../../Shared/Entities/mentions";
import { skills } from "../../Shared/Entities/skills";
// Style
import "./index.less";
import "react-phone-number-input/style.css";
import {
  $insertUserCV,
  selectSessionUser,
  selectUserCV,
} from "../../Session/slice";

import { Trash } from "tabler-icons-react";

// Scoped components
const { Content } = Layout;

const { Title } = Typography;
const { TextArea } = Input;
const { Panel } = Collapse;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function CreateCv() {
  /* ********************************** HOOKS ********************************* */
  const [personalInfoForm] = Form.useForm();
  const [educationForm] = Form.useForm();
  const [skillsForm] = Form.useForm();
  const [experinceForm] = Form.useForm();
  const dispatch = useDispatch();
  const currentUser = useSelector(selectSessionUser);
  const currentUserCV = useSelector(selectUserCV);

  /* -------------------------------- CALLBACKS ------------------------------- */
  function onSaveUserData() {
    const userData = {
      userPersonalInfo: personalInfoForm.getFieldsValue(),
      userCVInfo: {
        ...experinceForm.getFieldsValue(),
        ...educationForm.getFieldsValue(),
        ...skillsForm.getFieldsValue(),
      },
    };

    dispatch($insertUserCV({ _id: currentUser._id, ...userData }))
      .then((action) => {
        message.success("Changes saved !");
      })
      .catch((error) => {
        message.error("An error occured");
      });
  }
  /* ***************************** RENDER HELPERS ***************************** */

  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };
  function getEducationForm() {
    return (
      <Form
        form={educationForm}
        initialValues={{ ...currentUser, ...currentUserCV }}
        {...formItemLayout}
      >
        <b>Diplomas</b>

        <Form.List name="diplomas">
          {(fields, { add, remove }) => (
            <>
              {fields.map((field, index) => (
                <div className="collapse-wrapper">
                  <Collapse>
                    <Panel
                      key={index}
                      header={<span>{`Diploma ${index + 1}`}</span>}
                    >
                      <Space
                        key={field.key}
                        style={{ display: "flex", marginBottom: 8 }}
                        align="baseline"
                      >
                        <Row>
                          <Col span={7}>
                            <Form.Item
                              {...field}
                              name={[field.name, "name"]}
                              label={"name"}
                              fieldKey={[field.fieldKey, "name"]}
                            >
                              <Input />
                            </Form.Item>
                          </Col>
                          <Col span={7} offset={1}>
                            <Form.Item
                              {...field}
                              name={[field.name, "location"]}
                              label={"location"}
                              fieldKey={[field.fieldKey, "location"]}
                            >
                              <CountrySelect />
                            </Form.Item>
                          </Col>
                          <Col span={7} offset={1}>
                            <Form.Item
                              {...field}
                              name={[field.name, "school"]}
                              label={"school"}
                              fieldKey={[field.fieldKey, "school"]}
                            >
                              <Input />
                            </Form.Item>
                          </Col>

                          <Col span={7}>
                            <Form.Item
                              {...field}
                              name={[field.name, "date"]}
                              label={"Date"}
                              fieldKey={[field.fieldKey, "date"]}
                            >
                              <DatePicker />
                            </Form.Item>
                          </Col>
                          <Col span={7} offset={1}>
                            <Form.Item
                              {...field}
                              label={"Mention"}
                              name={[field.name, "mention"]}
                              fieldKey={[field.fieldKey, "mention"]}
                            >
                              <Select
                                options={mentions.map((mention) => mention)}
                              />
                            </Form.Item>
                          </Col>
                        </Row>

                        <Trash onClick={() => remove(field.name)} />
                      </Space>{" "}
                    </Panel>
                  </Collapse>
                </div>
              ))}
              <Form.Item>
                <Row justify="end">
                  <Button
                    className="add-button"
                    type="primary"
                    onClick={() => add()}
                    icon={<PlusOutlined />}
                  >
                    Add diploma
                  </Button>
                </Row>
              </Form.Item>
            </>
          )}
        </Form.List>
      </Form>
    );
  }

  function getSkillsForm() {
    return (
      <Form
        form={skillsForm}
        initialValues={{ ...currentUser, ...currentUserCV }}
        {...formItemLayout}
      >
        <Form.Item label="Languages" name="languages">
          <Select
            mode="multiple"
            allowClear
            options={[
              { label: "English", value: "english" },
              { label: "French", value: "french" },
              { label: "Arabic", value: "arabic" },
            ]}
          />
        </Form.Item>
        <Form.Item label="Skills" name="skills">
          <Select
            mode="multiple"
            allowClear
            options={skills.map((skill) => skill)}
          />
        </Form.Item>
      </Form>
    );
  }

  function getExperienceForm() {
    return (
      <>
        <Form
          form={experinceForm}
          initialValues={{ ...currentUser, ...currentUserCV }}
          {...formItemLayout}
        >
          <b>Experience</b>

          <Form.List name="work">
            {(fields, { add, remove }) => (
              <>
                {fields.map((field, index) => (
                  <div className="collapse-wrapper">
                    <Collapse>
                      <Panel
                        key={index}
                        header={<span>{`Experience ${index + 1}`}</span>}
                      >
                        <Space
                          key={field.key}
                          style={{ display: "flex", marginBottom: 8 }}
                          align="baseline"
                        >
                          <Row>
                            <Col span={9}>
                              <Form.Item
                                {...field}
                                name={[field.name, "title"]}
                                label={"Title"}
                                fieldKey={[field.fieldKey, "title"]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col span={9} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "company"]}
                                label={"company"}
                                fieldKey={[field.fieldKey, "company"]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col span={4} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "isCurrentWork"]}
                                label={"current work"}
                                fieldKey={[field.fieldKey, "isCurrentWork"]}
                                valuePropName="checked"
                              >
                                <Switch />
                              </Form.Item>
                            </Col>
                            <Col span={9}>
                              <Form.Item
                                {...field}
                                name={[field.name, "location"]}
                                label={"location"}
                                fieldKey={[field.fieldKey, "location"]}
                              >
                                <CountrySelect />
                              </Form.Item>
                            </Col>

                            <Col span={5} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "startDate"]}
                                label={"Start date"}
                                fieldKey={[field.fieldKey, "startDate"]}
                              >
                                <DatePicker />
                              </Form.Item>
                            </Col>
                            <Col span={5} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "endDate"]}
                                label={"End date"}
                                fieldKey={[field.fieldKey, "endDate"]}
                              >
                                <DatePicker />
                              </Form.Item>
                            </Col>
                            <Col span={20}>
                              <Form.Item
                                {...field}
                                label={"Main tasks"}
                                name={[field.name, "mainTasks"]}
                                fieldKey={[field.fieldKey, "mainTasks"]}
                              >
                                <TextArea
                                  placeholder="maxLength is 200"
                                  showCount
                                  maxLength={200}
                                />
                              </Form.Item>
                            </Col>
                          </Row>

                          <Trash onClick={() => remove(field.name)} />
                        </Space>
                      </Panel>
                    </Collapse>
                  </div>
                ))}
                <Form.Item>
                  <Row justify="end">
                    <Button
                      className="add-button"
                      type="primary"
                      onClick={() => add()}
                      icon={<PlusOutlined />}
                    >
                      Add experience
                    </Button>
                  </Row>
                </Form.Item>
              </>
            )}
          </Form.List>

          <b>Internships</b>

          <Form.List name="internships">
            {(fields, { add, remove }) => (
              <>
                {fields.map((field, index) => (
                  <div className="collapse-wrapper">
                    <Collapse>
                      <Panel
                        key={index}
                        header={<span>{`Internship ${index + 1}`}</span>}
                      >
                        <Space
                          key={field.key}
                          style={{ display: "flex", marginBottom: 8 }}
                          align="baseline"
                        >
                          <Row>
                            <Col span={9}>
                              <Form.Item
                                {...field}
                                name={[field.name, "title"]}
                                label={"Title"}
                                fieldKey={[field.fieldKey, "title"]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col span={9} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "company"]}
                                label={"company"}
                                fieldKey={[field.fieldKey, "company"]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col span={9}>
                              <Form.Item
                                {...field}
                                name={[field.name, "location"]}
                                label={"location"}
                                fieldKey={[field.fieldKey, "location"]}
                              >
                                <CountrySelect />
                              </Form.Item>
                            </Col>
                            <Col span={4} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "startDate"]}
                                label={"Start date"}
                                fieldKey={[field.fieldKey, "startDate"]}
                              >
                                <DatePicker />
                              </Form.Item>
                            </Col>
                            <Col span={4} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "endDate"]}
                                label={"End date"}
                                fieldKey={[field.fieldKey, "endDate"]}
                              >
                                <DatePicker />
                              </Form.Item>
                            </Col>
                            <Col span={20}>
                              <Form.Item
                                {...field}
                                label={"Main tasks"}
                                name={[field.name, "mainTasks"]}
                                fieldKey={[field.fieldKey, "mainTasks"]}
                              >
                                <TextArea
                                  placeholder="maxLength is 55"
                                  showCount
                                  maxLength={55}
                                />
                              </Form.Item>
                            </Col>
                          </Row>

                          <Trash onClick={() => remove(field.name)} />
                        </Space>{" "}
                      </Panel>
                    </Collapse>
                  </div>
                ))}
                <Form.Item>
                  <Row justify="end">
                    <Button
                      className="add-button"
                      type="primary"
                      onClick={() => add()}
                      icon={<PlusOutlined />}
                    >
                      Add internship
                    </Button>
                  </Row>
                </Form.Item>
              </>
            )}
          </Form.List>
          <b>Trainings</b>

          <Form.List name="trainings">
            {(fields, { add, remove }) => (
              <>
                {fields.map((field, index) => (
                  <div className="collapse-wrapper">
                    <Collapse>
                      <Panel
                        key={index}
                        header={<span>{`Training ${index + 1}`}</span>}
                      >
                        <Space
                          key={field.key}
                          style={{ display: "flex", marginBottom: 8 }}
                          align="baseline"
                        >
                          <Row>
                            <Col span={9}>
                              <Form.Item
                                {...field}
                                name={[field.name, "name"]}
                                label={"name"}
                                fieldKey={[field.fieldKey, "name"]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col span={9} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "company"]}
                                label={"company"}
                                fieldKey={[field.fieldKey, "company"]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col span={4} offset={1}>
                              <Form.Item
                                {...field}
                                name={[field.name, "periode"]}
                                label={"periode"}
                                fieldKey={[field.fieldKey, "periode"]}
                              >
                              <InputNumber />
                              </Form.Item>
                            </Col>
                            <Col span={20}>
                              <Form.Item
                                {...field}
                                label={"Link"}
                                name={[field.name, "Link"]}
                                fieldKey={[field.fieldKey, "Link"]}
                              >
                                <TextArea
                                  placeholder="maxLength is 30"
                                  showCount
                                  maxLength={30}
                                />
                              </Form.Item>
                            </Col>
                          </Row>

                          <Trash onClick={() => remove(field.name)} />
                        </Space>{" "}
                      </Panel>
                    </Collapse>
                  </div>
                ))}
                <Form.Item>
                  <Row justify="end">
                    <Button
                      className="add-button"
                      type="primary"
                      onClick={() => add()}
                      icon={<PlusOutlined />}
                    >
                      Add Training
                    </Button>
                  </Row>
                </Form.Item>
              </>
            )}
          </Form.List>
        </Form>
      </>
    );
  }

  function getContent() {
    return (
      <Content className="form-wrapper">
        <Row justify="end">
          <Col>
            <Button type="primary" className="upload-cv-button">
              Upload my CV
            </Button>
          </Col>
          <Col offset={1}>
            <Button
              type="primary"
              className="upload-cv-button"
              onClick={() => {
                onSaveUserData();
              }}
            >
              Save
            </Button>
          </Col>
        </Row>
        <div>
          <Title className="title" level={3}>
            Resume
          </Title>
        </div>
        {getExperienceForm()}
        {getEducationForm()}
        {getSkillsForm()}
        <Row justify="end">
          <Button
            type="primary"
            className="upload-cv-button"
            onClick={() => {
              onSaveUserData();
            }}
          >
            Save
          </Button>
        </Row>
      </Content>
    );
  }
  /* ******************************** RENDERING ******************************* */
  return (
    <Content className="career-lab-layout">
      <Content className="career-lab-content">
        <Content className="basic-info">{getContent()}</Content>
      </Content>
    </Content>
  );
}

export default CreateCv;
