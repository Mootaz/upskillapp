/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React from "react";
import { Col, Image, Layout, Rate, Row } from "antd";
import Graph from "./graphics";
//styles
import "./index.less";

//assets
import COMING_SOON from "../../../assets/coming-soon.png";
import { useSelector } from "react-redux";
import { selectUserRole } from "../../Session/slice";

// scoped componenets

const { Content } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function GlobalDashboard() {
  /* ---------------------------- HOOKS ---------------------------- */
  const currentRole = useSelector(selectUserRole);
  /* -------------------------------- CALLBACKS ------------------------------- */

  /* ---------------------------- RENDERIN HELPERS ---------------------------- */
  function getUserDashboard() {
    return (
      <Content>
        <Row className="row-wrapper-1">
          <span className="title">Activity</span>
          <div className="graph-row">
            <Graph color="orange" />
          </div>
        </Row>
        <Row className="row-wrapper-2">
          <span className="title">Career Path</span>
          <div className="graph-row">
            <Image
              className="coming-soon-image"
              src={COMING_SOON}
              preview={false}
            ></Image>
          </div>
        </Row>
      </Content>
    );
  }

  function getMentorDashboard() {
    return (
      <Content>
        <Row className="row-wrapper-1">
          <span className="title">Revenue</span>
          <div className="graph-row">
            <Graph color="green" />
          </div>
        </Row>

        <Row className="row-wrapper-1 margin">
          <span className="title">Mentoring Activity</span>
          <div className="graph-row">
            <Graph color="orange" />
          </div>
        </Row>
        <Row className="row-wrapper-1 margin">
          <span className="title">Top sellers</span>
          <div className="graph-row">
            <Graph color="violet" />
          </div>
        </Row>

        <Row className="row-wrapper-1 margin">
          <span className="title">Recent reviews</span>
          <div className="graph-row">
            <Row>
              {[1, 2, 3].map((t) => (
                <Col className="mentor-Review-Box" align="middle">
                  <Rate
                    className="mentor-Review-Box-stars"
                    allowHalf
                    defaultValue={3}
                  />
                  <p className="mentor-Review-Box-title">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.{" "}
                  </p>
                  <p className="mentor-Review-Box-Nom">Foulan FOULANI </p>
                </Col>
              ))}
            </Row>
          </div>
        </Row>
      </Content>
    );
  }
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="user-dashboard-content">
      {currentRole === "mentor" ? getMentorDashboard() : getUserDashboard()}
    </Content>
  );
}
export default GlobalDashboard;
