/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React, { useEffect, useState } from "react";
import { Row, Col, Collapse, Button, Layout } from "antd";
import { format } from "date-fns";
//styles
import "./index.less";
import axios from "axios";
import { baseURL } from "../../../../constants";
import { useSelector } from "react-redux";
import { selectSessionUser } from "../../../Session/slice";

// scoped componenets
const { Content } = Layout;
const { Panel } = Collapse;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function RequestsHistory() {
  /* --------------------------------- HOOKS --------------------------------- */
  // redux
  const currentUser = useSelector(selectSessionUser);

  // states
  const [mentorRequests, setMentorRequests] = useState([]);

  // side- effects
  useEffect(() => {
    (async function () {
      try {
        await axios({
          method: "get",
          baseURL: `${baseURL}/mentoringRequest/getMentoringRequests`,
          params: { senderId: currentUser._id },
        })
          .then((res) => {
            setMentorRequests(res.data);
          })
          .catch();
      } catch (e) {
        console.error(e);
      }
    })();
  }, []);
  /* -------------------------------- CALLBACKS ------------------------------- */

  /* ---------------------------- RENDERING HELPERS --------------------------- */

  function getRequestClassName(status) {
    switch (status) {
      case "accepted":
        return "dot-accepted";
      case "pending":
        return "dot-pending";
      case "rejected":
        return "dot-rejected";

      default:
        break;
    }
  }

  function HeaderCollapse(request) {
    const classname = getRequestClassName(request.tatus);
    return (
      <Row style={{ width: "100%" }}>
        <Col span={4}>{`${
          request.mentor?.firstName ? request.mentor?.firstName : "-"
        } ${request.mentor?.lastName ? request.mentor?.lastName : ""}`}</Col>
        <Col span={12} offset={1}>
          {request.title}
        </Col>
        <Col span={3} offset={1}>
          {request.dates.length
            ? format(new Date(request.dates[0]), "dd/MM/yyy")
            : "-"}
        </Col>
        <Col span={3} style={{ right: "-2%" }}>
          <Row align="center">
            <Col>{request.status}</Col>&nbsp;
            <Col>
              <span class={classname}></span>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }

  function getRequestRow(request) {
    return (
      <Row className="collapse-wrapper-user-dashboard">
        <Collapse bordered={false}>
          <Panel header={HeaderCollapse(request)}>
            <div>
              <Row>
                <Col span={12} offset={6}>
                  {request.subject}
                </Col>
              </Row>
              <br></br>
              <Row justify="end">
                <Col style={{ width: "35%" }}>
                  <Row>
                   <Button className="send-reminder-button"  >
                      Send reminder
                    </Button>
                  </Row>
                </Col>
              </Row>
            </div>
            <br></br>
            <div></div>
          </Panel>
        </Collapse>
      </Row>
    );
  }
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content>
      <Row>
        <Col span={4} offset={2}>
          Mentor Name
        </Col>
        <Col span={11}>Subject</Col>
        <Col span={3} offset={1}>
          Date
        </Col>
        <Col span={2}>Status</Col>
      </Row>
      {mentorRequests?.map((request) => getRequestRow(request))}
    </Content>
  );
}

export default RequestsHistory;
