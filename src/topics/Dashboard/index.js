/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React, { useEffect, useState } from "react";
import { Layout, Row, Col, Rate, Switch, message } from "antd";
import Avatar from "antd/lib/avatar/avatar";
import {
  $insertUserCV,
  selectSessionUser,
  selectUserRole,
} from "../Session/slice";
import { useDispatch, useSelector } from "react-redux";
//styles
import "./index.less";

import TopBar from "../layouts/HomePage/topBar";
import { UserOutlined } from "@ant-design/icons";
import DashbaodPage from "./DashboardPage";

// scoped componenets

const { Content } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function Dashbaod() {
  const currentUser = useSelector(selectSessionUser);
  const currentRole = useSelector(selectUserRole);
  const dispatch = useDispatch();
  const [checked, setChecked] = useState(currentUser.mainRole === currentRole);
  /* ---------------------------- Rendering Helpers ---------------------------- */

  /* -------------------------------- CALLBACKS ------------------------------- */

  useEffect(() => {
    setChecked(currentUser.mainRole === currentRole);
  }, [currentRole]);

  function onChangeUserMainRole() {
    setChecked(true);
    dispatch(
      $insertUserCV({
        _id: currentUser._id,
        userPersonalInfo: { mainRole: currentRole },
      })
    )
      .then((action) => {
        message.success("Main account changed !");
      })
      .catch((error) => {
        message.error("An error occured");
      });
  }
  /*--------------------- RENDERIN HELPERS ---------------------------- */

  return (
    <Content>
      <TopBar />
      <div className="content-wrapper">
        <div className="user-dashboard">
          <Row className="user-details-row" align="middle">
            <Col span={2}>
              <Avatar
                size={{
                  xs: 24,
                  sm: 32,
                  md: 40,
                  lg: 64,
                  xl: 80,
                  xxl: 100,
                }}
                icon={<UserOutlined />}
              >
                t
              </Avatar>
            </Col>
            <Col span={5}>
              <Row>{`Hello , ${
                currentUser?.firstName ? currentUser.firstName : "-"
              } ${currentUser?.lastName ? currentUser.lastName : "-"}`}</Row>
              <Row>{currentRole === "student" ? "User" : "Mentor"}</Row>
              <Row>
                <Rate value={3} />
              </Row>
              <Row className="main-account-row">
                <Col>Main account</Col>
                <Col offset={1}>
                  <Switch
                    disabled={currentUser.mainRole === currentRole}
                    checked={checked}
                    onChange={(e) => {
                      onChangeUserMainRole(e);
                    }}
                  />
                </Col>
              </Row>
            </Col>
            <Col offset={12} span={3}>
              Silver User
            </Col>
            <Col span={2}>
              <Avatar
                size={{
                  xs: 24,
                  sm: 32,
                  md: 40,
                  lg: 64,
                  xl: 80,
                  xxl: 100,
                }}
                icon={<UserOutlined />}
              >
                t
              </Avatar>
            </Col>
          </Row>

          <DashbaodPage />
        </div>
      </div>
    </Content>
  );
}
export default Dashbaod;
