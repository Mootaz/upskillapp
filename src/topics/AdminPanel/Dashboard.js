/* -------------------------------------------------------------------------- */
/*                                 DEPENDECIES                                */
/* -------------------------------------------------------------------------- */

// packages
import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import { Layout, Menu, Card, Carousel } from 'antd';
import './Dashboard.css'; // Import the CSS file
import userstatistics from "../../assets/pexels-photo-6483626.webp"
import coursetatistics from "../../assets/pexels-photo-185576.webp"
import paymentstatistics from "../../assets/pexels-photo-259200.webp"



import { LineChart, Line, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, Cell } from 'recharts';



// Constants
import { baseURL } from "../../constants";
import {
  UserOutlined,
  DashboardOutlined,
  FileOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";

const { Header, Content, Sider } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function Dashboard() {
  /* ---------------------------------- HOOKS --------------------------------- */

  const [statistics, setStatistics] = useState(null);
  const [chartData, setChartData] = useState(null);
  const [chartDatacourse, setChartDatacourse] = useState(null);
  const [paymentData, setPaymentData] = useState([]);
  const [lineChartData, setLineChartData] = useState([]);








  // side-effects
  useEffect(() => {
    const fetchUserData = async () => {
      try {

        const statisticsResponse = await axios.get(`${baseURL}/user/getAllUserstatistics`);
        const statisticscourseResponse = await axios.get(`${baseURL}/courses/getAllcoursestatistics`);
        setStatistics(statisticscourseResponse.data.statistics);
        setStatistics(statisticsResponse.data.statistics);

        // Prepare chart user data
        if (statisticsResponse.data && statisticsResponse.data.statistics) {
          const statisticsData = statisticsResponse.data.statistics;
          const chartData = Object.entries(statisticsData).map(([label, value]) => ({
            label,
            value,
          }));
          setChartData(chartData);
        }

        // Prepare chart course data
        if (statisticscourseResponse.data && statisticscourseResponse.data.statistics) {
          const statisticsData = statisticscourseResponse.data.statistics;
          const chartDatacourse = Object.entries(statisticsData).map(([label, value]) => ({
            label,
            value,
          }));
          setChartDatacourse(chartDatacourse);
        }

        // Prepare line chart data
        if (statisticsResponse.data && statisticsResponse.data.statistics) {
          const statisticsData = statisticsResponse.data.statistics;
          const lineChartData = Object.entries(statisticsData).map(([label, value]) => ({
            label,
            value, // Use the relevant data for the line chart, e.g., average scores, completion rates, etc.
          }));
          setLineChartData(lineChartData);
        }



      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };

    fetchUserData();
    fetchPaymentStatistics();
  }, []);

  const userChartsRef = useRef(null);
  const courseChartsRef = useRef(null);
  const PaymentChartsRef = useRef(null);
  const fetchPaymentStatistics = async () => {
    try {
      const response = await axios.get('http://localhost:4000/payment/statistics'); // Replace this with your API endpoint
      // Reformat data to match recharts' format and rename keys
      const formattedData = response.data.map((data) => ({
        status: data._id, // Assuming '_id' is the key for status
        totalAmount: data.totalAmount,
        count: data.count,
      }));
      setPaymentData(formattedData);
    } catch (error) {
      console.error('Error fetching payment statistics:', error);
    }
  };
  const chartDatacoursee = [
    { label: 'A', value: 10, color: 'teal' },
    { label: 'B', value: 20, color: 'blue' },
    { label: 'C', value: 15, color: 'green' },
    { label: 'C', value: 15, color: 'purple' },
    { label: 'C', value: 15, color: 'orange' },
    // Add more data points with labels, values, and colors
  ];


  const handleViewUserCharts = () => {
    userChartsRef.current.scrollIntoView({ behavior: "smooth" });
  };
  const handleViewCourseCharts = () => {
   courseChartsRef.current.scrollIntoView({ behavior: "smooth" });
  };
  const handleViewPaymentCharts = () => {
   PaymentChartsRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];



  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Layout>
      <Header className="header" style={{ background: 'teal' }}>
        <div className="logo" />
        <span style={{ color: 'white', marginRight: '20px', fontSize: '30px', fontWeight: 'bold' }}>Admin Dashboard</span>
      </Header>
      <Layout>
        <Sider width={200} className="site-layout-background" style={{ background: 'teal' }}>
          <Menu mode="inline" defaultSelectedKeys={["1"]} style={{ background: 'teal' }} >
            <Menu.Item key="1" icon={<DashboardOutlined />} style={{ color: 'black', marginTop: "40px" }}>
              <Link to='/Dashboard'>  </Link>
              Dashboard
            </Menu.Item>
            <Menu.Item key="2" icon={<UserOutlined />} style={{ color: 'black', marginTop: "40px" }}>
              UsersList
              <Link to='/userlistdata'>  </Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<FileOutlined />} style={{ color: 'black', marginTop: "40px" }}>
              <Link to='/userlistdata'>  </Link>
              CourseList
            </Menu.Item>
          </Menu>
        </Sider>

        <Layout style={{ padding: "0 24px 24px" }}>
          <Content
            className="site-layout-background"
            style={{
              padding: 90,
              margin: 90,
              minHeight: 280,
            }}
          >
            <h1 className="text-Teal text-6xl text-center font-bold mb-10">Dashboard statistics</h1>
            <p className="text-black text-xl text-center ">
              This dashboard serves as a central hub for tracking and managing various statistical data, empowering administrators to monitor user progress, course enrollments, and financial transactions related to statistics education efficiently</p>


            <div class="container mb-40">
              <div class="card user-statistics">
                <img src={userstatistics} alt="User Statistics" />
                <h2 class="title">User Statistic</h2>
                <p >Here, you can find various statistics related to users, such as the number of registered users, active users, etc.</p>
                <button onClick={handleViewUserCharts}>View User charts</button>
              </div>
              <div class="card course-statistics">
                <img src={coursetatistics} alt="Course Statistics" />
                <h2 class="title">Course Statistic</h2>
                <p >This card presents statistics related to courses, including the number of courses available, enrollment data, etc.</p>
                <button onClick={handleViewCourseCharts}>View Course charts</button>
              </div>
              <div class="card payment-statistics">
                <img src={paymentstatistics} alt="Payment Statistics" />
                <h2 class="title">Payment Statistic</h2>
                <p >Here, you can view payment statistics, such as total revenue, successful payments, pending payments, etc.</p>
                <button onClick={handleViewPaymentCharts}>View Payment charts</button>
              </div>
            </div>
            <div ref={userChartsRef}>
              <div className="mb-10 text-center text-4xl mr-20">
                <h1>User Statistics</h1>
              </div>
              {chartData && (
                <div style={{ display: "flex", marginBottom: "40px" }}>

                  <div style={{ flex: 1, marginRight: "20px" }}>
                    <BarChart width={600} height={300} data={chartData}>
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis dataKey="label" />
                      <YAxis />
                      <Tooltip />
                      <Legend />
                      <Bar dataKey="value" fill="#8884d8">
                        {
                          chartDatacoursee.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={entry.color} />
                          ))
                        }
                      </Bar>
                    </BarChart>
                  </div>

                  <div style={{ display: "flex", marginTop: "5px" }}>
                    <div className="pie-chart-container">

                      <PieChart width={400} height={300}>
                        <Pie
                          dataKey="value"
                          isAnimationActive={true} // Enable animation
                          data={chartData}
                          cx="50%"
                          cy="50%"
                          outerRadius={80}
                          label={({ name }) => name} // Display course names as labels
                        >
                          {chartDatacoursee.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                          ))}
                        </Pie>
                        <Tooltip />
                      </PieChart>
                    </div>
                  </div>

                </div>
              )}
            </div>

            <div ref={courseChartsRef}>
            <div className="mb-10 text-center text-4xl mr-20">
              <h1>Course Statistics</h1>
            </div>
            {chartDatacourse && (
              <div style={{ display: "flex" }}>
                <div style={{ flex: 1 }}>
                  <BarChart width={600} height={300} data={chartDatacourse}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="label" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="value" fill="#8884d8">
                      {
                        chartDatacoursee.map((entry, index) => (
                          <Cell key={`cell-${index}`} fill={entry.color} />
                        ))
                      }
                    </Bar>
                  </BarChart>
                </div>
                <div style={{ flex: 1 }}>
                  <div className="pie-chart-container">

                    <PieChart width={400} height={300}>
                      <Pie
                        dataKey="value"
                        isAnimationActive={true} // Enable animation
                        data={chartDatacourse}
                        cx="50%"
                        cy="50%"
                        outerRadius={80}
                        label={({ name }) => name} // Display course names as labels
                      >
                        {chartDatacourse.map((entry, index) => (
                          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                      </Pie>
                      <Tooltip />
                    </PieChart>
                  </div>
                </div>
              </div>
            )}
            </div>
            <div className="mb-10 mt-10 text-center text-4xl mr-20">
              <h1>Payment Statistics</h1>
            </div>
             <div ref={PaymentChartsRef}>
            <div style={{ display: 'flex' }}>

              <BarChart width={600} height={400} data={paymentData}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="status" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="totalAmount" fill="teal" name="Total Amount" />
                <Bar dataKey="count" fill="teal" name="Count" />
              </BarChart>


              <div className="pie-chart-container"  >
                <PieChart width={400} height={300}>
                  <Pie
                    data={paymentData}
                    dataKey="totalAmount"
                    nameKey="status"
                    cx="50%"
                    cy="50%"
                    outerRadius={80}
                    fill="rgba(75,192,192,0.2)"
                    label
                  >
                    {paymentData.map((entry, index) => (
                      <Cell key={`cell-${index}`} fill={`rgba(75,192,192,${(index + 1) / paymentData.length})`} />
                    ))}
                  </Pie>
                  <Tooltip />
                  <Legend />
                </PieChart>
              </div>
            </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
}

export default Dashboard;
