/* -------------------------------------------------------------------------- */
/*                                 DEPENDECIES                                */
/* -------------------------------------------------------------------------- */

// packages
import React, { useEffect, useState } from "react";
import axios from "axios";
import { Layout, Menu } from "antd";
// Lib dependencies
import { Button, Input, message, Table, Popconfirm } from "antd";
import { DeleteOutlined } from '@ant-design/icons';

import { Link } from "react-router-dom";

// Constants
import { baseURL } from "../../../constants";
import { ExportToExcel } from "../../Shared/helpers/ExcelExport";
import {
  UserOutlined,
  DashboardOutlined,
  FileOutlined,
} from "@ant-design/icons";
//styles
import './Uselistdata.css';



const { Header, Content, Sider } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function UsersList() {
  /* ---------------------------------- HOOKS --------------------------------- */
  const [userList, setUserList] = useState([])
  const [edit, setEdit] = useState()
  const [statistics, setStatistics] = useState(null)
  const [chartData, setChartData] = useState(null)




  // side-effects
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const userListResponse = await axios.get(`${baseURL}/user/getUserList`);
        const statisticsResponse = await axios.get(`${baseURL}/user/getAllUserstatistics`);

        setUserList(userListResponse.data);
        setStatistics(statisticsResponse.data.statistics);

        // Prepare chart data
        // Prepare chart data
        if (statisticsResponse.data && statisticsResponse.data.statistics) {
          const statisticsData = statisticsResponse.data.statistics;
          const chartData = Object.entries(statisticsData).map(([label, value]) => ({
            label,
            value,
          }));
          setChartData(chartData);
        }

      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };

    fetchUserData();
  }, []);

  /* -------------------------------- callbacks ------------------------------- */
  async function onBanUser(userObj) {
    await axios({
      method: "post",
      baseURL: `${baseURL}/user/updateUser`,
      data: { userId: userObj._id, updatedUser: { enabled: !userObj.enabled } },
    })
      .then((res) => {
        const userUpdated = res.data;
        const userListUpdate = userList.map((user) => {
          if (user._id === userUpdated._id) {
            return userUpdated;
          }
          return user;
        });
        setUserList(userListUpdate);

        if (userUpdated.enabled) {
          message.success(`${userUpdated.firstName} Has been activated`);
        } else {
          message.success(`${userUpdated.firstName} Has been blocked`);
        }
      })
      .catch();
  }

  async function onDeleteUser(userObj) {
    try {
      // Make the API call to delete the user
      await axios.delete(`${baseURL}/user/deleteadminuser/${userObj._id}`);

      // Update the user list in the state by filtering out the deleted user
      const updatedUserList = userList.filter((user) => user._id !== userObj._id);
      setUserList(updatedUserList);

      message.success(`${userObj.firstName} has been deleted.`);
    } catch (error) {
      // Handle error
    }
  }

  /* ----------------------------- LOCAL VARIABLES ---------------------------- */
  const columnsDefNames = ["firstName", "lastName", "email", "mainRole", "Ban", "Delete"];
  const columns = columnsDefNames.map((col) => {
    const columnSchema = {};
    columnSchema.title = col;
    columnSchema.dataIndex = col;
    columnSchema.key = col;
    columnSchema.render = (a, record) => {
      switch (col) {
        case "firstName":
          return edit?.col === "firstName" && edit?.index === record.key ? (
            <Input placeholder="firstName" />
          ) : (
            <div
              onClick={() => {
                setEdit({ col: "firstName", index: record.key });
              }}
            >
              {record?.firstName}
            </div>
          );
        case "Ban":
          return (
            <div>
              {" "}
              <Button onClick={() => onBanUser(record)}>
                {record.enabled === true ? "Ban" : "Activate"}
              </Button>
            </div>
          );
        case "Delete":
          return (
            <Popconfirm
              title="Are you sure you want to delete this user?"
              onConfirm={() => onDeleteUser(record)}
              okText="Yes"
              cancelText="No"
            >
              <Button icon={<DeleteOutlined />} danger />
            </Popconfirm>
          );
        default:
          return (record[col]);
      }
    };
    return columnSchema;
  });

  const test = userList.map((user, index) => ({
    key: index,
    _id: user._id,
    firstName: user.firstName,
    lastName: user.lastName,
    mainRole: user.mainRole,
    email: user.email,
    enabled: user.enabled,
  }));

  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Layout>
      <Header className="header" style={{ background: 'teal' }}>
        <div className="logo" />
        <span style={{ color: 'white', marginRight: '20px', fontSize: '30px', fontWeight: 'bold' }}>Admin Dashboard</span>
      </Header>
      <Layout>
        <Sider width={200} className="site-layout-background" style={{ background: 'teal' }}>
          <Menu mode="inline" defaultSelectedKeys={["1"]} style={{ background: 'teal' }} >
            <Menu.Item key="1" icon={<DashboardOutlined />} style={{ color: 'black', marginTop: "40px" }}>
              <Link to='/Dashboard'>  </Link>
              Dashboard
            </Menu.Item>
            <Menu.Item key="2" icon={<UserOutlined />} style={{ color: 'black', marginTop: "40px" }}>
              UsersList
              <Link to='/userlistdata'>  </Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<FileOutlined />} style={{ color: 'black', marginTop: "40px" }}>
              <Link to='/userlistdata'>  </Link>
              CourseList
            </Menu.Item>
          </Menu>
        </Sider>

        <Layout style={{ padding: "0 24px 24px" }}>
          <Content
            className="site-layout-background"
            style={{
              padding: 80,
              margin: 80,
              minHeight: 280,
            }}
          >
            <h1 className="py-5 text-4xl text-center text-Teal">User Management</h1>
            <Table dataSource={test} columns={columns} className="custom-table" />

            <ExportToExcel apiData={test} fileName={"MootazFarwa"} />
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
}

export default UsersList;
