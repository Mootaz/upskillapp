/* -------------------------------------------------------------------------- */
/*                                 DEPENDECIES                                */
/* -------------------------------------------------------------------------- */

// packages
import React, { useEffect, useState } from "react";
import axios from "axios";
import { Layout, Menu } from "antd";
// Lib dependencies
import { Button, Input, message, Table, Popconfirm } from "antd";
import { DeleteOutlined } from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from "react-router-dom";
import { deleteCourse } from "../../components/Actions/courseActions";
// Constants
import { baseURL } from "../../constants";
import { ExportToExcel } from "../Shared/helpers/ExcelExport";
import {
    UserOutlined,
    DashboardOutlined,
    FileOutlined,
} from "@ant-design/icons";


const { Header, Content, Sider } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function Userslistdata() {
    /* ---------------------------------- HOOKS --------------------------------- */
    const [userList, setUserList] = useState([])






    // side-effects
    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const userListResponse = await axios.get(`${baseURL}/course/getallcourse`);

                setUserList(userListResponse.data);

            } catch (error) {
                console.error('Error fetching user data:', error);
            }
        };

        fetchUserData();
    }, []);


    const dispatch = useDispatch();
    const coursesdelete = useSelector(state => state.courses);
    const handleDelete = async (id) => {
        try {
            await axios.delete(`http://localhost:4000/course/deletecourse/${id}`);
            setUserList(userList.filter((course) => course._id !== id));
            dispatch(deleteCourse(id));
        } catch (err) {
            console.error(err);
        }
    };


    /* -------------------------------- callbacks ------------------------------- */




    /* ----------------------------- LOCAL VARIABLES ---------------------------- */
    const columnsDefNames = ["title", "description", "category", "price", "image", "actions"];
    const columns = columnsDefNames.map((col) => {
        const columnSchema = {};
        columnSchema.title = col;
        columnSchema.dataIndex = col;
        columnSchema.key = col;
        columnSchema.render = (value, record) => {
          if (col === "image") {
            return <img src={`/coursefile/${record.image}`} style={{ width: '100px', height: '100px' }} />;
          } else if (col === "actions") {
            return (
              <Popconfirm
                title="Are you sure you want to delete this course?"
                onConfirm={() => handleDelete(record._id)}
                okText="Yes"
                cancelText="No"
              >
               <Button icon={<DeleteOutlined />} danger />
              </Popconfirm>
            );
          } else {
            return <span>{value}</span>;
          }
        };
        return columnSchema;
      });
      





    const test = userList.map((course, index) => ({
        key: index,
        _id: course._id,
        title: course.title,
        description: course.description,
        category: course.category,
        price: course.price,
        image: course.image

    }));

    /* -------------------------------- RENDERING ------------------------------- */
    return (
        <Layout>
            <Header className="header" style={{ background: 'teal' }}>
                <div className="logo" />
                <span style={{ color: 'white', marginRight: '20px', fontSize: '30px', fontWeight: 'bold' }}>Admin Dashboard</span>
            </Header>
            <Layout>
                <Sider width={200} className="site-layout-background" style={{ background: 'teal' }}>
                    <Menu mode="inline" defaultSelectedKeys={["1"]} style={{ background: 'teal' }} >
                        <Menu.Item key="1" icon={<DashboardOutlined />} style={{ color: 'black', marginTop: "40px" }}>
                            <Link to='/Dashboard'>  </Link>
                            Dashboard
                        </Menu.Item>
                        <Menu.Item key="2" icon={<UserOutlined />} style={{ color: 'black', marginTop: "40px" }}>
                            UsersList
                            <Link to='/UsersList'>  </Link>
                        </Menu.Item>
                        <Menu.Item key="3" icon={<FileOutlined />} style={{ color: 'black', marginTop: "40px" }}>
                            <Link to='/userlistdata'>  </Link>
                            CourseList
                        </Menu.Item>
                    </Menu>
                </Sider>


                <Layout style={{ padding: "0 24px 24px" }}>

                    <Content
                        className="site-layout-background"
                        style={{
                            padding: 80,
                            margin: 80,
                            minHeight: 280,
                        }}
                    >
                        <h1 className="py-5 text-4xl text-center text-Teal ">Course Management</h1>

                        <Table dataSource={test} columns={columns} />
                       
                        <ExportToExcel apiData={test} fileName={"MootazFarwa"} />
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
}

export default Userslistdata;
