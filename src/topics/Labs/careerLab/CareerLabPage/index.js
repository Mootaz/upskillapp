/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React from "react";

import Slider from "react-slick";
import { useNavigate } from "react-router-dom";

// UI lib components
import { Button, Col, Layout, Row, Image } from "antd";

// Local ui components
import TopBar from "../../../layouts/HomePage/topBar";

// assests
import Get_STARTED_1320 from "../../../../assets/careerLab/get-started-1320.png";
import MY_RESUME from "../../../../assets/careerLab/my-resume.png";
import MY_TESTS from "../../../../assets/careerLab/my-tests.png";
import CARRER_PATH from "../../../../assets/careerLab/career-path.png";

// style
import "./index.less";
import Footer from "../../../layouts/HomePage/Footer";

// scoped components
const { Content } = Layout;
function CareerLabPage() {
  /* ---------------------------------- HOOKS --------------------------------- */
  const navigate = useNavigate();
  /* ------------------------------ renderHelpers ----------------------------- */

  function getStartedSection() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      autoplaySpeed: 2000,
      draggable: false,
      cssEase: "linear",
      raggable: true,
      autoplay: true,
      backgroundImage: `url(${Get_STARTED_1320})`,
      backgroundPosition: "center",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
    };
    return (
      <Slider {...settings}>
        <div className="slide-test-CareerLab">
          <Row align="middle">
            <Col span={12} offset={6}>
              <Row justify="center" style={{ marginBottom: "-4%" }}>
                <h1 className="slider-subtitre">
                  <p className="p1">CAREER LAB </p>
                </h1>
              </Row>
              <Row justify="center" className="Row-start-now" >
                {" "}
                <h2 className="slider-subtitre">
                  <p className="p2">Start now! </p>{" "}
                </h2>
              </Row>
              <Row justify="center">
                {" "}
                <Button
                  style={{
                    width: "39%",
                    borderRadius: "50px",
                    color: "white",
                    backgroundColor: "#1034a8",
                    fontSize: "15px",
                  }}
                >
                  <label> Schedule a meeting</label>
                </Button>
              </Row>
            </Col>
          </Row>
        </div>
      </Slider>
    );
  }

  function getMyResumeSection() {
    return (
      <Content className="my-resume">
        <div
          className="container-my-resume"
          align="middle"
          justify="space-around"
        >
          <Row justify="center" align="middle">
            <Col span={7}>
              <Row>
                <h2 className="my-resume-title">MY RESUME</h2>
              </Row>
              <Row>
                <h4 className="container-title">Start now !</h4>
              </Row>

              <Row>
                <div className="container-text">
                  Request a mentorship now ! <br />
                  <br />
                  And get the tips straight <br />
                  <br />
                  from professionals.
                </div>
              </Row>
              <br />
              <Row>
                <Col span={11}>
                  <Button
                    type="primary"
                    align="left"
                    className="txt-btn"
                    style={{}}
                  >
                    Upload CV here
                  </Button>
                </Col>
                <Col span={11} offset={2}>
                  <Button
                    className="txt-btn"
                    type="primary"
                    align="left"
                    onClick={() => {
                      navigate("/dashboard/careerLab");
                    }}
                  >
                    Build CV here
                  </Button>
                </Col>
              </Row>
            </Col>
            <Col span={6} offset={2}>
              <Image
                preview={false}
                src={MY_RESUME}
                className="image-container-needMentorship"
              ></Image>
            </Col>
          </Row>
        </div>
      </Content>
    );
  }

  function getSeparatorSection() {
    return (
      <div className="separator-container">
        <Row align="middle" justify="center">
          <Col>
            <span className="first-row">Get hired on your own terms.</span>{" "}
            <br />
            <span>80% more chance to get your dream job!</span>
          </Col>
        </Row>
      </div>
    );
  }

  function getCareerPathSection() {
    return (
      <Content className="career-path-section">
        <div
          className="container-my-resume"
          align="middle"
          justify="space-around"
        >
          <Row align="middle">
            <Col span={5} offset={4}>
              <div class="ant-image" style={{ width: "160%", right: "60%" }}>
                <Image
                  preview={false}
                  src={CARRER_PATH}
                  className="image-container-needMentorship"
                ></Image>
              </div>
            </Col>
            <Col span={8} offset={2}>
              <Row>
                <h2 className="my-resume-title">MY CAREER PATH</h2>
              </Row>
              <Row>
                <h4 className="container-title">Discover now !</h4>
              </Row>

              <Row>
                <div className="container-text">
                  Request a mentorship now ! <br />
                  <br />
                  And get the tips straight <br />
                  <br />
                  from professionals.
                </div>
              </Row>
              <br />
              <Row>
                <Col span={11} offset={3}>
                  <Button
                    type="primary"
                    align="left"
                    className="txt-btn"
                    onClick={() => {}}
                  >
                    Get started !
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </Content>
    );
  }
  function getMyTestsSection() {
    return (
      <Content className="my-tests-section">
        <div
          className="container-my-resume"
          align="middle"
          justify="space-around"
        >
          <Row justify="center" align="middle">
            <Col span={7}>
              <Row>
                <h2 className="my-resume-title">MY TESTS</h2>
              </Row>
              <Row>
                <h4 className="container-title">Test my skills !</h4>
              </Row>

              <Row>
                <div className="container-text">
                  Request a mentorship now ! <br />
                  <br />
                  And get the tips straight <br />
                  <br />
                  from professionals.
                </div>
              </Row>
              <br />
              <Row>
                <Col span={11} offset={3}>
                  <Button
                    type="primary"
                    className="txt-btn"
                    align="left"
                    onClick={() => {}}
                  >
                    Start testing
                  </Button>
                </Col>
              </Row>
            </Col>
            <Col span={6}>
              <Image
                preview={false}
                src={MY_TESTS}
                className="image-container-needMentorship"
              ></Image>
            </Col>
          </Row>
        </div>
      </Content>
    );
  }
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="career-lab-page">
      <TopBar />
      <Content className="career-lab-content">
        {getStartedSection()}
        {getMyResumeSection()}
        {getSeparatorSection()}
        {getCareerPathSection()}
        {getMyTestsSection()}
        <Footer />
      </Content>
    </Content>
  );
}
export default CareerLabPage;
