const roles = {
    student: "student",
    mentor: "mentor",
    instructor: "instructor",
    recruter: "recruter"
};
const allRoles = () => [
    roles.student,
    roles.mentor,
    roles.instructor,
    roles.recruter
];
export { roles as default, allRoles };