export const mentions = [
  { label: "Passable", value: "passable" },
  { label: "Assez bien", value: "assez-bien" },
  { label: "Bien", value: "bien" },
  { label: "Trés bien", value: "tres-bien" },
  { label: "Excellent", value: "excellent" },
];
