const mentorServices = [
  {
    name: "visual coffee",
    identifier: "visual-coffee",
    time: "15min",
    priceLevels: "Free",
  },
  {
    name: "Breakthrough session",
    identifier: "breakthrough-session",
    time: "45min",
    priceLevels: ["0 - 49.99", "50 - 99.99", "100 - 199.99", "200 <"],
  },
  {
    name: "Extentded breakthrough session",
    identifier: "extednde-breakthrough-session",
    time: "90min",
    priceLevels: ["0 - 49.99", "50 - 99.99", "100 - 199.99", "200 <"],
  },
  {
    name: "Development journey",
    identifier: "development-journey",
    priceLevels: ["0 - 49.99", "50 - 99.99", "100 - 199.99", "200 <"],
  },
];
export default mentorServices;
