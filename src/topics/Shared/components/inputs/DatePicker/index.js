/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */
// Lib dependencies
import React, { useState } from "react";

import DatePickerComponent, {
  registerLocale,
  setDefaultLocale,
} from "react-datepicker";

import fr from "date-fns/locale/fr";

// Style
import "./index.less";
import "react-datepicker/dist/react-datepicker.css";

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function DatePicker({ value, onChange, adjacentLabel }) {
  const DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
  /* -------------------------------- CONTANTS -------------------------------- */
  registerLocale("fr", fr);
  setDefaultLocale("fr");
  // format the date back to ISO

  const resolvedValue = value
    ? // ? parseDateFns(formattedValue, DEFAULT_DATE_FORMAT, new Date())
      new Date(value)
    : undefined;
  const [selectedDate, setSelectedDate] = useState(resolvedValue);
  /**
   * Helper to change field value
   * @param {Date} date
   * @param {DateString} string
   */
  function handleChange(date) {
    setSelectedDate(date);
    onChange(date);
  }

  /* ----------------------------- RENDER HELPERS ----------------------------- */

  /* ----------------------------------- RENDERING ---------------------------------- */
  return (
    <div className="date-picker">
      <div className="flow-widget-native-input">
        <DatePickerComponent
          selected={selectedDate}
          onChange={handleChange}
          locale="fr"
          dateFormat={DEFAULT_DATE_FORMAT}
        />
        {adjacentLabel && (
          <div className="flow-widget-adjacent-label">
            <span>{adjacentLabel}</span>
          </div>
        )}
      </div>
    </div>
  );
}

export default DatePicker;
