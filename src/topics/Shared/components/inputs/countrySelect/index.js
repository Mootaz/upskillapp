import { AutoComplete } from "antd";
import React, { useState, useMemo } from "react";

import countryList from "react-select-country-list";

function CountrySelect({ value, onChange }) {
  // states
  const countriesOptions = useMemo(() => countryList().getData(), []);
  const [options, setOptions] = useState(countriesOptions);
  const resolvedValue = value
    ? countriesOptions.find((ct) => ct.label === value)?.label
    : undefined;

  const [countryValue, setCountryValue] = useState(resolvedValue);
  /* -------------------------------- Callbacks ------------------------------- */
  const changeHandler = (value) => {
    setCountryValue(value);
    onChange(value);
  };

  const onSearch = (searchText) => {
    setOptions(
      !searchText || searchText === ""
        ? countriesOptions
        : countriesOptions.filter((option) =>
            option.label
              .toLocaleLowerCase()
              .startsWith(searchText.toLocaleLowerCase())
          )
    );
  };
  /* -------------------------------- rendering ------------------------------- */
  return (
    <AutoComplete
      options={options.map((option) => ({
        key: option.value,
        value: option.label,
      }))}
      value={countryValue}
      onChange={(e) => {
        changeHandler(e);
      }}
      onSearch={onSearch}
    />
  );
}

export default CountrySelect;
