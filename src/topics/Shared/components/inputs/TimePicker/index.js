/* -------------------------------------------------------------------------- */
/*                                 Dependecies                                */
/* -------------------------------------------------------------------------- */

import React, { useState } from "react";
import NativeTimePicker from "react-time-picker";
import "react-time-picker/dist/TimePicker.css";
import "react-clock/dist/Clock.css";
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function TimePicker({ value, onChange }) {
  /* ---------------------------------- HOOKS --------------------------------- */
  const [selectedTime, setSelectedTime] = useState(value);

  /* -------------------------------- CALLBACKS ------------------------------- */
  const onTimeChange = (timeValue) => {
    onChange(timeValue);
    setSelectedTime(timeValue);
  };

  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <div>
      <NativeTimePicker
        onChange={function cb(time) {
          onTimeChange(time);
        }}
        value={selectedTime}
        disableClock={true}
      />
    </div>
  );
}

export default TimePicker;
