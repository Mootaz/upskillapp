// State

import store from "../../../store";
import { $logout } from "../../Session/slice";

// Constants and data types

// Scoped modules
const { dispatch } = store;

/* -------------------------------------------------------------------------- */
/*                                   Methods                                  */
/* -------------------------------------------------------------------------- */

/**
 * Returns the session token from store (also persisted in local storage)
 * @returns {String} Session JWT-encoded token
 */
export const getSessionToken = () => {
  // @TODO: This is a shortcut to the fact that we're not
  // able to wait for rehydration to complete reactivly
  const storedSessionSlice = localStorage.getItem("persist:root")
    ? JSON.parse(JSON.parse(localStorage.getItem("persist:root")).Session)
    : null;
  const token =
    store.getState().Session.authToken ?? storedSessionSlice?.authToken;
  return token;
};

/**
 * Clears all state slices and dispatches logout action
 */
export function resetSession() {
  // Logout action
  dispatch($logout());
}
