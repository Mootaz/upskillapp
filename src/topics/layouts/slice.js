import { createSlice } from "@reduxjs/toolkit";

// Set initial state
const initialLayoutState = {
  modalContext: null,
};

// Set reducers
const Layout = createSlice({
  name: "Layout",
  initialState: initialLayoutState,
  reducers: {
    $setModalContext(state, action) {
      const stateUpdate = state;
      stateUpdate.modalContext = action.payload || null;
      return stateUpdate;
    },
  },
});
export default Layout.reducer;

// Selectors
export const selectModalContext = (state) => state.Layout.modalContext;
export const { $setModalContext } = Layout.actions;
