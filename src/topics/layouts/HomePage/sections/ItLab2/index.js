/* -------------------------------------------------------------------------- */
/*                                 Dpendencies                                */
/* -------------------------------------------------------------------------- */
// Lib dependencies
import React from "react";

// UI LIB dependencies
import { Row, Col, Image, Button, Typography } from "antd";
// assets
import PC from "../../../../../assets/homePage/Pc.jpeg";

// styles
import "./index.less";

// Scoped components
const { Text } = Typography;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function ItLab2() {
  return (
    <div>
      <Row className="itlab2-container" justify="center" align="middle">
        <Col offset={4} span={8}>
          <Row justify="start">
            <Col>
              <Row className="title-container">
                <Text className="title">
                  Need the help of <span className="resume"> an Expert ?</span>
                </Text>
              </Row>

              <Row>
                <Col className="slider-para">
                  <Row>
                    Explore our list of certified mentors. Experts who can help
                    you advance and provide the best coaching.
                  </Row>
                </Col>
              </Row>
            </Col>
            <Row>
              <Button className="slider-button">
                <Text className="button-text"> Find your match</Text>
              </Button>
            </Row>
          </Row>
        </Col>
        <Col span={10} offset={1}>
          <Row justify="start">
            <Image className="image" preview={false} src={PC}></Image>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
export default ItLab2;
