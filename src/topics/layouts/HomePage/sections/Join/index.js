/* -------------------------------------------------------------------------- */
/*                                DEpendencies                                */
/* -------------------------------------------------------------------------- */
// Import Swiper React components
import { Row, Col, Image, Button, Typography } from "antd";
import React from "react";

// assets Img
import PC1 from "../../../../../assets/JoinTof1.png";
import PC2 from "../../../../../assets/JoinTof2.png";
import PC3 from "../../../../../assets/JoinTof3.png";

//Import css
import "./index.less";
// scoped
const { Text } = Typography
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function Join() {
    return (
        <Row className="rowContainerJoin" justify="center">
            <Col span={24}>
                <Row className="Sec1" justify="center" >
                    <Col span={24}>
                        <Row justify="center">
                            <div className="Sec1Grand" >
                                <div>By your side</div>
                                <div>every step of the way</div>
                            </div>
                        </Row>
                        <Row justify="center">
                            <div className="Sec1Petit">
                                <div>Here we are present to help you through your career</div>
                                <div>from the level of your choice and all the way up !</div>
                            </div>
                        </Row>
                    </Col>
                </Row>
                <Row className="Sec2" justify="center" align="top">
                    <Col span={24}>
                        <Row justify="center" align="middle">
                            <Col span={6} className="Sec2Gauche" >
                                <Row >
                                    <Col span={24}>
                                        <Row className="Sec2Haut" justify="center">
                                            <div className="Circle">
                                                <Image className="img" preview={false} src={PC1}></Image>
                                            </div>
                                        </Row>
                                        <Row justify="center">
                                            <Col span={16}>
                                                <Row className="Sec2M" justify="center">
                                                    <Text className="title" >Stand out in job applications</Text>
                                                </Row>
                                                <Row className="Sec2Para" justify="center">
                                                    <Col span={24}>
                                                        <Text className="Para">
                                                            Let us help you build the resume that highlights your skills and gets you the dream job come true.
                                                        </Text>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>

                            <Col span={6} className="Sec2Gauche">
                                <Row>
                                    <Col span={24}>
                                        <Row className="Sec2Haut" justify="center" align="middle">
                                            <div className="Circle">
                                                <Image className="img" preview={false} src={PC2}></Image>
                                            </div>
                                        </Row>
                                        <Row justify="center"  >
                                            <Col span={16}>
                                                <Row className="Sec2M">
                                                    <Text className="title">Our experts are just one ckick away</Text>
                                                </Row>
                                                <Row className="Sec2Para">
                                                    <Text className="Para">
                                                        Our mentors are available to help you through your journey with the right kick-start
                                                    </Text>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                            <Col span={6} className="Sec2Gauche">
                                <Row>
                                    <Col span={24}>
                                        <Row className="Sec2Haut" justify="center" align="middle">
                                            <div className="Circle">
                                                <Image className="img" preview={false} src={PC3}></Image>
                                            </div>
                                        </Row>
                                        <Row justify="center"  >
                                            <Col span={16}>
                                                <Row className="Sec2M">
                                                    <Text className="title">Showcase your projects</Text>
                                                </Row>
                                                <Row className="Sec2Para">
                                                    <Text className="Para">
                                                    The perfect place to showcase your work and get reviews, offers and much more.
                                                    </Text>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                            
                        </Row>
                    </Col>
                </Row>

            </Col>
        </Row>

    );
}
export default Join;