/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */
import { Row, Image, Typography, Space, Col, Avatar } from "antd";
import React from "react";

// assets
import UserAvatar1 from "../../../../../assets/homePage/userAvatar1.png";
import UserAvatar2 from "../../../../../assets/homePage/userAvatar2.png";
import yellowLine from "../../../../../assets/homePage/YellowLine.png";
import BleuQuotes from "../../../../../assets/homePage/bleuQuotes.png";

// styles
import "./index.less";
// scoped components
const { Text } = Typography;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function UserFeedback() {
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <div className="feedback-container">
      <div>
        <Row justify="center" align="middle">
          <Space className="title">
            <Text>Users</Text> <Text className="word2">Feedback</Text>
          </Space>
        </Row>
        <Row>
          <Image
            className="yellow-line"
            preview={false}
            src={yellowLine}
          ></Image>
        </Row>
        <Row justify="center" align="middle">
          <Text className="subtitle">
            Various versions have evolved over the years,
          </Text>
        </Row>
        <Row justify="center" align="middle">
          <Text className="subtitle2">sometimes by accident.</Text>
        </Row>
        <Row className="reviews">
          <Col span={24}>
            <Row>
              <Col span={7} offset={4} className="review-container">
                <Row>
                  <Col span={4}>
                    <Avatar src={UserAvatar1} className="avatar"></Avatar>
                  </Col>
                  <Col span={18}>
                    <Row>
                      <Text className="name">Jenny Wilson</Text>
                    </Row>
                    <Row>
                      <Text className="job">UI_UX Designer</Text>
                    </Row>
                  </Col>
                </Row>
                <Row>
                  <Text className="review-message">
                    Le Lorem Ipsum est simplement du faux texte employé dans la
                    composition et la mise en page avant impression. Le Lorem
                    Ipsum est le faux texte standard de l'imprimerie.
                    <br /> <br />
                    Quand un imprimeur anonyme assembla ensemble des morceaux de
                    texte pour réaliser un livre spécimen de polices de texte.
                    Il n'a pas fait que survivre cinq siècles
                  </Text>
                </Row>
              </Col>
              <Col>
                <Image
                  className="quotes"
                  preview={false}
                  src={BleuQuotes}
                ></Image>
              </Col>
              <Col span={7} offset={2} className="review-container">
                <Row>
                  <Col span={4}>
                    <Avatar className="avatar" src={UserAvatar2}></Avatar>
                  </Col>
                  <Col ospan={18}>
                    <Row>
                      <Text className="name">Guy Hawkins</Text>
                    </Row>
                    <Row>
                      <Text className="job">UI_UX Designer</Text>
                    </Row>
                  </Col>
                </Row>
                <Row>
                  <Text className="review-message">
                    Le Lorem Ipsum est simplement du faux texte employé dans la
                    composition et la mise en page avant impression. Le Lorem
                    Ipsum est le faux texte standard de l'imprimerie.
                    <br /> <br />
                    Quand un imprimeur anonyme assembla ensemble des morceaux de
                    texte pour réaliser un livre spécimen de polices de texte.
                    Il n'a pas fait que survivre cinq siècles
                  </Text>
                </Row>
              </Col>
              <Col>
                <Image
                  className="quotes"
                  preview={false}
                  src={BleuQuotes}
                ></Image>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}
export default UserFeedback;
