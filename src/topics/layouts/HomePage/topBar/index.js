/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */
// Packages
import React from "react";

import {
  Col,
  Layout,
  Row,
  Dropdown,
  Image,
  Menu,
  Button,
  Avatar,
  Space,
  Typography,
} from "antd";
// import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

// UI lib components
import { DownOutlined, UserOutlined, LogoutOutlined } from "@ant-design/icons";
import NotificationMenu from "./notifications/NotificationMenu";

// redux
import { $setModalContext } from "../../slice";
import { useDispatch, useSelector } from "react-redux";
import { $logout, selectSessionUser } from "../../../Session/slice";

// Assets
import logo from "../../../../assets/Logo.png";

// Style
import "./index.less";

// scoped components
const { Text } = Typography;
/* -------------------------------------------------------------------------- */
/*                               Implementation                               */
/* -------------------------------------------------------------------------- */
function TopBar() {
  /* ---------------------------------- hooks --------------------------------- */
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const currentUser = useSelector(selectSessionUser);
  /* -------------------------------- callbacks ------------------------------- */
  function onClickLoginButton() {
    dispatch($setModalContext("login"));
  }

  function onClickLogoutButton() {
    dispatch($logout());
    navigate("/homePage");
  }
  function onClickProfil() {
    navigate("/careerLab");
  }
  function onClickDashbord() {
    navigate("/dashboard/globalDashboard");
  }
  function onClickCareerLab() {
    navigate("/careerLab");
  }
  function onClickmentorlab() {
    navigate("/mentorlab");
  }
  function onClickMentorProfil() {
    navigate(`/mentorProfil/${currentUser._id}`);
  }
  function onClickLearning() {
    navigate(`/Learning`);
  }

  // Localization
  // const { t } = useTranslation("layouts");
  const { Header, Content } = Layout;

  const menu = (
    <Menu
      items={[
        {
          label: (
            <Row
              onClick={() => {
                onClickLearning();
              }}
            >
              Learning Management
            </Row>
          ),
          key: "0",
        },
        {
          label: (
            <Row
              onClick={() => {
                onClickCareerLab();
              }}
            >
              Career Lab
            </Row>
          ),
          key: "1",
        },
        {
          label: (
            <Row
              onClick={() => {
                onClickmentorlab();
              }}
            >
              Mentoring Lab
            </Row>
          ),

          key: "2",
        },
        {
          label: "Training Lab",
          key: "3",
        },
      ]}
    />
  );
  /* ----------------------------- Local variables ---------------------------- */
  const userMenu = (
    <Menu
      items={[
        {
          label: `${currentUser?.firstName} ${currentUser?.lastName}`,
          key: "0",
        },
        {
          label: (
            <Row
              onClick={() => {
                onClickProfil();
              }}
            >
              Profile
            </Row>
          ),
          key: "1",
        },

        {
          label: (
            <Row
              onClick={() => {
                onClickMentorProfil();
              }}
            >
              Mentor profile
            </Row>
          ),
          key: "2",
        },

        {
          label: (
            <Row
              onClick={() => {
                onClickDashbord();
              }}
            >
              Dashbord
            </Row>
          ),
          key: "3",
        },

        {
          type: "divider",
        },
        {
          label: (
            <Row>
              <Col>
                <LogoutOutlined />
              </Col>
              <Col
                onClick={() => {
                  onClickLogoutButton();
                }}
              >
                Logout
              </Col>
            </Row>
          ),
          key: "3",
        },
      ]}
    />
  );
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="top-bar">
      <Header className="top-bar-header">
        <Row>
          <Col span={3} className="logo">
            <Image
              preview={false}
              src={logo}
              onClick={() => {
                navigate("/homePage");
              }}
            ></Image>
          </Col>
          <Col className="menu-container" flex={"auto"}>
            <Row align="middle" justify="center">
              <Col className="Lab">
                <Button
                  className="menu-button"
                  onClick={() => {
                    navigate("/homePage");
                  }}
                >
                  <Text className="top-bar-item">
                    {/* {t("HomePage.TopBar.Home")} */}Home
                  </Text>
                </Button>
              </Col>
              <Col className="Lab">
                <Dropdown overlay={menu} trigger={["click"]}>
                  <div onClick={(e) => e.preventDefault()}>
                    <Space>
                      {/* {t("HomePage.TopBar.Explore")} */}Explore
                      <DownOutlined
                        style={{ fontSize: "70%", color: "#848484" }}
                      />
                    </Space>
                  </div>
                </Dropdown>
              </Col>

              <Col className="Lab">
                <Button className="menu-button">
                  <Text className="top-bar-item">For Business</Text>
                </Button>
              </Col>
              <Col className="Lab">
                <Button className="menu-button">
                  <Text className="top-bar-item">
                    {/* {t("HomePage.TopBar.Community")} */}Community
                  </Text>
                </Button>
              </Col>
            </Row>
          </Col>

          {!currentUser && (
            <Col>
              <Button
                className="sign-in-button"
                onClick={() => {
                  onClickLoginButton();
                }}
              >
                <Text className="button-text">Sign In | Get Started</Text>
              </Button>
            </Col>
          )}
          {currentUser && (
            <Col style={{ paddingRight: "2%" }}>
              <NotificationMenu />
            </Col>
          )}
          {currentUser && (
            <Col>
              <Dropdown overlay={userMenu} trigger={["click"]}>
                <Space>
                  <Text className="user-welcome-message">
                    {`Welcome ${currentUser.firstName} ${currentUser.lastName}`}{" "}
                  </Text>
                  <Avatar
                    className="user-avatar"
                    icon={<UserOutlined />}
                  ></Avatar>
                </Space>
              </Dropdown>
            </Col>
          )}
        </Row>
      </Header>
    </Content>
  );
}
export default TopBar;
