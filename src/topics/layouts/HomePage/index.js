/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React, { useEffect, useState } from "react";

// UI lib components
import { Layout } from "antd";

// UI local components
import App from "../../../App";
import TopBar from "./topBar";
import MentorsSection from "./sections/MentorsSection";

import UserFeedback from "./sections/UserFeedback";
import ItLab from "./sections/ItLab";
import Footer from "./Footer";
import BuildCVPage from "../../Session/buildCV";
import ModalLayout from "../ModalLayout";
// Style
import "./index.less";
import { useSelector } from "react-redux";
import { selectModalContext } from "../slice";
import Join from "./sections/Join";
import HeroImg from "./sections/HeroImg";
import ItLab2 from "./sections/ItLab2";

// Scoped components
const { Content } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function HomePage() {
  /* ********************************** HOOKS ********************************* */

  const [convertCVInVisible, setConvertCVInVisible] = useState(false);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const sourceModalContext = useSelector(selectModalContext);
  console.log("process.env", process.env);
  // side effects
  useEffect(() => {
    setIsModalVisible(sourceModalContext !== null);
  }, [sourceModalContext]);

  // const test = new Date(date.getTime() - userTimezoneOffset);
  // console.log("date", test);
  // /* ***************************** RENDER HELPERS ***************************** */

  /* ******************************** RENDERING ******************************* */
  return (
    <App>
      <Content>
        <TopBar />
        <BuildCVPage
          modalVisible={convertCVInVisible}
          setModalVisible={setConvertCVInVisible}
        />
        <Content>
          <HeroImg />
          <Join />
          <ItLab />
          <ItLab2 />
          <MentorsSection />
          <UserFeedback />
        </Content>
        <Footer />
      </Content>
      <ModalLayout isVisible={isModalVisible} />
    </App>
  );
}

export default HomePage;
