import React from 'react'
import {
    CloudUploadIcon,
    DatabaseIcon,
    PaperAirplaneIcon,
    ServerIcon,
} from '@heroicons/react/solid'
import HommefemmeImg from '../assets/womenman.png'
import EtoileHomePagetailwind from '../assets/EtoileHomePagetailwind.png'
import vecHomePagetailwind from '../assets/vecHomePagetailwind.png'

export default function Hero() {
    return (
        <div name='home' className='w-full h-screen bg-blue-500 flex flex-col justify-between'>
         
            <div className='grid md:grid-cols-2 max-w-[1240px] m-auto'>
            {/* <img className=' absolute   right-6 mr-20 w-20 pb-20  mt-60 ' src={vecHomePagetailwind}/> */}
            <img  className=' absolute   ml-80  h-5 w-5 mt-6 ' src={EtoileHomePagetailwind}/> 
                <div className='flex flex-col justify-center md:items-start w-full px-2 py-8'>
                    <p className='text-6xl text-white font-bold'>Access To</p>
                    <p className='text-6xl text-indigo-600 font-bold'>hundreds</p>
                    <p className='text-6xl text-white font-bold'>Of experts in </p>
                    <p className='text-6xl text-white font-bold'>one click !</p>
                    </div>          
                    
                   
                    
                    <div>
                    <img className=' ' src={HommefemmeImg} alt="/" />
                    </div>
            </div>
        </div>
    )
}
