import React from 'react'
import yellowLine from '../assets/YellowLine.png'
import bleuQuotestailwind from '../assets/bleuQuotestailwind.png'

import userAvatar1tailwind from '../assets/userAvatar1tailwind.png'
import userAvatar2tailwind from '../assets/userAvatar2tailwind.png'





export default function Userfeedback() {
  return (
    <div class="feedback-container ">
      <div class="mx-auto max-w-screen-lg ">
        <h1 class="text-5xl font-bold text-center mt-6">
          Users <span class="text-blue-500">Feedback</span>
        </h1>
        <div class="flex justify-center">
          <img class="w-24 h-5" src={yellowLine} alt="Yellow Line" />
        </div>
        <div class="flex justify-center mt-2">
          <p class="text-xl text-gray-500 text-center">
            Various versions have evolved over the years , 
          </p>
        </div>
        <div class="flex justify-center ">
          <p class="text-xl text-center text-gray-500 ">sometimes by accident.</p>
        </div>
        <div class="grid grid-cols-2 gap-20 mt-3 ">
          <div class="first-review-container">
            <div class="flex flex-row gap-x-7  pt-3  justify-between bg-white rounded-3xl">
              <div class="gap-x-7 md:w-2/3 px-2 ">
                <div class="flex flex-row px-2">
                  <img src={userAvatar1tailwind} class="w-14 h-15 rounded-full" />
                  <div class="flex flex-col ml-2">
                    <p class="text-sm font-bold">Jenny Wilson</p>
                    <p class="text-sm">UI_UX Designer</p>
                  </div>
                </div>
                <div class="mt-2 px-2">
                  <p >Le Lorem Ipsum est simplement du faux texte employé dans la</p>
                  <p className='mt-3'>Le Lorem Ipsum est simplement du faux texte employé dans la</p>
                
                </div>
              </div>
              <div class="hidden md:block w-1/12 px-2">
                <img src={bleuQuotestailwind} class="w-4 h-4" />
              </div>
            </div>
          </div>
          <div class="first-review-container ">
            <div class="flex flex-row gap-x-7 pt-3 justify-between bg-white rounded-3xl">
              <div class="gap-x-7 md:w-2/3 px-2">
                <div class="flex flex-row px-2">
                  <img src={userAvatar2tailwind} class="w-14 h-15 rounded-full" />
                  <div class="flex flex-col ml-2">
                    <p class="text-sm font-bold">Guy Hawkins</p>
                    <p class="text-sm">UI_UX Designer</p>
                  </div>
                </div>
                <div class="mt-2 px-2">
                  <p >Le Lorem Ipsum est simplement du faux texte employé dans la</p>
                  <p className='mt-3'>Le Lorem Ipsum est simplement du faux texte employé dans la</p>
                
                </div>
              </div>
              <div class="hidden md:block w-1/12 px-2">
                <img src={bleuQuotestailwind} class="w-4 h-4" />
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>

  );
}

