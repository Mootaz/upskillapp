import React, {useState} from 'react';
import { Link, animateScroll as scroll, } from 'react-scroll'


import { MenuIcon, XIcon } from '@heroicons/react/outline';
import upskillImg  from '../assets/upskilllogo.png'
import './Navbar.css'
  
const Navbar = () => {
    const [nav, setNav] = useState(false)
    const handleClick = () => setNav(!nav)

    const handleClose =()=> setNav(!nav)

  return (
    <div className='w-screen h-[80px] z-10 bg-white fixed drop-shadow-lg'>
      <div className='px-2 flex justify-between items-center w-full h-full'>
        <div className='flex items-center'>
        <img className=' justify-center items-center' src={upskillImg} alt="/" />
          <ul className='hidden md:flex '>
          <li className='md:ml-8 '><Link to="home" smooth={true} duration={500}>Home</Link></li>
          <li className='md:ml-8 '><Link to="about" smooth={true} offset={-200} duration={500}>Explore</Link></li>
          <li className='md:ml-8 '><Link to="support" smooth={true} offset={-50} duration={500}>Support</Link></li>
          <li className='md:ml-8 '><Link to="platforms" smooth={true} offset={-100} duration={500}>For Buisness</Link></li>
          <li className='md:ml-8 '><Link to="pricing" smooth={true} offset={-50} duration={500}>Community</Link></li>
          </ul>
        </div>
        <div className='hidden md:flex pr-4'>
          
          <button className='sign-in-button px-8 py-3 bg-indigo- text-xs text-white'>Sign In| Get Started</button>
        </div>
        <div className='md:hidden mr-4' onClick={handleClick}>
            {!nav ? <MenuIcon className='w-5' /> : <XIcon className='w-5' />}
          
        </div>
      </div>

      <ul className={!nav ? 'hidden' : 'absolute bg-zinc-200 w-full px-8'}>
          <li className='border-b-2 border-zinc-300 w-full'><Link onClick={handleClose} to="home" smooth={true} duration={500}>Home</Link></li>
          <li className='border-b-2 border-zinc-300 w-full'><Link onClick={handleClose} to="about" smooth={true} offset={-200} duration={500}>About</Link></li>
          <li className='border-b-2 border-zinc-300 w-full'><Link onClick={handleClose} to="support" smooth={true} offset={-50} duration={500}>Support</Link></li>
          <li className='border-b-2 border-zinc-300 w-full'><Link onClick={handleClose} to="platforms" smooth={true} offset={-100} duration={500}>Platforms</Link></li>
          <li className='border-b-2 border-zinc-300 w-full'><Link onClick={handleClose} to="pricing" smooth={true} offset={-50} duration={500}>Pricing</Link></li>

        <div className='flex flex-col my-4'>
            <button className='sign-in-button px-8 py-3'>Sign In| Get Started</button>
        </div>
      </ul>
    </div>
  );
};

export default Navbar;